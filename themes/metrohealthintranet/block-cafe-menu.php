<?php
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);
?>
<div class="portlet <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
	<div class="portlet-header">Cafe Menu<span><?php collapse_expand_links(); ?></span></div>
	<div class="portlet-content">
		<a class="button" href="#">See Full Menu</a>
	</div>
</div>