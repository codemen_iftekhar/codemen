<aside>
	<?php
	if( $post->post_parent ) {
		$children = wp_list_pages('title_li=&child_of='.$post->post_parent.'&echo=0&depth=1'); 
		$parent_title = get_the_title( $post->post_parent );
	} else {
		$children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0&depth=1');
		$parent_title = get_the_title( $post->ID );
	}
	
	if ( empty( $children ) ) {
		$children = wp_list_pages('title_li=&echo=0&depth=1');
	} else {
		echo "<h2>" . $parent_title . "</h2>";
	}
	
	?>
	<ul>
		<?php echo $children; ?> 
	</ul>
	<?php 
	
	
	if ( is_singular() ) echo get_post_meta( $post->ID, '_side_content', true );
	?>
</aside>