<?php
/*
Template Name: Department Staff Page 
*/
get_header();

dynamic_sidebar('breadcrumbs') ?>

<div class="column-main-sidenav">
	<?php get_sidebar(); ?>
</div>
<div id="twocolumns">
	<div class="twocolumns-holder">
		<div class="column-inner">
			<div class="main-heading">
	   		<h2><?php echo get_the_title(get_the_ID()) ?></h2>
  		</div>
		<?php if(have_posts()) : the_post() ?>
			  <?php the_content() ?>
		<?php endif ?>
		
		<?php
		$department_id = get_post_meta( $post->post_parent, '_department', true);
		$these_people = Ciho_Metrohealth_IDM_Department::get_deparment_users( $department_id );
		
		print( '<ul>' );
		foreach ( $these_people as $person) {
			printf( '<li><a href="/people/%1$s"><img height="63" width="63" src="%2$s" /> %3$s %4$s</a></li>', strtolower($person->AD_ID), get_template_directory_uri() . '/images/ManSilhouette.jpg', $person->first_name, $person->last_name );
		}
		print( '</ul>' );
		
		?>
		
		</div>
		<div class="column-inner">
			<?php get_sidebar('right'); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>