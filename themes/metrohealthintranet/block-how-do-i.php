<?php
$component_name = 'how-do-i';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);

$args = array( 
		'post_type' => 'faq'
	);
query_posts($args);

$faqs = array();
while( have_posts() ) 
{
	the_post();
	$faqs[get_permalink()] = get_the_title();
}
wp_reset_query();
wp_reset_postdata();
?>
<?php if ($component_properties->movable) { ?><article class="block <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h3>How Do I</h3>
		</div>
		
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
			<div class="portlet-content">
				<?php 
				if ($faqs)
				{
					?>
					<select name="faq" class="redirect_on_change">
						<option>Frequently Asked Questions</option>
						<?php
						foreach ($faqs as $faq_url => $faq_title)
						{
							printf('<option value="%s">%s</option>', $faq_url, $faq_title);
						}
						?>
					</select>
					<?php
				}
				?>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>
