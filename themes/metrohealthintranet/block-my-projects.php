<?php
$component_name = 'my-projects';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);
	
if (empty($component_properties))
	$filter_val = 0;
elseif($component_properties->filter_val==0)
	$filter_val = 0;
else
	$filter_val = $component_properties->filter_val;

?>
<?php if ($component_properties->movable) { ?><article class="box style01 <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h2>My Groups</h2>
		</div>
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
		
			<div id="ffshome-my-group" class="portlet-content">
                            <?php 

                            global $current_user;
                            //var_dump($current_user);
                             $uid=$current_user->ID;
                             $username = $current_user->first_name;
                             //var_dump($username);
                            $args=array(
                                   'user_id'=>$uid,
                                    'scope'=>'groups',
                                    'per_page'=>3
                               );
                            //if ( bp_has_activities( bp_ajax_querystring( 'activity' ) ) ) :   
                             if ( bp_has_activities( $args ) ) : ?>
                            
                                <ul id="activity-stream" class="activity-list item-list">
                                    <?php while ( bp_activities() ) : bp_the_activity(); ?>

                                            <?php //locate_template( array( 'activity/entry.php' ), true, false ); ?>
                                                       <div class="activity-content">

                                                    <div class="activity-header">

                                                            <?php 
                                                            global $activities_template;
                                                            //echo '<pre>';
                                                            //var_dump($activities_template);
                                                            //echo '</pre>';
                                                            $action = $activities_template->activity->action;
                                                          
                                                            $str = preg_replace('/<a[^>]*>(.*?)<\/a>/', '$1', $action, 2); 
                                                           $replace="You";                                                           
                                                           echo preg_replace('/'.$username.'\b/i',$replace,$str);
                                                           echo $activity_meta = bp_insert_activity_meta('');
                                                           
                                                                   
                                                            ?>

                                                    </div>

                                                    <?php if ( bp_activity_has_content() ) : ?>

                                                            <div class="activity-inner">

                                                                    <?php
                                                                   $string = bp_get_activity_content_body();
                                                                    $pattern = '/(.*<a .*>).*(<\/a>.*)/'; 
                                                                    $replacement = '$1VIEW PROJECT$2'; 
                                                                    echo preg_replace($pattern, $replacement, $string);
                                                                    preg_replace('/hello\b/i','NEW',$_text)
                                                                    ?>
                                                               
                                                            </div>

                                                    <?php endif; ?>
                                                       
                                                    <?php do_action( 'bp_activity_entry_content' ); ?>


                                            </div>
                                    <?php endwhile; ?>
                             </ul>


                            <?php else : ?>

                                    <div id="message" class="info">
                                            <p><?php _e( 'Sorry, there was no activity found. Please try a different filter.', 'buddypress' ); ?></p>
                                    </div>

                            <?php endif; ?>
                            <p class="align-left">
                                <?php 
                                echo'<a href="'. get_home_url().'/people/'.$username.'/groups/">View All MY Projects</a>';
                                ?>
                            </p>
                            <p class="align-right">
                                <?php 
                                echo'<a href="'. get_home_url().'/groups/">Add Projects</a>';
                                ?>
                            </p>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>

<!--
<article class="box active style01">
				<div class="heading-holder">
					<a href="#" class="opener">opener</a>
					<h2>My Groups & Projects</h2>
				</div>
				<div class="slide">
					<ul class="info-list">
						<li>
							<h3><a href="#">You were added to "Community Ourreach"</a></h3>
							<em class="date">Added 2 Hours ago </em>
							<p>Brna nibh, blandit ut mattis vel, elementum iaculis. Brna nibh, blandit ut mattis vel, elementum iaculis. <a href="#">VIEW PROJECT</a></p>
						</li>
						<li>
							<h3><a href="#">You created an event "Metro at the Zoo"</a></h3>
							<em class="date">Added 8 Hours ago </em>
							<p>Brna nibh, blandit ut mattis vel, elementum iaculis.  Brna nibh, blandit ut mattis vel, elementum iaculis. <a href="#">VIEW PROJECT</a></p>
						</li>
						<li>
							<h3><a href="#">You were assigned a task "Contact Dr. Thomson"</a></h3>
							<em class="date">Added 2 Days ago </em>
							<p>Brna nibh, blandit ut mattis vel, elementum iaculis.Brna nibh, blandit ut mattis vel, elementum iaculis. <a href="#">VIEW PROJECT</a></p>
						</li>
					</ul>
					<div class="more-holder">
						<a href="#" class="more alignright">Add Project</a>
						<a href="#" class="more alignleft">View All My Projects</a>
					</div>
				</div>
			</article>
-->