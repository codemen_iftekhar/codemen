<!doctype html> <!-- html5 -->
<html> <!-- NO lang, NO xmlns="http://www.w3.org/1999/xhtml" and so on -->
<head>
<meta http-equiv="x-ua-compatible" content="IE=8"/> <!-- as the vey first line after head-->
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php wp_title(' | ', true, 'right'); ?><?php bloginfo('name'); ?></title>
	<link media="all" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/all.css">
	<link media="all" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
	<!--[if IE 8]>
		
		<link media="all" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/ie.css">
	<![endif]-->
	
	<?php if ( is_singular() ) wp_enqueue_script( 'theme-comment-reply', get_template_directory_uri()."/js/comment-reply.js" );
	wp_enqueue_script('jquery');
	global $is_IE; if ($is_IE) wp_enqueue_script('psd2html_ie', get_template_directory_uri() . '/js/ie.js' );
	wp_head();       
       
        ?>
 
    <!--[if IE]>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.main.ie.js"></script>
    <![endif]-->
    <![if !IE]>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.main.js"></script>
		<style>
			article#my-applications-remove { display:none!important; }
		</style>
    <![endif]>
</head>
<body <?php body_class(); ?>>
	<noscript>Javascript must be enabled for the correct page display</noscript>
	<div class="skip"><a accesskey="S" href="#main">Skip to Content</a></div>
	<div class="wrapper">
		<header id="header">
                    <?php //var_dump($_REQUEST); ?>
			<div class="wrap">
				<div class="holder">
					<h1 class="logo"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="MetroHealth"></a></h1>
					<div class="login-box">
						<div class="img-holder"><a href="#"><img width="44" height="51" src="<?php echo get_template_directory_uri(); ?>/images/ManSilhouette.jpg" alt=""></a></div>
						<ul class="add-nav">
							<li>Hello <strong><?php global $current_user; echo $current_user->first_name; ?></strong></li>
							                                                       
                                                        <li class="ffs-notification">                                                          
                                                                <?php fss_bp_adminbar_notifications_menu(); ?>
                                                            
                                                        </li> 
							<li><a href="/preferences">Change Preferences</a></li>
						</ul>
						<p>Welcome to the Metronet- Take a Tour & Customize Your Experience</p>
					</div>
				</div>
				<div class="frame">
					<nav id="nav">

						<a href="#" class="open">open</a>
						<ul>
							<li><a href="#">People</a></li>
							<li><a href="#">Departments</a>
							<div class="drop">
									<div class="column">
										<!--<strong class="heading">Day To Day 2</strong>-->
										<?php if(has_nav_menu('depts-menu-left'))
                                            wp_nav_menu( array('container' => false,
                                                     'theme_location' => 'depts-menu-left',
                                                     'depth' => 1,
                                                     'items_wrap' => '<ul>%3$s</ul>'."\n"
                                                    ) );
											dynamic_sidebar('depts-menu-left') 
										?>
									</div>
									<div class="column">
										<!--strong class="heading">Learn, Grow & Connect</strong>-->
										<?php if(has_nav_menu('depts-menu-middle'))
                                            wp_nav_menu( array('container' => false,
                                                     'theme_location' => 'depts-menu-middle',
                                                     'depth' => 1,
                                                     'items_wrap' => '<ul>%3$s</ul>'."\n"
                                                    ) );
											dynamic_sidebar('depts-menu-middle') 
										?>
									</div>
									<div class="column style01">
										<!--<strong class="heading">Metro Way</strong>-->
										<?php if(has_nav_menu('depts-menu-right'))
                                            wp_nav_menu( array('container' => false,
                                                     'theme_location' => 'depts-menu-right',
                                                     'depth' => 1,
                                                     'items_wrap' => '<ul>%3$s</ul>'."\n"
                                                    ) );
											dynamic_sidebar('depts-menu-right') 
										?>
									</div>

								</div>
							</li>
							<li>

								<a href="#">Resources</a>

								<div class="drop">

									<div class="column">
										<strong class="heading">Day To Day</strong>
										<ul>
											<li><a href="#">Applications Policies</a></li>
											<li><a href="#">Forms</a></li>
											<li><a href="#">Places & Maps </a></li>
											<li><a href="#">Ordering & Supplies</a></li>
										</ul>
									</div>
									<div class="column">
										<strong class="heading">Learn, Grow & Connect</strong>
										<ul>
											<li><a href="#">Education & Training News</a></li>
											<li><a href="#">Events</a></li>
											<li><a href="#">Discounts & Incentives Healthy Best Classifieds</a></li>
										</ul>
									</div>
									<div class="column style01">
										<strong class="heading">Metro Way</strong>
										<ul>
											<li><a href="#">Handbook Leadership Quality Scores Awards</a></li>
											<li><a href="#">Financial Reports Pennant</a></li>
										</ul>
									</div>

								</div>

							</li>
							<li><a href="#">Calendars & Events</a></li>
						</ul>
					</nav>
					
					<?php  dynamic_sidebar('search-sidebar') ?>
					
				</div>
			</div>
		</header>
		<div id="main">