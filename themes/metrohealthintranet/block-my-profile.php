<?php
$component_name = 'my-profile';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);

global $current_user;
get_currentuserinfo();

?>
<article class="portlet box style01 <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
	<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
		<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
		<h2>My Profile</h2>
	</div>
	<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
		<div class="holder">
			<div class="img-holder">
				<img src="<?php echo get_template_directory_uri(); ?>/images/ManSilhouette.jpg" alt="image description">
			</div>
			<div class="text-holder">
				<h3><?php printf('%s %s', $current_user->idm_data->first_name, $current_user->idm_data->last_name); ?></h3>
				<dl>
					<?php
					$title = get_user_meta( $current_user->ID, 'adi_title', true );
					if ( ! empty( $title ) ) {
						?>
						<dt>TITLE: </dt>
						<dd><?php echo $title; ?></dd>
						<?php
					}
					?>
					<!--
					<dt>SPECIALITY:</dt>
					<dd>---</dd> -->
					<dt>DEPARTMENT: </dt>
					<dd><?php echo $current_user->idm_data->department_name; ?></dd>
					<!-- <dt>REPORTS TO: </dt>
					<dd>---</dd> -->
				</dl>
				<dl>
					<?php
					$phone_number = get_user_meta( $current_user->ID, 'adi_telephonenumber', true );
					if ( ! empty( $phone_number ) ) {
						?>
						<dt>PHONE: </dt>
						<dd><?php echo $phone_number; ?></dd>
						<?php
					}
					?>
					
					<dt>EMAIL:</dt>
					<dd><a href="mailto:<?php echo $current_user->user_email; ?>"><?php echo $current_user->idm_data->email; ?></a></dd>
				</dl>
				<!-- <dl>
					<dt>EDUCATION: </dt>
					<dd>---</dd>
				</dl> -->
				<!-- <div class="more-holder">
					<a href="#" class="more">Customize Profile</a>
				</div> -->
			</div>
		</div>
	<?php if ($component_properties->movable) { ?></div><?php } ?>
</article>