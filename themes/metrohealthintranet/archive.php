<?php get_header();

dynamic_sidebar('breadcrumbs') ?>
<div class="column-main-sidenav">
	<?php get_sidebar(); ?>
</div>
<div id="twocolumns">
	<div class="twocolumns-holder">
		<div class="column-inner">
			<div class="main-heading">
	   		<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
	<?php /* If this is a category archive */ if (is_category()) { ?>
	<h2><?php printf(__( 'Archive for the &#8216;%s&#8217; Category', 'base' ), single_cat_title('', false)); ?></h2>
	<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>			
	<h2><?php printf(__( 'Posts Tagged &#8216;%s&#8217;', 'base' ), single_tag_title('', false)); ?></h2>
	<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
	<h2><?php _e('Archive for', 'base'); ?> <?php the_time('F jS, Y'); ?></h2>
	<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
	<h2><?php _e('Archive for', 'base'); ?> <?php the_time('F, Y'); ?></h2>
	<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
	<h2><?php _e('Archive for', 'base'); ?> <?php the_time('Y'); ?></h2>
	<?php /* If this is an author archive */ } elseif (is_author()) { ?>
	<h2><?php _e('Author Archive', 'base'); ?></h2>
	<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
	<h2><?php _e('Blog Archives', 'base'); ?></h2>
	<?php } ?>
  		</div>
		<?php if(have_posts()) : ?>
	
		
			
			<?php while (have_posts()) : the_post(); ?>
				
				<div id="post-<?php the_ID(); ?>">
					<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'base'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<?php the_excerpt(); ?>
				</div>
		
			<?php endwhile; ?>
			
			<div class="navigation">
				<div class="next"><?php next_posts_link(__('Older Entries &raquo;', 'base')) ?></div>
				<div class="prev"><?php previous_posts_link(__('&laquo; Newer Entries', 'base')) ?></div>
			</div>
			
		
		
	<?php else : ?>
	
		<div class="block-contact">
			<h2><?php _e('Not Found', 'base'); ?></h2>
			<p><?php _e('Sorry, but you are looking for something that isn\'t here.', 'base'); ?></p>
		</div>
		
	<?php endif ?>
		</div>
		<div class="column-inner">
			<article class="block active style02">
				<div class="heading-holder">
					<a href="#" class="opener">opener</a>
					<h3>How Do I?/ FAQ�s</h3>
				</div>
				<div class="slide">
					<div class="more-holder"><a href="#" class="more">Show FAQ�s</a></div>
				</div>
			</article>
			<article class="block active">
				<div class="heading-holder">
					<a href="#" class="opener">opener</a>
					<h3>Metro Moments</h3>
				</div>
				<div class="slide">
					<div class="slide-container">
						<h4>Interesting Award of <br />Accomplishment </h4>
						<p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis   <a href="#">READ MORE</a></p>
						<div class="col-holder">
							<div class="col">
								<p><a href="#">Submit</a></p>
								<p><a href="#">Metro Moment</a></p>
							</div>
							<div class="col">
								<p><a href="#">View All </a></p>
								<p><a href="#">Metro Moments</a></p>
							</div>
						</div>
					</div>
				</div>
			</article>
			<article class="block style03">
				<div class="heading-holder">
					<h3>HR Calendar of Events</h3>
				</div>
				<div class="calendar-placeholder">
					<img src="<?php echo get_template_directory_uri(); ?>/images/img-calendar-placeholder.gif" alt="image description">
				</div>
				<a href="#" class="more">View More HR Events</a>
			</article>
		</div>
	</div>
</div>


<?php get_footer(); ?>