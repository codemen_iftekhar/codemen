<?php
/*
Template Name: Home Template
*/
get_header(); 
$user_settings = User_Settings::get();
?>
<div class="column-main column" column_id="1">
	<?php
	$components = $user_settings->get_column_order(1);
	if (empty($components)) $components = Metro_Widget_Defaults::get_default_column_order(1);
	$components_a = explode(',', $components);
	foreach ($components_a as $component) get_template_part('block', $component);
	
	// get_template_part('block', 'important-news');
	// get_template_part('block', 'my-metro-updates');
	// get_template_part('block', 'best-of-metro');
	// get_template_part('block', 'metro-moments');
	?>
</div>
<div id="twocolumns">
	<div class="twocolumns-holder">
		<div class="column" column_id="2">
			<?php
			$components = $user_settings->get_column_order(2);
			if (empty($components)) $components = Metro_Widget_Defaults::get_default_column_order(2);
			$components_a = explode(',', $components);
			foreach ($components_a as $component) get_template_part('block', $component);
			?>
		</div>
		<div class="column third-column" column_id="3">
			<?php
			$components = $user_settings->get_column_order(3);
			if (empty($components)) $components = Metro_Widget_Defaults::get_default_column_order(3);
			$components_a = explode(',', $components);
			foreach ($components_a as $component) get_template_part('block', $component);
			?>		
		</div>
	</div>
</div>
		
<?php get_footer(); ?>