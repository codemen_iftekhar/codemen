<?php get_header();

dynamic_sidebar('breadcrumbs') ?>

<div class="column-main-sidenav">
	<?php get_sidebar(); ?>
</div>
<div id="twocolumns">
	<div class="twocolumns-holder">
		<div class="column-inner">
			<div class="main-heading">
	   		<h2><?php echo get_the_title(get_the_ID()) ?></h2>
  		</div>
		<?php if(have_posts()) : the_post() ?>
	
		
			
			<?php the_content() ?>
			
			
			
		
		<?php endif ?>
		</div>
		<div class="column-inner">
			
		</div>
	</div>
</div>

<?php get_footer(); ?>