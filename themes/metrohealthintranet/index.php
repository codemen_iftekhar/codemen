<?php get_header();

dynamic_sidebar('breadcrumbs') ?>

<div class="main-heading">
	<h2><?php _e('Blog Archives', 'base'); ?></h2>
</div>
<div id="twocolumns">
	
	<?php if(have_posts()) : ?>
	
		<div class="block-contact">
			
			<?php while (have_posts()) : the_post(); ?>
				
				<div id="post-<?php the_ID(); ?>">
					<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'base'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<?php the_excerpt(); ?>
				</div>
		
			<?php endwhile; ?>
			
			<div class="navigation">
				<div class="next"><?php next_posts_link(__('Older Entries &raquo;', 'base')) ?></div>
				<div class="prev"><?php previous_posts_link(__('&laquo; Newer Entries', 'base')) ?></div>
			</div>
			
		</div>
		
	<?php else : ?>
	
		<div class="block-contact">
			<h2><?php _e('Not Found', 'base'); ?></h2>
			<p><?php _e('Sorry, but you are looking for something that isn\'t here.', 'base'); ?></p>
		</div>
		
	<?php endif ?>
	
</div>

<?php get_footer(); ?>