<?php
//add_action('init', 'remove_admin_bar');

//function remove_admin_bar() {
//if (!current_user_can('administrator') && !is_admin()) {
//  show_admin_bar(false);
//}
//}
//staging restrictions
if (file_exists(sys_get_temp_dir().'/staging-restrictions.php')) {
	define('STAGING_RESTRICTIONS', true);
	require_once sys_get_temp_dir().'/staging-restrictions.php';
}


include( get_template_directory() .'/constants.php' );
include( get_template_directory() .'/classes.php' );
include( get_template_directory() .'/widgets.php' );

add_theme_support( 'automatic-feed-links' );

if ( ! isset( $content_width ) ) $content_width = 900;

//remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wp_generator');

add_action( 'after_setup_theme', 'theme_localization' );
function theme_localization () {
	load_theme_textdomain( 'base', get_template_directory() . '/languages' );
}


if ( function_exists('register_sidebar') ) {
	/*register_sidebar(array(
		'id' => 'default-sidebar',
		'name' => __('Default Sidebar', 'base'),
		'before_widget' => '<div class="widget %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));*/
	
    register_sidebar(array(
        'name' => 'my_mega_menu',
        'before_widget' => '<div id="my-mega-menu-widget">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => '',
));

	register_sidebar(array(
		'id' => 'breadcrumbs',
		'name' => __('Breadcrumbs', 'base'),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	register_sidebar(array(
		'id' => 'search-sidebar',
		'name' => __('Search Sidebar', 'base'),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	register_sidebar(array(
		'id' => 'footer-sidebar',
		'name' => __('Footer Sidebar', 'base'),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	register_sidebar(array(
		'id' => 'archive-sidebar',
		'name' => __('Archive Sidebar', 'base'),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	register_sidebar(array(
		'id' => 'category-sidebar',
		'name' => __('Category Sidebar', 'base'),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
}

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 50, 50, true ); // Normal post thumbnails
	add_image_size( 'single-post-thumbnail', 400, 9999, true );
	add_image_size( '106x87', 106, 87, true );
	add_image_size( '413x367', 413, 367, true );
}

register_nav_menus( array(
	'primary' => __( 'Primary Navigation', 'base' ),
	'depts-menu-left' => __( 'Department Menu Left', 'base' ),
	'depts-menu-middle' => __( 'Department Menu Middle', 'base' ),
	'depts-menu-right' => __( 'Department Menu Right', 'base' ),
	'search-menu' => __( 'Search Navigation', 'base' ),
	'footer-menu' => __( 'Footer Navigation', 'base' ),
	'footer-menu-2' => __( 'Footer Navigation 2', 'base' ),
) );


//add [email]...[/email] shortcode
function shortcode_email($atts, $content) {
	$result = '';
	for ($i=0; $i<strlen($content); $i++) {
		$result .= '&#'.ord($content{$i}).';';
	}
	return $result;
}
add_shortcode('email', 'shortcode_email');

// register tag [template-url]
function filter_template_url($text) {
	return str_replace('[template-url]',get_bloginfo('template_url'), $text);
}
add_filter('the_content', 'filter_template_url');
add_filter('get_the_content', 'filter_template_url');
add_filter('widget_text', 'filter_template_url');

// register tag [site-url]
function filter_site_url($text) {
	return str_replace('[site-url]',get_bloginfo('url'), $text);
}
add_filter('the_content', 'filter_site_url');
add_filter('get_the_content', 'filter_site_url');
add_filter('widget_text', 'filter_site_url');


/* Replace Standart WP Menu Classes */
function change_menu_classes($css_classes) {
        $css_classes = str_replace("current-menu-item", "active", $css_classes);
        $css_classes = str_replace("current-menu-parent", "active", $css_classes);
        return $css_classes;
}
add_filter('nav_menu_css_class', 'change_menu_classes');


//allow tags in category description
$filters = array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description');
foreach ( $filters as $filter ) {
    remove_filter($filter, 'wp_filter_kses');
}


//Make WP Admin Menu HTML Valid
function wp_admin_bar_valid_search_menu( $wp_admin_bar ) {
	if ( is_admin() )
		return;

	$form  = '<form action="' . esc_url( home_url( '/' ) ) . '" method="get" id="adminbarsearch"><div>';
	$form .= '<input class="adminbar-input" name="s" id="adminbar-search" tabindex="10" type="text" value="" maxlength="150" />';
	$form .= '<input type="submit" class="adminbar-button" value="' . __('Search', 'base') . '"/>';
	$form .= '</div></form>';

	$wp_admin_bar->add_menu( array(
		'parent' => 'top-secondary',
		'id'     => 'search',
		'title'  => $form,
		'meta'   => array(
			'class'    => 'admin-bar-search',
			'tabindex' => -1,
		)
	) );
}
function fix_admin_menu_search() {
	remove_action( 'admin_bar_menu', 'wp_admin_bar_search_menu', 4 );
	add_action( 'admin_bar_menu', 'wp_admin_bar_valid_search_menu', 4 );
}
add_action( 'add_admin_bar_menus', 'fix_admin_menu_search' );

add_filter('post_thumbnail_html', 'theme_thumbnail_filter', 99, 5);
function theme_thumbnail_filter($html, $post_id, $post_thumbnail_id, $size, $attr){
	$html = preg_replace('#(width|height)=\"\d*\"\s#', "", $html);
	return $html;
}
add_filter('the_content', 'content_wrapper', 999);	
function content_wrapper($content) {
    return '<div class="wp-content">'.$content.'</div>';
}

// End PSD2HTML

function my_scripts_method()
{
	wp_enqueue_script(
		'fake-nav',
		get_template_directory_uri() . '/js/fake-nav.js',
		array('jquery')
	);
	
	wp_enqueue_script(
		'jquery-ui',
		get_template_directory_uri() . '/js/jquery.ui/jquery-ui-1.10.0.custom.min.js',
		array('jquery'),
		'1_10_0'
	);
	
	wp_enqueue_script(
		'theme-scripts',
		get_template_directory_uri() . '/js/scripts.js',
		array('jquery', 'jquery-ui'),
		'1_10_0'
	);
	
	// embed the javascript file that makes the AJAX request
	wp_enqueue_script( 
		'my-ajax-request', 
		get_template_directory_uri() . '/js/ajax.js', 
		array( 'jquery' ) 
	);
	/*
	global $is_IE;
	if ( $is_IE ) {
		wp_enqueue_script('psd2html_ie', get_template_directory_uri() . '/js/ie.js' );
		wp_enqueue_script('jquery.main', get_template_directory_uri() . '/js/jquery.main.ie.js', 'jquery' );
	} else {
		wp_enqueue_script('jquery.main', get_template_directory_uri() . '/js/jquery.main.js', 'jquery' );
	} */
	
	// declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)
	wp_localize_script( 'my-ajax-request', 'MyAjax', array( 
		'ajaxurl' => admin_url( 'admin-ajax.php' ), 
		'postCommentNonce' => wp_create_nonce( 'myajax-post-comment-nonce' ) 
		) 
	);
}
add_action('wp_enqueue_scripts', 'my_scripts_method');

function ciho_metrohealth_theme_styles()  
{ 
	wp_register_style( 
		'all', 
		get_template_directory_uri() . '/css/all.css', 
		array(), 
		'20130320', 
		'all' 
	);
	// wp_enqueue_style( 'all' );
	
	wp_register_style( 
		'style', 
		get_template_directory_uri() . '/style.css', 
		array(), 
		'20130320', 
		'all' 
	);
	// wp_enqueue_style( 'style' );
	
	global $is_IE;
	if ( $is_IE ) {
		wp_register_style( 
			'iecss', 
			get_template_directory_uri() . '/css/ie.css', 
			array(), 
			'20130320', 
			'iecss' 
		);
		// wp_enqueue_style( 'iecss' );
	}
}
add_action('wp_enqueue_scripts', 'ciho_metrohealth_theme_styles');

function set_excerpt_length($excerpt)
{
	$length = 160;
	return substr($excerpt, 0, $length).' [...] ';
}
// add_filter('the_excerpt', 'set_excerpt_length');


function append_form_if_enabled($content)
{
	global $post;
	$enabled = get_post_meta($post->ID, '_should_allow_questions', true);
	if ($enabled) $content .= $content . 
			sprintf('<a class="button" href="%s?post_id=%d&post_url=%s">Ask a Question/Report a Problem</a>', get_permalink(88), $post->ID, urlencode(get_permalink($post->ID)));
	return $content;
}
add_filter('the_content', 'append_form_if_enabled');

/**
 * Return space-separated css classes based on widget properties
 * 
 * @param object $widget_obj Metro_Widget
 * @return type 
 */
function component_properties_to_css_classes($widget_obj)
{
	$css_classes = array();
	if ($widget_obj->movable     === true)        $css_classes[] = 'movable';     else $css_classes[] = 'non-movable';
	if ($widget_obj->collapsible === true)        $css_classes[] = 'collapsible'; else $css_classes[] = 'non-collapsible';
	if ($widget_obj->state       === 'collapsed') $css_classes[] = 'mobile-hidden';   else $css_classes[] = 'active';
	return implode(' ', $css_classes);
}

function collapse_expand_links()
{
	echo ' <a class="collapse" href="#">-</a> <a class="expand" href="#">+</a>';
}

/**
 * If no excerpt is defined return the first 10-11 words of content
 * 
 * @param string $excerpt
 * @param string $content
 * @return string 
 */
function ciho_get_excerpt( $excerpt, $content ) {
	if (!empty($excerpt)) return apply_filters( 'the_excerpt', $excerpt );
	
	$content_a = explode( ' ', strip_tags( $content ) );
	$c = 0;
	$new_string_a = array();
	for ( $i = 1; $i <= count( $content_a ); $i++ ) {
		if ( ! isset( $content_a[ $i ] ) ) break;
		$new_string_a []= $content_a[ $i ];
		$c++;
		if ( $c > 10 ) break;
	}
	return implode( ' ', $new_string_a );
}

// Plugins rely on this
do_action('functions-php');


function fss_bp_adminbar_notifications_menu() {
global $bp;

if ( !is_user_logged_in() )
return false;


//_e( 'Alerts', 'buddypress' );

if ( $notifications = bp_core_get_notifications_for_user( $bp->loggedin_user->id ) ) { ?>
<sup><?php echo count( $notifications ) ?></sup>
    <?php
}

echo 'Updates';
echo '<ul>';

if ( $notifications ) {
    $counter = 0;
    for ( $i = 0; $i < count($notifications); $i++ ) { ?>
        <li><?php echo $notifications[$i] ?></li>
        <?php $counter++;
    }
} else { ?>
        <li>
            <?php _e( 'You have no new Updates', 'buddypress' ); ?>
        </li>
 
<?php
}

echo '</ul>';

}