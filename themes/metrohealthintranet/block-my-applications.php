<?php
$component_name = 'my-applications';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);
$application_images = MH_Application::get_images();
?>
<?php if ($component_properties->movable) { ?><article class="block <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h3>My Apps</h3>
		</div>
		
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
			<div class="portlet-content">
				<ul class="icons-list">
					<?php
					foreach ($component_properties->filter_val as $name => $url)
					{
						if (isset($application_images[$name]))
							printf('<li><a title="%s" href="%s" target="_blank"><img src="%s" /></a></li>', $name, $url, $application_images[$name]);
					}
					?>
				</ul>
				<p class="align-right pad"><a href="/profile-settings">Customize My Apps</a></p>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>

<!--
<article class="block active style01 mobile-hidden">
	<div class="heading-holder">
		<a href="#" class="opener">opener</a>
		<h3>My Apps</h3>
	</div>
	<div class="slide">
		<ul class="icons-list">
			<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/ico13.png" alt="image description"></a></li>
			<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/ico14.png" alt="image description"></a></li>
			<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/ico15.png" alt="image description"></a></li>
			<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/ico16.png" alt="image description"></a></li>
			<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/ico17.png" alt="image description"></a></li>
			<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/ico18.png" alt="image description"></a></li>
			<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/ico19.png" alt="image description"></a></li>
			<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/ico20.png" alt="image description"></a></li>
		</ul>
	</div>
</article>
-->