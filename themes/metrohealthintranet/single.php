<?php get_header();

dynamic_sidebar('breadcrumbs') ?>

<div class="column-main-sidenav">
	<?php get_sidebar('blog'); ?>
</div>
<div id="twocolumns">
	<div class="twocolumns-holder">
		<div class="column-inner">
			<div class="main-heading">
	   		<h2><?php echo get_the_title(get_the_ID()) ?></h2>
  		</div>
		<?php if(have_posts()) : the_post() ?>
	
		
			
			<?php the_content() ?>
			
			<div class="meta">
				<ul>
					<li><?php _e('Posted in', 'base'); ?> <?php the_category(', ') ?></li>
					<li><?php comments_popup_link(__('No Comments', 'base'), __('1 Comment', 'base'), __('% Comments', 'base')); ?></li>
					<?php the_tags(__('<li>Tags: ', 'base'), ', ', '</li>'); ?>
				</ul>
			</div>
			<div class="block-contact">
			<?php comments_template(); ?>
			
		</div>
		
	<?php endif ?>
		</div>
		<div class="column-inner">
			<article class="block active style02">
				<div class="heading-holder">
					<a href="#" class="opener">opener</a>
					<h3>How Do I?/ FAQ�s</h3>
				</div>
				<div class="slide">
					<div class="more-holder"><a href="#" class="more">Show FAQ�s</a></div>
				</div>
			</article>
			<article class="block active">
				<div class="heading-holder">
					<a href="#" class="opener">opener</a>
					<h3>Metro Moments</h3>
				</div>
				<div class="slide">
					<div class="slide-container">
						<h4>Interesting Award of <br />Accomplishment </h4>
						<p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis   <a href="#">READ MORE</a></p>
						<div class="col-holder">
							<div class="col">
								<p><a href="#">Submit</a></p>
								<p><a href="#">Metro Moment</a></p>
							</div>
							<div class="col">
								<p><a href="#">View All </a></p>
								<p><a href="#">Metro Moments</a></p>
							</div>
						</div>
					</div>
				</div>
			</article>
			<article class="block style03">
				<div class="heading-holder">
					<h3>HR Calendar of Events</h3>
				</div>
				<div class="calendar-placeholder">
					<img src="<?php echo get_template_directory_uri(); ?>/images/img-calendar-placeholder.gif" alt="image description">
				</div>
				<a href="#" class="more">View More HR Events</a>
			</article>
		</div>
	</div>
</div>

<?php get_footer(); ?>