                </div>
	</div>
	<footer id="footer">
		<div class="wrap">
			
                        <?php if(has_nav_menu('footer-menu'))
                                            wp_nav_menu( array('container' => false,
                                                     'theme_location' => 'footer-menu',
                                                     'depth' => 1,
                                                     'items_wrap' => '<ul>%3$s</ul>'."\n"
                                                    ) );
			
                        if(has_nav_menu('footer-menu-2'))
                                            wp_nav_menu( array('container' => false,
                                                     'theme_location' => 'footer-menu-2',
                                                     'depth' => 1,
                                                     'items_wrap' => '<ul>%3$s</ul>'."\n",
                                                     'walker' => new Custom_Walker_Nav_Menu
                                                    ) );                                
                        
                        dynamic_sidebar('footer-sidebar') ?>

		</div>
	</footer>
	<div class="skip"><a accesskey="B" href="#main">Back to Top</a></div>
        
        <?php wp_footer(); ?>
        
</body>
</html>