<?php get_header();

dynamic_sidebar('breadcrumbs') ?>

<div class="main-heading">
	<h2><?php _e('Not Found', 'base'); ?></h2>
</div>
<div id="twocolumns">
	<div class="block-contact">
		<p><?php _e('Sorry, but you are looking for something that isn\'t here.', 'base'); ?></p>
	</div>
</div>

<?php get_footer(); ?>