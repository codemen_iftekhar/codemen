<?php
$component_name = 'metro-moments';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);

$args = array( 
	'post_type'      => 'post',
	'cat'            => $component_properties->filter_val
);

?>
<?php if ($component_properties->movable) { ?><article class="block <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h3><?php echo get_cat_name($component_properties->filter_val) ?></h3>
		</div>
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
			<div class="portlet-content slide-holder">
				<?php

				query_posts($args);

				while( have_posts() ) 
				{
					the_post();
					?>
					
						<h4><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
						<?php
						the_excerpt();
						?>
					
					<?php
				}

				wp_reset_query();
				wp_reset_postdata();
				?>
			</div>
			<div class="more-holder">
				<a href="<?php echo @get_category_link($component_properties->filter_val); ?>" class="more">View All</a>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>

<!--
<article class="block active">
	<div class="heading-holder">
		<a href="#" class="opener">opener</a>
		<h3>Metro Moments</h3>
	</div>
	<div class="slide">
		<div class="slide-holder">
			<h4>Interesting Award of <br />Accomplishment </h4>
			<p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis   <a href="#">READ MORE</a></p>
			<div class="col-holder">
				<div class="col">
					<p><a href="#">Submit</a></p>
					<p><a href="#">Metro Moment</a></p>
				</div>
				<div class="col">
					<p><a href="#">View All </a></p>
					<p><a href="#">Metro Moments</a></p>
				</div>
			</div>
		</div>
	</div>
</article>
-->