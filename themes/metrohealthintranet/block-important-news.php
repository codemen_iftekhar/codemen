<?php
$component_name = 'important-news';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);
?>
<article class="immovable" id="<?php echo $component_name; ?>">
	<div class="portlet" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h2><?php echo get_cat_name($component_properties->filter_val) ?></h2>
		</div>
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
			<div class="portlet-content post-holder">
				<?php
				$in_args = array(
					'cat' => 8, 
					'showposts' => 0,
					'meta_query' => array(
						array(
							'key' => '_department_page',
							'value' => 'home',
							'compare' => '='
						)
					)
				);
				$in_query = new WP_Query( $in_args );
				
				while( $in_query->have_posts() ) {
					$in_query->next_post();
					?>
					<div class="post-snippet post">
						<h3><a href="<?php echo get_permalink( $in_query->post->ID ) ?>"><?php echo get_the_title( $in_query->post->ID ) ?></a></h3>
						<em class="date"><?php echo get_the_time( 'd/m/Y', $in_query->post->ID ); ?></em>
						<?php echo ciho_get_excerpt( $in_query->post->post_excerpt, $in_query->post->post_content ); ?>
					</div>
					<?php
				}

				wp_reset_query();
				wp_reset_postdata();
				?>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
</article>