<?php
$component_name = 'newshound';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);
?>
<?php if ($component_properties->movable) { ?><article class="block <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h3>Newshound / Ask a Question</h3>
		</div>
		
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
			<div class="portlet-content">
				<?php echo do_shortcode('[gravityform id="1" name="Newshound: Ask a Question" title="false" description="false" ajax="true"]'); ?>
				<a href="<?php echo get_category_link(64); ?>">View Newshound Responses</a>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>

<!--
<article class="block active style02">
	<div class="heading-holder">
		<a href="#" class="opener">opener</a>
		<h3>News Hound</h3>
	</div>
	<div class="slide">
		<div class="more-holder"><a href="#" class="more">Ask A Question</a></div>
	</div>
</article>
-->