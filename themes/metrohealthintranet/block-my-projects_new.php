<?php
$component_name = 'my-projects';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);
	
if (empty($component_properties))
	$filter_val = 0;
elseif($component_properties->filter_val==0)
	$filter_val = 0;
else
	$filter_val = $component_properties->filter_val;

?>
<?php if ($component_properties->movable) { ?><article class="box style01 <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h2>My Groups</h2>
		</div>
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
		
			<div class="portlet-content">
				<?php 
				// if ( bp_has_activities( bp_ajax_querystring( 'activity' ) ) )

				$do_it = false;
				if ($filter_val == 0) 
				{
					if ( bp_has_activities( 'type=activity_update&object=groups' ) ) $do_it = true;
				}
				else 
				{
					if ( bp_has_activities( 'type=activity_update&object=groups&primary_id='.$filter_val ) ) $do_it = true;
				}

				?>
				


				<?php
				if ( $do_it )
				{
					$i = 0;
					while ( bp_activities() ) 
					{
						$i++;
						bp_the_activity();
                                                
						//locate_template( array( 'activity/entry.php' ), true, false );
                                                ?>
                                                <div class="activity-content">

                                                    <div class="activity-header">

                                                            <?php 
                                                           global $activities_template;

                                                        $action = $activities_template->activity->action;
                                                        //$action = apply_filters_ref_array( 'bp_get_activity_action_pre_meta', array( $action, &$activities_template->activity ) );

                                                        if ( !empty( $action ) )
                                                                $action = bp_insert_activity_meta( $action );

                                                        echo apply_filters_ref_array( 'bp_get_activity_action', array( $action, &$activities_template->activity ) );
                                                            //bp_activity_action(); 
                                                        
                                                        
                                                            //echo "<pre>";
                                                           //var_dump($activities_template);
                                                            //echo "</pre>";
                                                             $action = $activities_template->activity->action;
                                                           // $str = preg_replace('/<a.*?<\/a>/', '', $action, 1); 
                                                           echo $str = preg_replace('/<a[^>]*>(.*?)<\/a>/', '$1', $action, 2);
                                                           //preg_match('/\shref="(?<href>[^"]+)"/', $str, $match);
                                                            //$gurl=$match[1];
                                                          //echo $str2 = preg_replace('/<a[^>]*>(.*?)<\/a>/', '$1', $str, 1);
                                                          echo $activity_meta = bp_insert_activity_meta('');
                                                        
                                                        
                                                        
                                                        ?>

                                                    </div>

                                                    <?php if ( 'activity_comment' == bp_get_activity_type() ) : ?>

                                                            <div class="activity-inreplyto">
                                                                    <strong><?php _e( 'In reply to: ', 'buddypress' ); ?></strong><?php bp_activity_parent_content(); ?> <a href="<?php bp_activity_thread_permalink(); ?>" class="view" title="<?php _e( 'View Thread / Permalink', 'buddypress' ); ?>"><?php _e( 'View', 'buddypress' ); ?></a>
                                                            </div>

                                                    <?php endif; ?>

                                                    <?php if ( bp_activity_has_content() ) : ?>

                                                            <div class="activity-inner">

                                                                    <?php bp_activity_content_body(); ?>

                                                            </div>

                                                    <?php endif; ?>

                                                    <?php do_action( 'bp_activity_entry_content' ); ?>

                                                    <?php if ( is_user_logged_in() ) : ?>

                                                            <div class="activity-meta">

                                                                    <?php if ( bp_activity_can_comment() ) : ?>

                                                                            <a href="<?php bp_get_activity_comment_link(); ?>" class="button acomment-reply bp-primary-action" id="acomment-comment-<?php bp_activity_id(); ?>"><?php printf( __( 'Comment <span>%s</span>', 'buddypress' ), bp_activity_get_comment_count() ); ?></a>

                                                                    <?php endif; ?>

                                                                    <?php if ( bp_activity_can_favorite() ) : ?>

                                                                            <?php if ( !bp_get_activity_is_favorite() ) : ?>

                                                                                    <a href="<?php bp_activity_favorite_link(); ?>" class="button fav bp-secondary-action" title="<?php esc_attr_e( 'Mark as Favorite', 'buddypress' ); ?>"><?php _e( 'Favorite', 'buddypress' ); ?></a>

                                                                            <?php else : ?>

                                                                                    <a href="<?php bp_activity_unfavorite_link(); ?>" class="button unfav bp-secondary-action" title="<?php esc_attr_e( 'Remove Favorite', 'buddypress' ); ?>"><?php _e( 'Remove Favorite', 'buddypress' ); ?></a>

                                                                            <?php endif; ?>

                                                                    <?php endif; ?>

                                                                    <?php if ( bp_activity_user_can_delete() ) bp_activity_delete_link(); ?>

                                                                    <?php do_action( 'bp_activity_entry_meta' ); ?>

                                                            </div>

                                                    <?php endif; ?>

                                            </div>
                                                <?php
						if ($i>1) break;
					}
				} 
				else
				{
					print('<p>No activity</p>');
				}
				?>
				<p class="align-right"><a href="/profile-settings">Customize My Projects</a></p>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>

<!--
<article class="box active style01">
				<div class="heading-holder">
					<a href="#" class="opener">opener</a>
					<h2>My Groups & Projects</h2>
				</div>
				<div class="slide">
					<ul class="info-list">
						<li>
							<h3><a href="#">You were added to "Community Ourreach"</a></h3>
							<em class="date">Added 2 Hours ago </em>
							<p>Brna nibh, blandit ut mattis vel, elementum iaculis. Brna nibh, blandit ut mattis vel, elementum iaculis. <a href="#">VIEW PROJECT</a></p>
						</li>
						<li>
							<h3><a href="#">You created an event "Metro at the Zoo"</a></h3>
							<em class="date">Added 8 Hours ago </em>
							<p>Brna nibh, blandit ut mattis vel, elementum iaculis.  Brna nibh, blandit ut mattis vel, elementum iaculis. <a href="#">VIEW PROJECT</a></p>
						</li>
						<li>
							<h3><a href="#">You were assigned a task "Contact Dr. Thomson"</a></h3>
							<em class="date">Added 2 Days ago </em>
							<p>Brna nibh, blandit ut mattis vel, elementum iaculis.Brna nibh, blandit ut mattis vel, elementum iaculis. <a href="#">VIEW PROJECT</a></p>
						</li>
					</ul>
					<div class="more-holder">
						<a href="#" class="more alignright">Add Project</a>
						<a href="#" class="more alignleft">View All My Projects</a>
					</div>
				</div>
			</article>
-->