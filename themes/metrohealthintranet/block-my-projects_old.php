<?php
$component_name = 'my-projects';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);
	
if (empty($component_properties))
	$filter_val = 0;
elseif($component_properties->filter_val==0)
	$filter_val = 0;
else
	$filter_val = $component_properties->filter_val;

?>
<?php if ($component_properties->movable) { ?><article class="box style01 <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h2>My Groups</h2>
		</div>
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
		
			<div class="portlet-content">
				<?php 
				// if ( bp_has_activities( bp_ajax_querystring( 'activity' ) ) )

				$do_it = false;
				if ($filter_val == 0) 
				{
					if ( bp_has_activities( 'type=activity_update&object=groups' ) ) $do_it = true;
				}
				else 
				{
					if ( bp_has_activities( 'type=activity_update&object=groups&primary_id='.$filter_val ) ) $do_it = true;
				}

				?>
				<form method="post" id="<?php echo $component_name; ?>_filter_val_form">
				<p>Filter Projects: 
				<select name="filter_val" class="ajax_submit_on_change" rel="<?php echo $component_name; ?>">
				<?php 
				if ( bp_has_groups() )
				{
					if ($filter_val == 0) 
						printf('<option value="%d" selected="selected" />%s</option>', 0, 'All');
					else
						printf('<option value="%d" />%s</option>', 0, 'All');

					while ( bp_groups() ) 
					{
						bp_the_group();

						if ($filter_val == bp_get_group_id())
							printf('<option value="%d" selected="selected" />%s</option>', bp_get_group_id(), bp_get_group_name());
						else
							printf('<option value="%d" />%s</option>', bp_get_group_id(), bp_get_group_name());
					}
				}
				?>
				</select></p>
				<input type="hidden" name="component_id" value="<?php echo $component_name; ?>" />
				<input type="hidden" name="action" value="single_widget_action" />
				</form>


				<?php
				if ( $do_it )
				{
					$i = 0;
					while ( bp_activities() ) 
					{
						$i++;
						bp_the_activity();
						locate_template( array( 'activity/entry.php' ), true, false );
						if ($i>1) break;
					}
				} 
				else
				{
					print('<p>No activity</p>');
				}
				?>
				<p class="align-right"><a href="/profile-settings">Customize My Projects</a></p>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>

<!--
<article class="box active style01">
				<div class="heading-holder">
					<a href="#" class="opener">opener</a>
					<h2>My Groups & Projects</h2>
				</div>
				<div class="slide">
					<ul class="info-list">
						<li>
							<h3><a href="#">You were added to "Community Ourreach"</a></h3>
							<em class="date">Added 2 Hours ago </em>
							<p>Brna nibh, blandit ut mattis vel, elementum iaculis. Brna nibh, blandit ut mattis vel, elementum iaculis. <a href="#">VIEW PROJECT</a></p>
						</li>
						<li>
							<h3><a href="#">You created an event "Metro at the Zoo"</a></h3>
							<em class="date">Added 8 Hours ago </em>
							<p>Brna nibh, blandit ut mattis vel, elementum iaculis.  Brna nibh, blandit ut mattis vel, elementum iaculis. <a href="#">VIEW PROJECT</a></p>
						</li>
						<li>
							<h3><a href="#">You were assigned a task "Contact Dr. Thomson"</a></h3>
							<em class="date">Added 2 Days ago </em>
							<p>Brna nibh, blandit ut mattis vel, elementum iaculis.Brna nibh, blandit ut mattis vel, elementum iaculis. <a href="#">VIEW PROJECT</a></p>
						</li>
					</ul>
					<div class="more-holder">
						<a href="#" class="more alignright">Add Project</a>
						<a href="#" class="more alignleft">View All My Projects</a>
					</div>
				</div>
			</article>
-->