<?php
$component_name = 'my-connections';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);
?>
<?php if ($component_properties->movable) { ?><article class="box style01 <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h2>My Connections</h2>
		</div>
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
			<div class="portlet-content">
				<?php 
				// if ( bp_has_activities( bp_ajax_querystring( 'activity' ) ) )
				if ( bp_has_activities( 'type=activity_update&object=friends,profile,status' ) )
				{
					$i = 0;
					while ( bp_activities() ) 
					{
						$i++;
						bp_the_activity();
						locate_template( array( 'activity/entry.php' ), true, false );
						if ($i>1) break;
					}
				} 
				else
				{
					print('<p>No activity</p>');
				}
				?>
				<!-- <p class="align-right"><a href="/profile-settings">Customize My Connections</a></p> -->
				<p class="align-right"><a href="/members/admin/activity/friends/">See All Activity</a></p>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>

<!--

<article class="box active style01">
				<div class="heading-holder">
					<a href="#" class="opener">opener</a>
					<h2>My Connections</h2>
				</div>
				<div class="slide">
					<ul class="comments-list">
						<li>
							<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/img6.jpg" alt="image description"></div>
							<div class="text-holder">
								<h3><a href="#">Kimberly Surname - 6 minutes ago</a></h3>
								<p> ut mattis vel, elementum iaculis. Brna vel, elementum iaculis. Brna</p>
							</div>
						</li>
						<li>
							<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/img7.jpg" alt="image description"></div>
							<div class="text-holder">
								<h3><a href="#">Thomas Pryce - 2 hours ago</a></h3>
								<p>ut mattis vel, elementum iaculis. Brna el, elementum iaculis. Brnanibh, blandit ut mattis vel, </p>
							</div>
						</li>
						<li>
							<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/img8.jpg" alt="image description"></div>
							<div class="text-holder">
								<h3><a href="#">Dr. Keith - 2 days ago</a></h3>
								<p>Added 2 Hours ago <br />Brna nibh, blandit ut mattis vel, elementum iaculis. Brnanibh, blandit ut mattis vel, elementum iacu</p>
							</div>
						</li>
					</ul>
					<div class="more-holder">
						<a href="#" class="more alignright">Add Conection</a>
						<a href="#" class="more alignleft">View All My Connections</a>
					</div>
				</div>
			</article>
-->
