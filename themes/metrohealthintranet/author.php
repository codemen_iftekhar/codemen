<?php
get_header();

dynamic_sidebar('breadcrumbs') ?>

<div class="column-main-sidenav">
	<?php get_sidebar(); ?>
</div>
<div id="twocolumns">
	<div class="twocolumns-holder">
		<div class="column-inner">
			<div class="main-heading">				
				<?php $author = apply_filters('ciho_hydrate_3rd_party_data', get_userdata( get_query_var('author') ) ); ?>
				<h2><?php echo $author->first_name; ?> <?php echo $author->last_name; ?></h2>
				<p>Title: <?php echo $author->adi_title; ?></p>
				<p>Department: <?php echo $author->idm_data->department_name; ?></p>
				<p>Phone: <?php echo $author->adi_telephonenumber; ?></p>
			</div>
		
		</div>
		<div class="column-inner">
			<?php get_sidebar('right'); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>