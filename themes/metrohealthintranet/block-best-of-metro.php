<?php
$component_name = 'best-of-metro';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);

$args = array( 
	'post_type'      => 'post',
	'cat'            =>  $component_properties->filter_val,
	'posts_per_page' => 2
);
?>
<?php if ($component_properties->movable) { ?><article class="box <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h2><?php echo get_cat_name($component_properties->filter_val) ?></h2>
		</div>
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
			<div class="portlet-content post-holder">
				<?php

				query_posts($args);

				while( have_posts() ) 
				{
					the_post();
					?>
					<div class="post-snippet post">
						<h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
						<em class="date"><?php the_time('d/m/Y') ?> <?php the_tags(__('TAGS: ', 'base'), ', ', ''); ?> </em>
						<?php
						the_excerpt();
						?>
					</div>
					<?php
				}

				wp_reset_query();
				wp_reset_postdata();
				?>
			</div>
			<div class="more-holder">
				<a href="<?php echo @get_category_link($component_properties->filter_val); ?>" class="more">View All</a>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>