<?php 
$component_name = 'my-metro-updates'; 
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);

$categories_a = get_categories(array('include' => $component_properties->options, 'hide_empty' => 0));

if ($component_properties->filter_val == $component_properties->options)
	$cat_dropdown_options = sprintf('<option value="%s" selected="selected">All</option>', $component_properties->options);
else
	$cat_dropdown_options = sprintf('<option value="%s">All</option>', $component_properties->options);

foreach ($categories_a as $category)
{
	$cat_dropdown_options .= sprintf('<option value="%d"', (int) $category->term_id);
	if ((string) $category->term_id === (string) $component_properties->filter_val) $cat_dropdown_options .= ' selected="selected" ';
	$cat_dropdown_options .= sprintf('>%s</option>', $category->name);
}

?>
<?php if ($component_properties->movable) { ?><article class="box <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h2>My Metro Updates</h2>
		</div>
		
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
			<div class="portlet-content post-holder">
				<form method="post" id="<?php echo $component_name; ?>_filter_val_form">
					<p>Show: 
						<select name="filter_val" class="ajax_submit_on_change" rel="<?php echo $component_name; ?>">
							<?php echo $cat_dropdown_options; ?>
						</select>
						<input type="hidden" name="component_id" value="<?php echo $component_name; ?>" />
						<input type="hidden" name="action" value="single_widget_action" />
					</p>
				</form>

				<?php
				$args = array( 
					'post_type'      => 'post',
					'cat'            => $component_properties->filter_val,
					'posts_per_page' => 2
				);

				query_posts($args);

				while( have_posts() ) 
				{
					the_post();
					?>
					<div class="post-snippet post">
						<h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
						<em class="date"><?php the_time('d/m/Y') ?> <?php // the_tags(__('TAGS: ', 'base'), ', ', ''); ?> </em>
						<?php
						the_excerpt();
						?>
					</div>
					<?php
				}

				wp_reset_query();
				wp_reset_postdata()
				?>
					
			</div>
			<div class="more-holder">
				<a href="<?php echo @get_category_link($component_properties->filter_val); ?>" class="more">View All</a> |
				<a href="/profile-settings" class="more">Customize My News</a>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>