jQuery(function() {
	// jQuery('a.nav-link').hover(show_nav_panel(jQuery(this).attr('id')), hide_nav_panel(jQuery(this).attr('id')));
	jQuery('a.nav-link').click(function(e) {e.preventDefault();show_nav_panel(jQuery(this).attr('id'))});
	jQuery('.nav-panel').append('<br /><a href="#" class="nav-close">[x] Close</a>');	
	jQuery('a.nav-close').click(function(e) {e.preventDefault();hide_nav_panel()});
});

function show_nav_panel(id)
{
	hide_nav_panel();
	jQuery('#'+id+'-panel').removeClass('nav-hidden').addClass('nav-visible');
}

function hide_nav_panel()
{
	jQuery('.nav-panel').removeClass('nav-visible').addClass('nav-hidden');
}