jQuery(function() {
	
	// START Ajax ordering elements on home profile page
	jQuery( ".home-page .column" ).sortable({
		 items: "article:not(.immovable)"
	});
	// jQuery( ".column" ).disableSelection();
	
	jQuery( ".home-page .column" ).on( "sortupdate", function( event, ui ) {
		var new_order = '';
		jQuery(ui.item).parent().find('article').each(function(index,obj){
			if (new_order.length > 0) 
			{
				new_order = new_order + ',' + jQuery(obj).attr('id');
			}
			else
			{
				new_order = jQuery(obj).attr('id');
			}
		});
				
		jQuery.post(
			MyAjax.ajaxurl,
			{
				action : 'myajax-save-order',
				newOrder : new_order,
				columnID : jQuery(ui.item).parent().attr('column_id'),
				postCommentNonce : MyAjax.postCommentNonce
			},
			function( response ) {
				// alert( response.the_new_order );
				// alert( response.column_id );
			}
		);
		
		// alert('The new order of ' + jQuery(ui.item).parent().attr('column_id') + ' would be: ' + new_order);
	});
	// END Ajax ordering elements on home profile page
	
	// START Bookmark This
	jQuery('.save_bookmark').click(function(e){
		e.preventDefault();
		jQuery.post(
			MyAjax.ajaxurl,
			{
				action : 'myajax-save-link',
				postID : jQuery(this).attr('postID'),
				postCommentNonce : MyAjax.postCommentNonce
			},
			function( response ) {
				mh_show_notification_message('We added a link to this page to your "My Links" area.');
				// alert( response.post_id );
				// alert( response.column_id );
			}
		);
	});
	// END Bookmark This
	
	// START Delete This Bookmark
	jQuery('.delete_bookmark').click(function(e){
		e.preventDefault();
		var my_delete_link = jQuery(this);
		jQuery.post(
			MyAjax.ajaxurl,
			{
				action : 'myajax-delete-link',
				postID : jQuery(this).attr('rel'),
				postCommentNonce : MyAjax.postCommentNonce
			},
			function( response ) {
				jQuery(my_delete_link).parent().parent().parent().hide('slow');
				// alert( response.post_id );
				// alert( response.column_id );
			}
		);
	});
	// END Delete This Bookmark
	
	// START Collapse/Expand Component
	jQuery('a.opener').click(function(e){
		e.preventDefault();
		
		var component_id = jQuery(this).closest('article').attr('id');
		
		if (jQuery('article#'+component_id).hasClass('active'))
		{
			jQuery('article#'+component_id).removeClass('active').addClass('mobile-hidden');
			jQuery.post(
				MyAjax.ajaxurl,
				{
					action : 'myajax-collapse-component',
					componentID : component_id,
					postCommentNonce : MyAjax.postCommentNonce
				},
				function( response ) {
					// alert( response.collapsed_component );
					// alert( response.column_id );
				}
			);
		}
		else
		{
			jQuery('article#'+component_id).removeClass('mobile-hidden').addClass('active');
			jQuery.post(
				MyAjax.ajaxurl,
				{
					action : 'myajax-expand-component',
					componentID : component_id,
					postCommentNonce : MyAjax.postCommentNonce
				},
				function( response ) {
					// alert( response.expanded_component );
					// alert( response.column_id );
				}
			);
		}
	});
	// END Collapse/Expand Component
	
	// START Dropdown to submit hidden form for each component
	jQuery(".ajax_submit_on_change").change(function(){
		jQuery('form#'+jQuery(this).attr('rel')+'_filter_val_form').submit();
	});
	// END Dropdown to submit hidden form for each component
	
	// START Dropdown to redirect
	jQuery(".redirect_on_change").change(function(){
		var redirect_to = jQuery(this).attr('value');
		if (redirect_to.length>1) window.location=redirect_to;
	});
	// END Dropdown to redirect

	// START Handle confirmation message
	// jQuery("body").find('#hidden_temporary_message').attr('rel');
	
	if (jQuery('#hidden_temporary_message').is('*'))
	{
		jQuery('#hidden_temporary_message').show('slow').delay(3000).hide('slow');
	}
	// END Handle confirmation message
});

function mh_show_notification_message(my_text)
{
	jQuery('#hidden_temporary_message_placeholder').html('<div id="hidden_temporary_message" class="success">'+my_text+'</div>');
	jQuery('#hidden_temporary_message_placeholder div').show('slow').delay(3000).hide('slow');
}