jQuery(function() {
	jQuery('#save_test_properties').click(function(e){
		e.preventDefault();
		jQuery.post(
			MyAjax.ajaxurl,
			{
				action : 'myajax-save-properties',
				newProperties : jQuery('#_properties').val(),
				componentID : jQuery('#_component_id').val(),
				postCommentNonce : MyAjax.postCommentNonce
			},
			function( response ) {
				alert( response.the_new_properties );
				alert( response.component_id );
			}
		);
	});
});