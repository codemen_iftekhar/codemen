// page init
jQuery(function(){
	initCycleCarousel();
	initCarousel();
	initInputs();
	FontResize.init();
	initTouchNav();
	initDropDownClasses();
	initPopups();
	initOpenClose();
});

// open-close init
function initOpenClose() {
	jQuery('.has-drop-down').openClose({
		activeClass: 'active',
		opener: '.has-drop-down-a',
		slider: '.drop',
		effect: 'none'
	});
}

/*
 * jQuery Open/Close plugin
 */
;(function($) {
	function OpenClose(options) {
		this.options = $.extend({
			addClassBeforeAnimation: true,
			activeClass:'active',
			opener:'.opener',
			slider:'.slide',
			animSpeed: 400,
			effect:'fade',
			event:'click'
		}, options);
		this.init();
	}
	OpenClose.prototype = {
		init: function() {
			if(this.options.holder) {
				this.findElements();
				this.attachEvents();
				this.makeCallback('onInit');
			}
		},
		findElements: function() {
			this.holder = $(this.options.holder);
			this.opener = this.holder.find(this.options.opener);
			this.slider = this.holder.find(this.options.slider);
			
			if (!this.holder.hasClass(this.options.activeClass)) {
				this.slider.addClass(slideHiddenClass);
			}
		},
		attachEvents: function() {
			// add handler
			var self = this;
			this.eventHandler = function(e) {
				e.preventDefault();
				if (self.slider.hasClass(slideHiddenClass)) {
					self.showSlide();
				} else {
					self.hideSlide();
				}
			};
			self.opener.bind(self.options.event, this.eventHandler);

			// hove mode handler
			if(self.options.event === 'over') {
				self.opener.bind('mouseenter', function() {
					self.holder.removeClass(self.options.activeClass);
					self.opener.trigger(self.options.event);
				});
				self.holder.bind('mouseleave', function() {
					self.holder.addClass(self.options.activeClass);
					self.opener.trigger(self.options.event);
				});
			}
		},
		showSlide: function() {
			var self = this;
			if (self.options.addClassBeforeAnimation) {
				self.holder.addClass(self.options.activeClass);
			}
			self.slider.removeClass(slideHiddenClass);

			self.makeCallback('animStart', true);
			toggleEffects[self.options.effect].show({
				box: self.slider,
				speed: self.options.animSpeed,
				complete: function() {
					if (!self.options.addClassBeforeAnimation) {
						self.holder.addClass(self.options.activeClass);
					}
					self.makeCallback('animEnd', true);
				}
			});
		},
		hideSlide: function() {
			var self = this;
			if (self.options.addClassBeforeAnimation) {
				self.holder.removeClass(self.options.activeClass);
			}
			
			self.makeCallback('animStart', false);
			toggleEffects[self.options.effect].hide({
				box: self.slider,
				speed: self.options.animSpeed,
				complete: function() {
					if (!self.options.addClassBeforeAnimation) {
						self.holder.removeClass(self.options.activeClass);
					}
					self.slider.addClass(slideHiddenClass);
					self.makeCallback('animEnd', false);
				}
			});
		},
		destroy: function() {
			this.slider.removeClass(slideHiddenClass);
			this.opener.unbind(this.options.event, this.eventHandler);
			this.holder.removeClass(this.options.activeClass).removeData('OpenClose');
		},
		makeCallback: function(name) {
			if(typeof this.options[name] === 'function') {
				var args = Array.prototype.slice.call(arguments);
				args.shift();
				this.options[name].apply(this, args);
			}
		}
	};
	
	// add stylesheet for slide on DOMReady
	var slideHiddenClass = 'js-slide-hidden';
	$(function() {
		var tabStyleSheet = $('<style type="text/css">')[0];
		var tabStyleRule = '.' + slideHiddenClass;
		tabStyleRule += '{position:absolute !important;left:-9999px !important;top:-9999px !important;display:block !important}';
		if (tabStyleSheet.styleSheet) {
			tabStyleSheet.styleSheet.cssText = tabStyleRule;
		} else {
			tabStyleSheet.appendChild(document.createTextNode(tabStyleRule));
		}
		$('head').append(tabStyleSheet);
	});
	
	// animation effects
	var toggleEffects = {
		slide: {
			show: function(o) {
				o.box.stop(true).hide().slideDown(o.speed, o.complete);
			},
			hide: function(o) {
				o.box.stop(true).slideUp(o.speed, o.complete);
			}
		},
		fade: {
			show: function(o) {
				o.box.stop(true).hide().fadeIn(o.speed, o.complete);
			},
			hide: function(o) {
				o.box.stop(true).fadeOut(o.speed, o.complete);
			}
		},
		none: {
			show: function(o) {
				o.box.hide().show(0, o.complete);
			},
			hide: function(o) {
				o.box.hide(0, o.complete);
			}
		}
	};
	
	// jQuery plugin interface
	$.fn.openClose = function(opt) {
		return this.each(function() {
			jQuery(this).data('OpenClose', new OpenClose($.extend(opt, {holder: this})));
		});
	};
}(jQuery));

// handle dropdowns on mobile devices
function initTouchNav() {
	lib.each(lib.queryElementsBySelector('#nav'), function(){
		new TouchNav({
			navBlock: this
		});
	});
}

// add classes if item has dropdown
function initDropDownClasses() {
	jQuery('#nav li').each(function() {
		var item = jQuery(this);
		var drop = item.find('.drop');
		var link = item.find('a').eq(0);
		if(drop.length) {
			item.addClass('has-drop-down');
			if(link.length) link.addClass('has-drop-down-a');
		}
	});
}

// navigation accesibility module
function TouchNav(opt) {
	this.options = {
		hoverClass: 'hover',
		menuItems: 'li',
		menuOpener: 'a',
		menuDrop: 'ul',
		navBlock: null
	};
	for(var p in opt) {
		if(opt.hasOwnProperty(p)) {
			this.options[p] = opt[p];
		}
	}
	this.init();
}
TouchNav.isActiveOn = function(elem) {
	return elem && elem.touchNavActive;
};
TouchNav.prototype = {
	init: function() {
		if(typeof this.options.navBlock === 'string') {
			this.menu = document.getElementById(this.options.navBlock);
		} else if(typeof this.options.navBlock === 'object') {
			this.menu = this.options.navBlock;
		}
		if(this.menu) {
			this.addEvents();
		}
	},
	addEvents: function() {
		// attach event handlers
		var self = this;
		this.menuItems = lib.queryElementsBySelector(this.options.menuItems, this.menu);

		for(var i = 0; i < this.menuItems.length; i++) {
			(function(i){
				var item = self.menuItems[i],
					currentDrop = lib.queryElementsBySelector(self.options.menuDrop, item)[0],
					currentOpener = lib.queryElementsBySelector(self.options.menuOpener, item)[0];

				// only for touch input devices
				if( (self.isTouchDevice || navigator.msPointerEnabled) && currentDrop && currentOpener) {
					lib.event.add(currentOpener, 'click', lib.bind(self.clickHandler, self));
					lib.event.add(currentOpener, navigator.msPointerEnabled ? 'MSPointerDown' : 'touchstart', function(e){
						if(navigator.msPointerEnabled && e.pointerType !== e.MSPOINTER_TYPE_TOUCH) return;
						self.touchFlag = true;
						self.currentItem = item;
						self.currentLink = currentOpener;
						self.pressHandler.apply(self, arguments);
					});
				}
				// for desktop computers and touch devices
				lib.event.add(item, 'mouseover', function(){
					if(!self.touchFlag) {
						self.currentItem = item;
						self.mouseoverHandler();
					}
				});
				lib.event.add(item, 'mouseout', function(){
					if(!self.touchFlag) {
						self.currentItem = item;
						self.mouseoutHandler();
					}
				});
				item.touchNavActive = true;
			})(i);
		}

		// hide dropdowns when clicking outside navigation
		if(this.isTouchDevice || navigator.msPointerEnabled) {
			lib.event.add(document, navigator.msPointerEnabled ? 'MSPointerDown' : 'touchstart', lib.bind(this.clickOutsideHandler, this));
		}
	},
	mouseoverHandler: function() {
		lib.addClass(this.currentItem, this.options.hoverClass);
		jQuery(this.currentItem).trigger('itemhover');
	},
	mouseoutHandler: function() {
		lib.removeClass(this.currentItem, this.options.hoverClass);
		jQuery(this.currentItem).trigger('itemleave');
	},
	hideActiveDropdown: function() {
		for(var i = 0; i < this.menuItems.length; i++) {
			if(lib.hasClass(this.menuItems[i], this.options.hoverClass)) {
				lib.removeClass(this.menuItems[i], this.options.hoverClass);
				jQuery(this.menuItems[i]).trigger('itemleave');
			}
		}
		this.activeParent = null;
	},
	pressHandler: function(e) {
		// hide previous drop (if active)
		if(this.currentItem != this.activeParent && !this.isParent(this.activeParent, this.currentLink)) {
			this.hideActiveDropdown();
		}
		// handle current drop
		this.activeParent = this.currentItem;
		if(lib.hasClass(this.currentItem, this.options.hoverClass)) {
			this.preventCurrentClick = false;
		} else {
			e.preventDefault();
			this.preventCurrentClick = true;
			lib.addClass(this.currentItem, this.options.hoverClass);
			jQuery(this.currentItem).trigger('itemhover');
		}
	},
	clickHandler: function(e) {
		// prevent first click on link
		if(this.preventCurrentClick) {
			e.preventDefault();
		}
	},
	clickOutsideHandler: function(event) {
		if(navigator.msPointerEnabled && event.pointerType !== event.MSPOINTER_TYPE_TOUCH) return;
		var e = event.changedTouches ? event.changedTouches[0] : event;
		if(this.activeParent && !this.isParent(this.menu, e.target)) {
			this.hideActiveDropdown();
			this.touchFlag = false;
		}
	},
	isParent: function(parent, child) {
		while(child.parentNode) {
			if(child.parentNode == parent) {
				return true;
			}
			child = child.parentNode;
		}
		return false;
	},
	isTouchDevice: (function() {
		try {
			return (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) || navigator.userAgent.indexOf('IEMobile') != -1;
		} catch (e) {
			return false;
		}
	}())
};

/*
 * Utility module
 */
lib = {
	hasClass: function(el,cls) {
		return el && el.className ? el.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)')) : false;
	},
	addClass: function(el,cls) {
		if (el && !this.hasClass(el,cls)) el.className += " "+cls;
	},
	removeClass: function(el,cls) {
		if (el && this.hasClass(el,cls)) {el.className=el.className.replace(new RegExp('(\\s|^)'+cls+'(\\s|$)'),' ');}
	},
	extend: function(obj) {
		for(var i = 1; i < arguments.length; i++) {
			for(var p in arguments[i]) {
				if(arguments[i].hasOwnProperty(p)) {
					obj[p] = arguments[i][p];
				}
			}
		}
		return obj;
	},
	each: function(obj, callback) {
		var property, len;
		if(typeof obj.length === 'number') {
			for(property = 0, len = obj.length; property < len; property++) {
				if(callback.call(obj[property], property, obj[property]) === false) {
					break;
				}
			}
		} else {
			for(property in obj) {
				if(obj.hasOwnProperty(property)) {
					if(callback.call(obj[property], property, obj[property]) === false) {
						break;
					}
				}
			}
		}
	},
	event: (function() {
		var fixEvent = function(e) {
			e = e || window.event;
			if(e.isFixed) return e; else e.isFixed = true;
			if(!e.target) e.target = e.srcElement;
			e.preventDefault = e.preventDefault || function() {this.returnValue = false;};
			e.stopPropagation = e.stopPropagation || function() {this.cancelBubble = true;};
			return e;
		};
		return {
			add: function(elem, event, handler) {
				if(!elem.events) {
					elem.events = {};
					elem.handle = function(e) {
						var ret, handlers = elem.events[e.type];
						e = fixEvent(e);
						for(var i = 0, len = handlers.length; i < len; i++) {
							if(handlers[i]) {
								ret = handlers[i].call(elem, e);
								if(ret === false) {
									e.preventDefault();
									e.stopPropagation();
								}
							}
						}
					};
				}
				if(!elem.events[event]) {
					elem.events[event] = [];
					if(elem.addEventListener) elem.addEventListener(event, elem.handle, false);
					else if(elem.attachEvent) elem.attachEvent('on'+event, elem.handle);
				}
				elem.events[event].push(handler);
			},
			remove: function(elem, event, handler) {
				var handlers = elem.events[event];
				for(var i = handlers.length - 1; i >= 0; i--) {
					if(handlers[i] === handler) {
						handlers.splice(i,1);
					}
				}
				if(!handlers.length) {
					delete elem.events[event];
					if(elem.removeEventListener) elem.removeEventListener(event, elem.handle, false);
					else if(elem.detachEvent) elem.detachEvent('on'+event, elem.handle);
				}
			}
		};
	}()),
	queryElementsBySelector: function(selector, scope) {
		scope = scope || document;
		if(!selector) return [];
		if(selector === '>*') return scope.children;
		if(typeof document.querySelectorAll === 'function') {
			return scope.querySelectorAll(selector);
		}
		var selectors = selector.split(',');
		var resultList = [];
		for(var s = 0; s < selectors.length; s++) {
			var currentContext = [scope || document];
			var tokens = selectors[s].replace(/^\s+/,'').replace(/\s+$/,'').split(' ');
			for (var i = 0; i < tokens.length; i++) {
				token = tokens[i].replace(/^\s+/,'').replace(/\s+$/,'');
				if (token.indexOf('#') > -1) {
					var bits = token.split('#'), tagName = bits[0], id = bits[1];
					var element = document.getElementById(id);
					if (element && tagName && element.nodeName.toLowerCase() != tagName) {
						return [];
					}
					currentContext = element ? [element] : [];
					continue;
				}
				if (token.indexOf('.') > -1) {
					var bits = token.split('.'), tagName = bits[0] || '*', className = bits[1], found = [], foundCount = 0;
					for (var h = 0; h < currentContext.length; h++) {
						var elements;
						if (tagName == '*') {
							elements = currentContext[h].getElementsByTagName('*');
						} else {
							elements = currentContext[h].getElementsByTagName(tagName);
						}
						for (var j = 0; j < elements.length; j++) {
							found[foundCount++] = elements[j];
						}
					}
					currentContext = [];
					var currentContextIndex = 0;
					for (var k = 0; k < found.length; k++) {
						if (found[k].className && found[k].className.match(new RegExp('(\\s|^)'+className+'(\\s|$)'))) {
							currentContext[currentContextIndex++] = found[k];
						}
					}
					continue;
				}
				if (token.match(/^(\w*)\[(\w+)([=~\|\^\$\*]?)=?"?([^\]"]*)"?\]$/)) {
					var tagName = RegExp.$1 || '*', attrName = RegExp.$2, attrOperator = RegExp.$3, attrValue = RegExp.$4;
					if(attrName.toLowerCase() == 'for' && this.browser.msie && this.browser.version < 8) {
						attrName = 'htmlFor';
					}
					var found = [], foundCount = 0;
					for (var h = 0; h < currentContext.length; h++) {
						var elements;
						if (tagName == '*') {
							elements = currentContext[h].getElementsByTagName('*');
						} else {
							elements = currentContext[h].getElementsByTagName(tagName);
						}
						for (var j = 0; elements[j]; j++) {
							found[foundCount++] = elements[j];
						}
					}
					currentContext = [];
					var currentContextIndex = 0, checkFunction;
					switch (attrOperator) {
						case '=': checkFunction = function(e) { return (e.getAttribute(attrName) == attrValue) }; break;
						case '~': checkFunction = function(e) { return (e.getAttribute(attrName).match(new RegExp('(\\s|^)'+attrValue+'(\\s|$)'))) }; break;
						case '|': checkFunction = function(e) { return (e.getAttribute(attrName).match(new RegExp('^'+attrValue+'-?'))) }; break;
						case '^': checkFunction = function(e) { return (e.getAttribute(attrName).indexOf(attrValue) == 0) }; break;
						case '$': checkFunction = function(e) { return (e.getAttribute(attrName).lastIndexOf(attrValue) == e.getAttribute(attrName).length - attrValue.length) }; break;
						case '*': checkFunction = function(e) { return (e.getAttribute(attrName).indexOf(attrValue) > -1) }; break;
						default : checkFunction = function(e) { return e.getAttribute(attrName) };
					}
					currentContext = [];
					var currentContextIndex = 0;
					for (var k = 0; k < found.length; k++) {
						if (checkFunction(found[k])) {
							currentContext[currentContextIndex++] = found[k];
						}
					}
					continue;
				}
				tagName = token;
				var found = [], foundCount = 0;
				for (var h = 0; h < currentContext.length; h++) {
					var elements = currentContext[h].getElementsByTagName(tagName);
					for (var j = 0; j < elements.length; j++) {
						found[foundCount++] = elements[j];
					}
				}
				currentContext = found;
			}
			resultList = [].concat(resultList,currentContext);
		}
		return resultList;
	},
	trim: function (str) {
		return str.replace(/^\s+/, '').replace(/\s+$/, '');
	},
	bind: function(f, scope, forceArgs){
		return function() {return f.apply(scope, typeof forceArgs !== 'undefined' ? [forceArgs] : arguments);};
	}
};


// cycle scroll gallery init
function initCycleCarousel() {
	jQuery('div.cycle-gallery').scrollAbsoluteGallery({
		mask: 'div.mask',
		slider: 'div.slideset',
		slides: 'div.slide',
		btnPrev: 'a.btn-prev',
		btnNext: 'a.btn-next',
		generatePagination: '.pagination',
		stretchSlideToMask: true,
		maskAutoSize: true,
		autoRotation: false,
		switchTime: 3000,
		animSpeed: 500
	});
}

// popups init
function initPopups() {
	jQuery('#nav').contentPopup({
		mode: 'click',
		popup: 'ul'
	});
}

// clear inputs on focus
function initInputs() {
	PlaceholderInput.replaceByOptions({
		// filter options
		clearInputs: true,
		clearTextareas: true,
		clearPasswords: true,
		skipClass: 'default',
		
		// input options
		wrapWithElement: false,
		showUntilTyping: false,
		getParentByClass: false,
		placeholderAttr: 'value'
	});
}

//detect device type
var isTouchDevice = (function() {
    try {
        return ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
    } catch (e) {
        return false;
    }
}());

function initCarousel() {
	jQuery('div.carousel').each(function() {
		var holder = jQuery(this),
			mask = holder.find('.mask'),
			slider = mask.find('>div.slideset'),
			slides = slider.find('>div.slide'),
			btnPrev = holder.find('a.btn-prev'),
			btnNext = holder.find('a.btn-next'),
			isAnimating = false,
			active = slides.length,
			animSpeed = 400,
			duration = 3000,
			autorotation = false,
			pauseOnHover = true,
			step = 8,
			timer,
			sliderWidth = getSliderWidth(),
			direction = true;
		
		if(mask.length && slides.length && sliderWidth * 3 >= mask.width()) {
			
			slides.clone().prependTo(slider).clone().appendTo(slider);
			slides = slider.find('>div.slide');
			
			refreshPosition();
			jQuery(window).on('resize orientationchange', refreshPosition);
			
			if(pauseOnHover) {
				slides.on({
					mouseenter: function() {
						autorotate(false);
					},
					mouseleave: function() {
						autorotate(autorotation);
					}
				});
			}
			btnPrev.on('click touchstart', function(e) {
				prevSlide(active - step);
				e.preventDefault();
			});
			btnNext.on('click touchstart', function(e) {
				nextSlide(active + step);
				e.preventDefault();
			});
			
			if(isTouchDevice ||  navigator.msPointerEnabled) {
				slides.each(function(i, el) {
					var wrap = jQuery(el);
					var href = wrap.find('a').attr('href');
					var img = wrap.find('img');
					var enableAnchor = true;
					img.unwrap('a');
					wrap.bind('touchmove', function(e){
						e.preventDefault();
						enableAnchor = false;
					});
					wrap.bind('touchend', function(e){
						e.preventDefault();
						if(enableAnchor) {
							window.location.href = href;
						}
						enableAnchor = true;
					});
				});
				
				mask.swipe({
					threshold: 20,
					allowPageScroll: 'vertical',
					swipe:function(event, direction) {
						if(direction === 'left') {
							nextSlide(active + step);
						} else if(direction === 'right') {
							prevSlide(active - step);
						}
					}
				});
			}
			autorotate(autorotation);
		}
		
		function nextSlide(next) {
			if(!isAnimating) {
				autorotate(false);
				isAnimating = true;
				direction = true;
				slider.stop().animate({'marginLeft': getSliderOffset(active , next)}, animSpeed, function() {
					if(next >= slides.length * 2 / 3){
						var currSliderOffset = parseInt(slider.css('marginLeft'), 10);
						slider.css({'marginLeft': direction ? currSliderOffset + sliderWidth : currSliderOffset - sliderWidth});
						active = next - slides.length / 3;
					} else {
						active = next;
					}
					isAnimating = false;
					autorotate(autorotation);
				});
			}
		}
		
		function prevSlide(prev) {
			if(!isAnimating) {
				autorotate(false);
				isAnimating = true;
				direction = false;
				slider.stop().animate({'marginLeft': getSliderOffset(active , prev)}, animSpeed, function() {
					if(prev < slides.length  / 3){
						var currSliderOffset = parseInt(slider.css('marginLeft'), 10);
						slider.css({'marginLeft': direction ? currSliderOffset + sliderWidth : currSliderOffset - sliderWidth});
						active = prev + slides.length / 3;
					} else {
						active = prev;
					}
					isAnimating = false;
					autorotate(autorotation);
				});
			}
		}
		
		function autorotate(flag) {
			clearTimeout(timer);
			if(flag) {
				timer = setTimeout(function(){nextSlide(active + step);}, duration);
			}
		}
		
		function getSliderWidth() {
			var width = 0;
			for(var i = 0; i < slides.length / 3; i++) {
				width += slides.eq(i).outerWidth(true);
			}
			return width;
		}
		
		function getSliderOffset(firstSlide, secondSlide) {
			var diffOffset = 0,
				currSliderOffset = parseInt(slider.css('marginLeft'), 10);
			
			for(var i = Math.min(firstSlide, secondSlide); i < Math.max(firstSlide, secondSlide); i++) {
				diffOffset += slides.eq(i).outerWidth(true);
			}
			return direction ? currSliderOffset - diffOffset : currSliderOffset + diffOffset;
		}
		
		function refreshPosition() {
			if(!isAnimating) {
				var newOffset = 0;
				
				for(var i = 0; i < active; i++) {
					newOffset += slides.eq(i).outerWidth(true);
				}
				sliderWidth = getSliderWidth();
				slider.css({
					'marginLeft': -newOffset,
					'width': sliderWidth * 3
				});
			}
		}
	});
}

/*
 * jQuery Cycle Carousel plugin
 */
;(function($){
	function ScrollAbsoluteGallery(options) {
		this.options = $.extend({
			activeClass: 'active',
			mask: 'div.slides-mask',
			slider: '>ul',
			slides: '>li',
			btnPrev: '.btn-prev',
			btnNext: '.btn-next',
			pagerLinks: 'ul.pager > li',
			generatePagination: false,
			pagerList: '<ul>',
			pagerListItem: '<li><a href="#"></a></li>',
			pagerListItemText: 'a',
			galleryReadyClass: 'gallery-js-ready',
			currentNumber: 'span.current-num',
			totalNumber: 'span.total-num',
			maskAutoSize: false,
			autoRotation: false,
			pauseOnHover: false,
			stretchSlideToMask: false,
			switchTime: 3000,
			animSpeed: 500,
			handleTouch: true,
			swipeThreshold: 50
		}, options);
		this.init();
	}
	ScrollAbsoluteGallery.prototype = {
		init: function() {
			if(this.options.holder) {
				this.findElements();
				this.attachEvents();
			}
		},
		findElements: function() {
			// find structure elements
			this.holder = $(this.options.holder).addClass(this.options.galleryReadyClass);
			this.mask = this.holder.find(this.options.mask);
			this.slider = this.mask.find(this.options.slider);
			this.slides = this.slider.find(this.options.slides);
			this.btnPrev = this.holder.find(this.options.btnPrev);
			this.btnNext = this.holder.find(this.options.btnNext);

			// slide count display
			this.currentNumber = this.holder.find(this.options.currentNumber);
			this.totalNumber = this.holder.find(this.options.totalNumber);

			// create gallery pagination
			if(typeof this.options.generatePagination === 'string') {
				this.pagerLinks = this.buildPagination();
			} else {
				this.pagerLinks = this.holder.find(this.options.pagerLinks);
			}

			// define index variables
			this.slideWidth = this.slides.width();
			this.currentIndex = 0;
			this.prevIndex = 0;

			// reposition elements
			this.slider.css({
				position: 'relative',
				height: this.slider.height()
			});
			this.slides.css({
				position: 'absolute',
				left: -9999,
				top: 0
			}).eq(this.currentIndex).css({
				left: 0
			});
			this.refreshState();
		},
		buildPagination: function() {
			var pagerLinks = $();
			if(!this.pagerHolder) {
				this.pagerHolder = this.holder.find(this.options.generatePagination);
			}
			if(this.pagerHolder.length) {
				this.pagerHolder.empty();
				this.pagerList = $(this.options.pagerList).appendTo(this.pagerHolder);
				for(var i = 0; i < this.slides.length; i++) {
					$(this.options.pagerListItem).appendTo(this.pagerList).find(this.options.pagerListItemText).text(i+1);
				}
				pagerLinks = this.pagerList.children();
			}
			return pagerLinks;
		},
		attachEvents: function() {
			// attach handlers
			var self = this;
			this.btnPrev.click(function(e){
				if(self.slides.length > 1) {
					self.prevSlide();
				}
				e.preventDefault();
			});
			this.btnNext.click(function(e){
				if(self.slides.length > 1) {
					self.nextSlide();
				}
				e.preventDefault();
			});
			this.pagerLinks.each(function(index){
				$(this).click(function(e){
					if(self.slides.length > 1) {
						self.numSlide(index);
					}
					e.preventDefault();
				});
			});

			// handle autorotation pause on hover
			if(this.options.pauseOnHover) {
				this.holder.hover(function(){
					clearTimeout(self.timer);
				}, function(){
					self.autoRotate();
				});
			}

			// handle holder and slides dimensions
			$(window).bind('load resize orientationchange', function(){
				if(!self.animating) {
					if(self.options.stretchSlideToMask) {
						self.resizeSlides();
					}
					self.resizeHolder();
					self.setSlidesPosition(self.currentIndex);
				}
			});
			if(self.options.stretchSlideToMask) {
				self.resizeSlides();
			}

			// handle swipe on mobile devices
			if(this.options.handleTouch && $.fn.swipe && this.slides.length > 1) {
				this.mask.swipe({
					excludedElements: '',
					fallbackToMouseEvents: false,
					threshold: this.options.swipeThreshold,
					allowPageScroll: 'vertical',
					swipeStatus: function(e, phase, direction, offset) {
						// avoid swipe while gallery animating
						if(self.animating) {
							return false;
						}

						// move gallery
						if(direction === 'left' || direction === 'right') {
							self.swipeOffset = -self.slideWidth + (direction === 'left' ? -1 : 1) * offset;
							self.slider.css({marginLeft: self.swipeOffset});
						}
						clearTimeout(self.timer);
						switch(phase) {
							case 'cancel':
								self.slider.animate({marginLeft: -self.slideWidth}, {duration: self.options.animSpeed});
								break;
							case 'end':
								if(direction === 'left') {
									self.nextSlide();
								} else {
									self.prevSlide();
								}
								self.swipeOffset = 0;
								break;
						}
					}
				});
			}

			// start autorotation
			this.autoRotate();
			this.resizeHolder();
			this.setSlidesPosition(this.currentIndex);
		},
		resizeSlides: function() {
			this.slideWidth = this.mask.width();
			this.slides.css({
				width: this.slideWidth
			});
		},
		resizeHolder: function() {
			if(this.options.maskAutoSize) {
				this.slider.css({
					height: this.slides.eq(this.currentIndex).outerHeight(true)
				});
			}
		},
		prevSlide: function() {
			if(!this.animating) {
				this.direction = -1;
				this.prevIndex = this.currentIndex;
				if(this.currentIndex > 0) this.currentIndex--;
				else this.currentIndex = this.slides.length - 1;
				this.switchSlide();
			}
		},
		nextSlide: function(fromAutoRotation) {
			if(!this.animating) {
				this.direction = 1;
				this.prevIndex = this.currentIndex;
				if(this.currentIndex < this.slides.length - 1) this.currentIndex++;
				else this.currentIndex = 0;
				this.switchSlide();
			}
		},
		numSlide: function(c) {
			if(!this.animating && this.currentIndex !== c) {
				this.direction = c > this.currentIndex ? 1 : -1;
				this.prevIndex = this.currentIndex;
				this.currentIndex = c;
				this.switchSlide();
			}
		},
		preparePosition: function() {
			// prepare slides position before animation
			this.setSlidesPosition(this.prevIndex, this.direction < 0 ? this.currentIndex : null, this.direction > 0 ? this.currentIndex : null, this.direction);
		},
		setSlidesPosition: function(index, slideLeft, slideRight, direction) {
			// reposition holder and nearest slides
			if(this.slides.length > 1) {
				var prevIndex = (typeof slideLeft === 'number' ? slideLeft : index > 0 ? index - 1 : this.slides.length - 1);
				var nextIndex = (typeof slideRight === 'number' ? slideRight : index < this.slides.length - 1 ? index + 1 : 0);

				this.slider.css({marginLeft: this.swipeOffset ? this.swipeOffset : -this.slideWidth});
				this.slides.css({left:-9999}).eq(index).css({left: this.slideWidth});

				if(prevIndex === nextIndex && typeof direction === 'number') {
					this.slides.eq(nextIndex).css({left: direction > 0 ? this.slideWidth*2 : 0 });
				} else {
					this.slides.eq(prevIndex).css({left: 0});
					this.slides.eq(nextIndex).css({left: this.slideWidth * 2});
				}
			}
		},
		switchSlide: function() {
			// prepare positions and calculate offset
			var self = this;
			var oldSlide = this.slides.eq(this.prevIndex);
			var newSlide = this.slides.eq(this.currentIndex);

			// start animation
			var animProps = {marginLeft: this.direction > 0 ? -this.slideWidth*2 : 0 };
			if(this.options.maskAutoSize) {
				// resize holder if needed
				animProps.height = newSlide.outerHeight(true);
			}
			this.animating = true;
			this.preparePosition();
			this.slider.animate(animProps,{duration:this.options.animSpeed, complete:function() {
				self.setSlidesPosition(self.currentIndex);

				// start autorotation
				self.animating = false;
				self.autoRotate();
			}});

			// refresh classes
			this.refreshState();
		},
		refreshState: function(initial) {
			// slide change function
			this.slides.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);
			this.pagerLinks.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);

			// display current slide number
			this.currentNumber.html(this.currentIndex + 1);
			this.totalNumber.html(this.slides.length);
		},
		autoRotate: function() {
			var self = this;
			clearTimeout(this.timer);
			if(this.options.autoRotation && self.slides.length > 1) {
				this.timer = setTimeout(function() {
					self.nextSlide();
				}, this.options.switchTime);
			}
		}
	};
	
	// jquery plugin
	$.fn.scrollAbsoluteGallery = function(opt){
		return this.each(function(){
			$(this).data('ScrollAbsoluteGallery', new ScrollAbsoluteGallery($.extend(opt,{holder:this})));
		});
	};
}(jQuery));

/*
 * Popups plugin
 */
;(function($) {
	function ContentPopup(opt) {
		this.options = $.extend({
			holder: null,
			popup: '.popup',
			btnOpen: '.open',
			btnClose: '.close',
			openClass: 'popup-active',
			clickEvent: 'click',
			mode: 'click',
			hideOnClickLink: true,
			hideOnClickOutside: true,
			delay: 50
		}, opt);
		if(this.options.holder) {
			this.holder = $(this.options.holder);
			this.init();
		}
	}
	ContentPopup.prototype = {
		init: function() {
			this.findElements();
			this.attachEvents();
		},
		findElements: function() {
			this.popup = this.holder.find(this.options.popup);
			this.btnOpen = this.holder.find(this.options.btnOpen);
			this.btnClose = this.holder.find(this.options.btnClose);
		},
		attachEvents: function() {
			// handle popup openers
			var self = this;
			this.clickMode = isTouchDevice || (self.options.mode === self.options.clickEvent);

			if(this.clickMode) {
				// handle click mode
				this.btnOpen.bind(self.options.clickEvent, function(e) {
					if(self.holder.hasClass(self.options.openClass)) {
						if(self.options.hideOnClickLink) {
							self.hidePopup();
						}
					} else {
						self.showPopup();
					}
					e.preventDefault();
				});

				// prepare outside click handler
				this.outsideClickHandler = this.bind(this.outsideClickHandler, this);
			} else {
				// handle hover mode
				var timer, delayedFunc = function(func) {
					clearTimeout(timer);
					timer = setTimeout(function() {
						func.call(self);
					}, self.options.delay);
				};
				this.btnOpen.bind('mouseover', function() {
					delayedFunc(self.showPopup);
				}).bind('mouseout', function() {
					delayedFunc(self.hidePopup);
				});
				this.popup.bind('mouseover', function() {
					delayedFunc(self.showPopup);
				}).bind('mouseout', function() {
					delayedFunc(self.hidePopup);
				});
			}

			// handle close buttons
			this.btnClose.bind(self.options.clickEvent, function(e) {
				self.hidePopup();
				e.preventDefault();
			});
		},
		outsideClickHandler: function(e) {
			// hide popup if clicked outside
			var currentNode = (e.changedTouches ? e.changedTouches[0] : e).target;
			if(!$(currentNode).parents().filter(this.holder).length) {
				this.hidePopup();
			}
		},
		showPopup: function() {
			// reveal popup
			this.holder.addClass(this.options.openClass);
			this.popup.css({display:'block'});

			// outside click handler
			if(this.clickMode && this.options.hideOnClickOutside && !this.outsideHandlerActive) {
				this.outsideHandlerActive = true;
				$(document).bind('click touchstart', this.outsideClickHandler);
			}
		},
		hidePopup: function() {
			// hide popup
			this.holder.removeClass(this.options.openClass);
			this.popup.css({display:'none'});

			// outside click handler
			if(this.clickMode && this.options.hideOnClickOutside && this.outsideHandlerActive) {
				this.outsideHandlerActive = false;
				$(document).unbind('click touchstart', this.outsideClickHandler);
			}
		},
		bind: function(f, scope, forceArgs){
			return function() {return f.apply(scope, forceArgs ? [forceArgs] : arguments);};
		}
	};

	// detect touch devices
	var isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

	// jQuery plugin interface
	$.fn.contentPopup = function(opt) {
		return this.each(function() {
			new ContentPopup($.extend(opt, {holder: this}));
		});
	};
}(jQuery));

// placeholder class
;(function(){
	var placeholderCollection = [];
	PlaceholderInput = function() {
		this.options = {
			element:null,
			showUntilTyping:false,
			wrapWithElement:false,
			getParentByClass:false,
			showPasswordBullets:false,
			placeholderAttr:'value',
			inputFocusClass:'focus',
			inputActiveClass:'text-active',
			parentFocusClass:'parent-focus',
			parentActiveClass:'parent-active',
			labelFocusClass:'label-focus',
			labelActiveClass:'label-active',
			fakeElementClass:'input-placeholder-text'
		};
		placeholderCollection.push(this);
		this.init.apply(this,arguments);
	};
	PlaceholderInput.refreshAllInputs = function(except) {
		for(var i = 0; i < placeholderCollection.length; i++) {
			if(except !== placeholderCollection[i]) {
				placeholderCollection[i].refreshState();
			}
		}
	};
	PlaceholderInput.replaceByOptions = function(opt) {
		var inputs = [].concat(
			convertToArray(document.getElementsByTagName('input')),
			convertToArray(document.getElementsByTagName('textarea'))
		);
		for(var i = 0; i < inputs.length; i++) {
			if(inputs[i].className.indexOf(opt.skipClass) < 0) {
				var inputType = getInputType(inputs[i]);
				var placeholderValue = inputs[i].getAttribute('placeholder');
				if(opt.focusOnly || (opt.clearInputs && (inputType === 'text' || inputType === 'email' || placeholderValue)) ||
					(opt.clearTextareas && inputType === 'textarea') ||
					(opt.clearPasswords && inputType === 'password')
				) {
					new PlaceholderInput({
						element:inputs[i],
						focusOnly: opt.focusOnly,
						wrapWithElement:opt.wrapWithElement,
						showUntilTyping:opt.showUntilTyping,
						getParentByClass:opt.getParentByClass,
						showPasswordBullets:opt.showPasswordBullets,
						placeholderAttr: placeholderValue ? 'placeholder' : opt.placeholderAttr
					});
				}
			}
		}
	};
	PlaceholderInput.prototype = {
		init: function(opt) {
			this.setOptions(opt);
			if(this.element && this.element.PlaceholderInst) {
				this.element.PlaceholderInst.refreshClasses();
			} else {
				this.element.PlaceholderInst = this;
				if(this.elementType !== 'radio' || this.elementType !== 'checkbox' || this.elementType !== 'file') {
					this.initElements();
					this.attachEvents();
					this.refreshClasses();
				}
			}
		},
		setOptions: function(opt) {
			for(var p in opt) {
				if(opt.hasOwnProperty(p)) {
					this.options[p] = opt[p];
				}
			}
			if(this.options.element) {
				this.element = this.options.element;
				this.elementType = getInputType(this.element);
				if(this.options.focusOnly) {
					this.wrapWithElement = false;
				} else {
					if(this.elementType === 'password' && this.options.showPasswordBullets) {
						this.wrapWithElement = false;
					} else {
						this.wrapWithElement = this.elementType === 'password' || this.options.showUntilTyping ? true : this.options.wrapWithElement;
					}
				}
				this.setPlaceholderValue(this.options.placeholderAttr);
			}
		},
		setPlaceholderValue: function(attr) {
			this.origValue = (attr === 'value' ? this.element.defaultValue : (this.element.getAttribute(attr) || ''));
			if(this.options.placeholderAttr !== 'value') {
				this.element.removeAttribute(this.options.placeholderAttr);
			}
		},
		initElements: function() {
			// create fake element if needed
			if(this.wrapWithElement) {
				this.fakeElement = document.createElement('span');
				this.fakeElement.className = this.options.fakeElementClass;
				this.fakeElement.innerHTML += this.origValue;
				this.fakeElement.style.color = getStyle(this.element, 'color');
				this.fakeElement.style.position = 'absolute';
				this.element.parentNode.insertBefore(this.fakeElement, this.element);
				
				if(this.element.value === this.origValue || !this.element.value) {
					this.element.value = '';
					this.togglePlaceholderText(true);
				} else {
					this.togglePlaceholderText(false);
				}
			} else if(!this.element.value && this.origValue.length) {
				this.element.value = this.origValue;
			}
			// get input label
			if(this.element.id) {
				this.labels = document.getElementsByTagName('label');
				for(var i = 0; i < this.labels.length; i++) {
					if(this.labels[i].htmlFor === this.element.id) {
						this.labelFor = this.labels[i];
						break;
					}
				}
			}
			// get parent node (or parentNode by className)
			this.elementParent = this.element.parentNode;
			if(typeof this.options.getParentByClass === 'string') {
				var el = this.element;
				while(el.parentNode) {
					if(hasClass(el.parentNode, this.options.getParentByClass)) {
						this.elementParent = el.parentNode;
						break;
					} else {
						el = el.parentNode;
					}
				}
			}
		},
		attachEvents: function() {
			this.element.onfocus = bindScope(this.focusHandler, this);
			this.element.onblur = bindScope(this.blurHandler, this);
			if(this.options.showUntilTyping) {
				this.element.onkeydown = bindScope(this.typingHandler, this);
				this.element.onpaste = bindScope(this.typingHandler, this);
			}
			if(this.wrapWithElement) this.fakeElement.onclick = bindScope(this.focusSetter, this);
		},
		togglePlaceholderText: function(state) {
			if(!this.element.readOnly && !this.options.focusOnly) {
				if(this.wrapWithElement) {
					this.fakeElement.style.display = state ? '' : 'none';
				} else {
					this.element.value = state ? this.origValue : '';
				}
			}
		},
		focusSetter: function() {
			this.element.focus();
		},
		focusHandler: function() {
			clearInterval(this.checkerInterval);
			this.checkerInterval = setInterval(bindScope(this.intervalHandler,this), 1);
			this.focused = true;
			if(!this.element.value.length || this.element.value === this.origValue) {
				if(!this.options.showUntilTyping) {
					this.togglePlaceholderText(false);
				}
			}
			this.refreshClasses();
		},
		blurHandler: function() {
			clearInterval(this.checkerInterval);
			this.focused = false;
			if(!this.element.value.length || this.element.value === this.origValue) {
				this.togglePlaceholderText(true);
			}
			this.refreshClasses();
			PlaceholderInput.refreshAllInputs(this);
		},
		typingHandler: function() {
			setTimeout(bindScope(function(){
				if(this.element.value.length) {
					this.togglePlaceholderText(false);
					this.refreshClasses();
				}
			},this), 10);
		},
		intervalHandler: function() {
			if(typeof this.tmpValue === 'undefined') {
				this.tmpValue = this.element.value;
			}
			if(this.tmpValue != this.element.value) {
				PlaceholderInput.refreshAllInputs(this);
			}
		},
		refreshState: function() {
			if(this.wrapWithElement) {
				if(this.element.value.length && this.element.value !== this.origValue) {
					this.togglePlaceholderText(false);
				} else if(!this.element.value.length) {
					this.togglePlaceholderText(true);
				}
			}
			this.refreshClasses();
		},
		refreshClasses: function() {
			this.textActive = this.focused || (this.element.value.length && this.element.value !== this.origValue);
			this.setStateClass(this.element, this.options.inputFocusClass,this.focused);
			this.setStateClass(this.elementParent, this.options.parentFocusClass,this.focused);
			this.setStateClass(this.labelFor, this.options.labelFocusClass,this.focused);
			this.setStateClass(this.element, this.options.inputActiveClass, this.textActive);
			this.setStateClass(this.elementParent, this.options.parentActiveClass, this.textActive);
			this.setStateClass(this.labelFor, this.options.labelActiveClass, this.textActive);
		},
		setStateClass: function(el,cls,state) {
			if(!el) return; else if(state) addClass(el,cls); else removeClass(el,cls);
		}
	};
	
	// utility functions
	function convertToArray(collection) {
		var arr = [];
		for (var i = 0, ref = arr.length = collection.length; i < ref; i++) {
			arr[i] = collection[i];
		}
		return arr;
	}
	function getInputType(input) {
		return (input.type ? input.type : input.tagName).toLowerCase();
	}
	function hasClass(el,cls) {
		return el.className ? el.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)')) : false;
	}
	function addClass(el,cls) {
		if (!hasClass(el,cls)) el.className += " "+cls;
	}
	function removeClass(el,cls) {
		if (hasClass(el,cls)) {el.className=el.className.replace(new RegExp('(\\s|^)'+cls+'(\\s|$)'),' ');}
	}
	function bindScope(f, scope) {
		return function() {return f.apply(scope, arguments);};
	}
	function getStyle(el, prop) {
		if (document.defaultView && document.defaultView.getComputedStyle) {
			return document.defaultView.getComputedStyle(el, null)[prop];
		} else if (el.currentStyle) {
			return el.currentStyle[prop];
		} else {
			return el.style[prop];
		}
	}
}());

// font resize script
FontResize = {
	options: {
		maxStep: 1.5,
		defaultFS: 1.4,
		resizeStep: 0.1,
		resizeHolder: 'body',
		cookieName: 'fontResizeCookie'
	},
	init: function() {
		this.runningLocal = (location.protocol.indexOf('file:') === 0);
		this.setDefaultScaling();
		this.addDefaultHandlers();
	},
	addDefaultHandlers: function() {
		this.addHandler('increase','inc');
		this.addHandler('decrease','dec');
		this.addHandler('reset');
	},
	setDefaultScaling: function() {
		if(this.options.resizeHolder == 'html') { this.resizeHolder = document.documentElement; }
		else { this.resizeHolder = document.body; }
		var cSize = this.getCookie(this.options.cookieName);
		if(!this.runningLocal && cSize) {
			this.fSize = parseFloat(cSize,10);
		} else {
			this.fSize = this.options.defaultFS;
		}
		this.changeSize();
	},
	changeSize: function(direction) {
		if(typeof direction !== 'undefined') {
			if(direction == 1) {
				this.fSize += this.options.resizeStep;
				if (this.fSize > this.options.defaultFS * this.options.maxStep) this.fSize = this.options.defaultFS * this.options.maxStep;
			} else if(direction == -1) {
				this.fSize -= this.options.resizeStep;
				if (this.fSize < this.options.defaultFS / this.options.maxStep) this.fSize = this.options.defaultFS / this.options.maxStep;
			} else {
				this.fSize = this.options.defaultFS;
			}
		}
		this.resizeHolder.style.fontSize = this.fSize + 'em';
		this.updateCookie(this.fSize.toFixed(2));
		
		// refresh Cufon if present
		if(typeof Cufon !== 'undefined' && typeof Cufon.refresh === 'function') {
			Cufon.refresh();
		}
		return false;
	},
	addHandler: function(obj, type) {
		if(typeof obj === 'string') { obj = document.getElementById(obj); }
		if(obj && obj.tagName) {
			switch (type) {
				case 'inc':
					obj.onclick = this.bind(this.changeSize,this, [1]);
					break;
				case 'dec':
					obj.onclick = this.bind(this.changeSize,this, [-1]);
					break;
				default:
					obj.onclick = this.bind(this.changeSize,this, [0]);
			}
		}
	},
	updateCookie: function(scaleLevel) {
		if(!this.runningLocal) {
			this.setCookie(this.options.cookieName,scaleLevel);
		}
	},
	getCookie: function(name) {
		var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
		return matches ? decodeURIComponent(matches[1]) : undefined;
	},
	setCookie: function(name, value) {
		var exp = new Date();
		exp.setTime(exp.getTime()+(30*24*60*60*1000));
		document.cookie = name + '=' + value + ';' +'expires=' + exp.toGMTString() + ';' +'path=/';
	},
	bind: function(fn, scope, args){
		return function() {
			return fn.apply(scope, args || arguments);
		};
	}
};

// touch swipe
;(function(a){function u(b){return!b||void 0!==b.allowPageScroll||void 0===b.swipe&&void 0===b.swipeStatus||(b.allowPageScroll=h),b||(b={}),b=a.extend({},a.fn.swipe.defaults,b),this.each(function(){var c=a(this),d=c.data(s);d||(d=new v(this,b),c.data(s,d))})}function v(t,u){function O(b){if(!(fb()||a(b.target).closest(u.excludedElements,H).length>0)){b=b.originalEvent;var c,d=q?b:r?b.touches[0]:b;return I=m,r?J=q?1:b.touches.length:b.preventDefault(),A=0,B=null,G=null,C=0,D=0,E=0,F=1,K=hb(),!r||J===u.fingers||u.fingers===l||db()?(K[0].start.x=K[0].end.x=d.pageX,K[0].start.y=K[0].end.y=d.pageY,L=bb(),2==J&&(K[1].start.x=K[1].end.x=b.touches[1].pageX,K[1].start.y=K[1].end.y=b.touches[1].pageY,D=E=X(K[0].start,K[1].start)),(u.swipeStatus||u.pinchStatus)&&(c=S(b,I))):(R(b),c=!1),c===!1?(I=p,S(b,I),c):(gb(!0),H.bind(x,P),H.bind(y,Q),void 0)}}function P(a){if(a=a.originalEvent,I!==o&&I!==p){var b,c=q?a:r?a.touches[0]:a;if(K[0].end.x=q?c.pageX:r?a.touches[0].pageX:c.pageX,K[0].end.y=q?c.pageY:r?a.touches[0].pageY:c.pageY,M=bb(),B=ab(K[0].start,K[0].end),r&&(J=q?1:a.touches.length),I=n,2==J&&(0==D?(K[1].start.x=a.touches[1].pageX,K[1].start.y=a.touches[1].pageY,D=E=X(K[0].start,K[1].start)):(K[1].end.x=a.touches[1].pageX,K[1].end.y=a.touches[1].pageY,E=X(K[0].end,K[1].end),G=Z(K[0].end,K[1].end)),F=Y(D,E)),J!==u.fingers&&u.fingers!==l&&r)I=p,S(a,I);else if(V(a,B),A=$(K[0].start,K[0].end),C=W(K[0].start,K[0].end),(u.swipeStatus||u.pinchStatus)&&(b=S(a,I)),!u.triggerOnTouchEnd){var d=!U();T()===!0?(I=o,b=S(a,I)):d&&(I=p,S(a,I))}b===!1&&(I=p,S(a,I))}}function Q(a){if(a=a.originalEvent,a.touches&&a.touches.length>0)return!0;if(a.preventDefault(),M=bb(),0!=D&&(E=X(K[0].end,K[1].end),F=Y(D,E),G=Z(K[0].end,K[1].end)),A=$(K[0].start,K[0].end),B=ab(K[0].start,K[0].end),C=W(),u.triggerOnTouchEnd||u.triggerOnTouchEnd===!1&&I===n){I=o;var b=eb()||!db(),c=J===u.fingers||u.fingers===l||!r,d=0!==K[0].end.x,e=c&&d&&b;if(e){var f=U(),g=T();g!==!0&&null!==g||!f?f&&g!==!1||(I=p,S(a,I)):S(a,I)}else I=p,S(a,I)}else I===n&&(I=p,S(a,I));H.unbind(x,P,!1),H.unbind(y,Q,!1),gb(!1)}function R(){J=0,M=0,L=0,D=0,E=0,F=1,gb(!1)}function S(a,h){var i=void 0;if(u.swipeStatus&&(i=u.swipeStatus.call(H,a,h,B||null,A||0,C||0,J)),u.pinchStatus&&eb()&&(i=u.pinchStatus.call(H,a,h,G||null,E||0,C||0,J,F)),h===p&&(!u.click||1!==J&&r||!isNaN(A)&&0!==A||(i=u.click.call(H,a,a.target))),h==o){switch(u.swipe&&(i=u.swipe.call(H,a,B,A,C,J)),B){case b:u.swipeLeft&&(i=u.swipeLeft.call(H,a,B,A,C,J));break;case c:u.swipeRight&&(i=u.swipeRight.call(H,a,B,A,C,J));break;case d:u.swipeUp&&(i=u.swipeUp.call(H,a,B,A,C,J));break;case e:u.swipeDown&&(i=u.swipeDown.call(H,a,B,A,C,J))}switch(G){case f:u.pinchIn&&(i=u.pinchIn.call(H,a,G||null,E||0,C||0,J,F));break;case g:u.pinchOut&&(i=u.pinchOut.call(H,a,G||null,E||0,C||0,J,F))}}return(h===p||h===o)&&R(a),i}function T(){return null!==u.threshold?A>=u.threshold:null}function U(){var a;return a=u.maxTimeThreshold?C>=u.maxTimeThreshold?!1:!0:!0}function V(a,f){if(u.allowPageScroll===h||db())a.preventDefault();else{var g=u.allowPageScroll===i;switch(f){case b:(u.swipeLeft&&g||!g&&u.allowPageScroll!=j)&&a.preventDefault();break;case c:(u.swipeRight&&g||!g&&u.allowPageScroll!=j)&&a.preventDefault();break;case d:(u.swipeUp&&g||!g&&u.allowPageScroll!=k)&&a.preventDefault();break;case e:(u.swipeDown&&g||!g&&u.allowPageScroll!=k)&&a.preventDefault()}}}function W(){return M-L}function X(a,b){var c=Math.abs(a.x-b.x),d=Math.abs(a.y-b.y);return Math.round(Math.sqrt(c*c+d*d))}function Y(a,b){var c=1*(b/a);return c.toFixed(2)}function Z(){return 1>F?g:f}function $(a,b){return Math.round(Math.sqrt(Math.pow(b.x-a.x,2)+Math.pow(b.y-a.y,2)))}function _(a,b){var c=a.x-b.x,d=b.y-a.y,e=Math.atan2(d,c),f=Math.round(180*e/Math.PI);return 0>f&&(f=360-Math.abs(f)),f}function ab(a,f){var g=_(a,f);return 45>=g&&g>=0?b:360>=g&&g>=315?b:g>=135&&225>=g?c:g>45&&135>g?e:d}function bb(){var a=new Date;return a.getTime()}function cb(){H.unbind(w,O),H.unbind(z,R),H.unbind(x,P),H.unbind(y,Q),gb(!1)}function db(){return u.pinchStatus||u.pinchIn||u.pinchOut}function eb(){return G&&db()}function fb(){return H.data(s+"_intouch")===!0?!0:!1}function gb(a){a=a===!0?!0:!1,H.data(s+"_intouch",a)}function hb(){for(var a=[],b=0;5>=b;b++)a.push({start:{x:0,y:0},end:{x:0,y:0},delta:{x:0,y:0}});return a}var v=r||!u.fallbackToMouseEvents,w=v?q&&"MSPointerDown"||"touchstart":"mousedown",x=v?q&&"MSPointerMove"||"touchmove":"mousemove",y=v?q&&"MSPointerUp"||"touchend":"mouseup",z="touchcancel",A=0,B=null,C=0,D=0,E=0,F=1,G=0,H=a(t),I="start",J=0,K=null,L=0,M=0;try{H.bind(w,O),H.bind(z,R)}catch(N){a.error("events not supported "+w+","+z+" on jQuery.swipe")}this.enable=function(){return H.bind(w,O),H.bind(z,R),H},this.disable=function(){return cb(),H},this.destroy=function(){return cb(),H.css({msTouchAction:""}).data(s,null),H}}var b="left",c="right",d="up",e="down",f="in",g="out",h="none",i="auto",j="horizontal",k="vertical",l="all",m="start",n="move",o="end",p="cancel",q=navigator.msPointerEnabled,r=q||"ontouchstart"in window,s="TouchSwipe",t={fingers:1,threshold:75,maxTimeThreshold:null,swipe:null,swipeLeft:null,swipeRight:null,swipeUp:null,swipeDown:null,swipeStatus:null,pinchIn:null,pinchOut:null,pinchStatus:null,click:null,triggerOnTouchEnd:!0,allowPageScroll:"auto",fallbackToMouseEvents:!0,excludedElements:"button, input, select, textarea, a, .noSwipe"};a.fn.swipe=function(b){var c=a(this).css({msTouchAction:"none"}),d=c.data(s);if(d&&"string"==typeof b){if(d[b])return d[b].apply(this,Array.prototype.slice.call(arguments,1));a.error("Method "+b+" does not exist on jQuery.swipe")}else if(!(d||"object"!=typeof b&&b))return u.apply(this,arguments);return c},a.fn.swipe.defaults=t,a.fn.swipe.phases={PHASE_START:m,PHASE_MOVE:n,PHASE_END:o,PHASE_CANCEL:p},a.fn.swipe.directions={LEFT:b,RIGHT:c,UP:d,DOWN:e,IN:f,OUT:g},a.fn.swipe.pageScroll={NONE:h,HORIZONTAL:j,VERTICAL:k,AUTO:i},a.fn.swipe.fingers={ONE:1,TWO:2,THREE:3,ALL:l}})(jQuery);