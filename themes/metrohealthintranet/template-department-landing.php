<?php
/*
Template Name: Department Landing Page
*/
get_header();

dynamic_sidebar('breadcrumbs') ?>

<div class="main-heading">
	<h2><?php echo get_the_title(get_the_ID()) ?></h2>
</div>
<?php
	// Prepare key field of people ahead of time, outside of the loop below
	$array_of_ad_logins = array();
	if ( get_field( '_key_people' ) ) while ( has_sub_field( '_key_people' ) ) {
		$array_of_ad_logins []= get_sub_field( '_key_person' );
	} 
	asort($array_of_ad_logins);
	$department = get_field( '_department' );
	if ( ( ! empty($department) ) && ( $department != 'home' ) )
		$these_people = Ciho_Metrohealth_IDM_Department::get_deparment_users( $department );
	else
		$these_people = array();
	$column_id = 0;
	if (get_field('_columns'))
	{
		while ( has_sub_field( '_columns' ) )
		{	
			$column_id++;
			if ($column_id == 1)
			{
				?>
				<div class="column-main">
					<?php $attachments = new Attachments( 'attachments' ); /* pass the instance name */ ?>
					<?php if ( $attachments->exist() ) { ?>
						<div class="cycle-gallery">
							<div class="mask">
								<div class="slideset">
									<?php while( $attachments->get() ) : ?>
										<div class="slide"><?php echo preg_replace('#(width|height)=\"\d*\"\s#', "", wp_get_attachment_image( $attachments->id(), '413x367' )); ?></div>
									<?php endwhile; ?>
								</div>
							</div>
							<div class="pagination"></div>
						</div>
					<?php } else { ?>
						<div class="cycle-gallery">
							<div class="mask">
								<div class="slideset">
									<div class="slide"><img src="<?php echo get_template_directory_uri(); ?>/images/department-default-hospital.jpg" /></div>
								</div>
							</div>
							<div class="pagination"></div>
						</div>
					<?php } ?>
				<?php
			}
			elseif ($column_id == 2)
			{
				?>
				<div id="twocolumns">
	
					<div class="block-contact">
						<h3>About Us</h3>
						<div class="columns-holder">
							<div class="column">
								<h4>Department of <?php the_title(); ?></h4>
								<?php
								$address1 = get_field('_address1');
								$address2 = get_field('_address_line_2');
								$city     = get_field('_city');
								$state    = get_field('_state');
								$zip      = get_field('_zip');
								$building = get_field('_building');
								if (   empty( $address1 )
									|| empty( $city )
									|| empty( $state )
									|| empty( $zip )
									|| empty( $address1 )
									) {
									?>
									<address><?php echo $address1; ?> <?php echo ( ! empty( $address2 ) ) ? ', ' . $address2 : ''; ?> <br /><?php echo $city; ?>, <?php echo $state; ?> <?php echo $zip; ?>
										<?php echo ( ! empty( $building ) ) ? '<br />' . $building : ''; ?></address>
									<?php
									}
								?>
								
								<dl>
									<?php 
									$phone = get_field( '_phone' ); 
									if ( ! empty( $phone ) ) {
										?>
										<dd><?php $phone; ?></dd>
										<?php
									}
									?>
									
									
									<dd><?php echo get_field('_phone'); ?></dd>
									
										
										<?php 
									$fax = get_field( '_fax' ); 
									if ( ! empty( $fax ) ) {
										?>
										<dt>Fax:</dt>
										<dd><?php echo $fax; ?></dd>
										<?php
									}
									?>
									
									<?php 
									$email = get_field( '_email' ); 
									if ( ! empty( $email ) ) {
										?>
										<dt>E-mail: </dt>
										<dd><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></dd>
										<?php
									}
									?>
								</dl>
							</div>
							<div class="column">
								<?php
								if ( ! empty( $array_of_ad_logins ) ) {	
									$idm_objects = Ciho_Metrohealth_IDM_Data_Mapper::get_users_by_ad_id( $array_of_ad_logins );
									if ( ! empty( $idm_objects ) ) $ad_objects = AD_Data_Mapper::get_array_by_user_logins( $array_of_ad_logins );
									?>
									<h4>Important Contacts</h4>
									<dl class="style01">
										<?php
										if ( ! empty( $idm_objects ) ) foreach ( $idm_objects as $AD_ID => $idm_object ) {
											?>
											<dt><a href="/people/<?php echo strtolower($idm_object->AD_ID); ?> "><?php printf( '%s %s', $idm_object->first_name, $idm_object->last_name ); ?></a></dt>
											<dd><?php echo ( ! empty( $ad_objects[ $AD_ID ] ) ) ? ' - ' . $ad_objects[ $AD_ID ]->adi_title : ' - Title from Active Directory' ; ?></dd>
											<?php
										}
										?>
									</dl>
									<?php 
								}
								?>
								<a href="staff" class="more">View All Contacts</a>
							</div>
						</div>
					</div>

				

					<div class="carousel-holder">
						<h2>Our Team</h2>
						<div class="carousel">
							<div class="mask">
								<div class="slideset">
									<?php
                                                                         $ffsurl=get_bloginfo('url');
									foreach ( $these_people as $person) {
                                                                              
										printf( '<div class="slide"><a href="%5$s/people/%1$s"><img src="%2$s" title="%3$s %4$s" alt="%3$s %4$s" /></a></div>', strtolower($person->AD_ID), get_template_directory_uri() . '/images/ManSilhouette.jpg', $person->first_name, $person->last_name,$ffsurl );
									}
									?>
								</div>
							</div>
							<a class="btn-prev" href="#">Previous</a>
							<a class="btn-next" href="#">Next</a>
						</div>
						
					</div>
					<div class="twocolumns-holder">
						<div class="column">	
				<?php
			}
			elseif ($column_id == 3)
			{
				?>
				<div class="column">
				<?php
			}
			
					
			while(has_sub_field('_flexible_content'))
			{
				if (get_row_layout() == '_latest_post') 
				{
					?>
					<h2 class="mobile-hidden"><?php echo get_sub_field('_title', get_the_ID()); ?></h2>
					<?php
					$category_id = get_sub_field('categories', get_the_ID());
					$how_many = get_sub_field('how_many_posts_to_show', get_the_ID());					
					
					if ( ( ! empty( $department) ) && ( $department != 'home' ) ) {
					
						$latest_args = array(
							'cat' => $category_id, 
							'showposts' => $how_many,
							'meta_query' => array(
								array(
									'key' => '_department_page',
									'value' => (integer) $department,
									'compare' => '='
								)
							)
						);
						$widget_query = new WP_Query( $latest_args );
						?>
						<div class="post-holder mobile-hidden">
							<?php
							while( $widget_query->have_posts() ) {
								$widget_query->next_post();
								?>
								<div class="post">
										<h3><a href="<?php echo get_permalink( $widget_query->post->ID ); ?>"><?php echo get_the_title( $widget_query->post->ID ); ?></a></h3>
										<em class="date">POSTED <?php echo get_the_time( 'd/m/Y', $widget_query->post->ID ); ?></em>
										<span class="tags"><?php the_tags(__('TAGS: ', 'base'), ', ', ''); ?></span> <?php the_excerpt(); ?>
										<p><?php echo ciho_get_excerpt( $widget_query->post->post_excerpt, $widget_query->post->post_content ); ?> <a href="<?php echo get_permalink( $widget_query->post->ID ); ?>">Read More</a></p>
								</div>
								<?php
							}
							?>
						</div>
						<?php
					}
				}
				elseif (get_row_layout() == '_button') 
				{
					?>
					<div class="heading-container">
						<h2><a href="#"><?php echo the_sub_field('_title'); ?></a></h2>
						<a href="<?php echo the_sub_field('_link'); ?>" class="sign-in"><?php echo the_sub_field('_label'); ?></a>
					</div>
					<?php
				}
				elseif ( get_row_layout() == '_metro_moments' )
				{
					?>
					<article class="block active">
							<div class="heading-holder">
									<h3>Metro Moments</h3>
							</div>
							<div class="slide">
									<div class="slide-container">
										<?php
										$mm_args = array(
											'cat' => 59, 
											'showposts' => 1
										);
										$mm_query = new WP_Query( $mm_args );
										
										while( $mm_query->have_posts() ) {
											$mm_query->next_post();
											?>
											<h4><?php echo get_the_title( $mm_query->post->ID ); ?></h4>
											<p><?php echo ciho_get_excerpt( $mm_query->post->post_excerpt, $mm_query->post->post_content ); ?>  
												 <a href="<?php echo get_permalink( $mm_query->post->ID ); ?>">Read More</a></p>
											<div class="col-holder">
													<div class="col">
															<p><a href="#">Submit</a></p>
															<p><a href="#">Metro Moment</a></p>
													</div>
													<div class="col">
															<p><a href="<?php echo get_category_link(59); ?>">View All </a></p>
															<p><a href="<?php echo get_category_link(59); ?>">Metro Moments</a></p>
													</div>
											</div>
											<?php
										}
										?>
									</div>
							</div>
					</article>
					<?php
				}
				elseif (get_row_layout() == '_two_columns') 
				{
					?>
					<article class="block active mobile-visible">
						<div class="heading-holder">
							<a href="#" class="opener">opener</a>
							<h3><?php echo get_sub_field('_title'); ?></h3>
						</div>
						<div class="slide">
							<div class="slide-frame">
								<ul>
									<li><?php echo get_sub_field('_first_column'); ?></li>
								</ul>
								<ul>
									<li><?php echo get_sub_field('_second_column'); ?></li>
								</ul>
							</div>
						</div>
					</article>
										
					<div class="column-area mobile-hidden">
                                <h2><?php echo get_sub_field('_title'); ?></h2>
                                <div class="col-holder">
                                        <div class="col">
                                                <?php echo get_sub_field('_first_column'); ?>
                                        </div>
                                        <div class="col">
                                                <?php echo get_sub_field('_second_column'); ?>
                                        </div>
                                </div>
                        </div>
					
					<?php
				}
				elseif (get_row_layout() == '_text_with_title') 
				{
					?>
					<article class="block active mobile-visible">
						<div class="heading-holder">
							<h3><?php echo get_sub_field('_title'); ?></h3>
						</div>
						<div class="slide">
							<div class="slide-frame">
								<?php echo get_sub_field('_content'); ?>
							</div>
						</div>
					</article>
										
					<div class="column-area mobile-hidden">
                                <h2><?php echo get_sub_field('_title'); ?></h2>
                                <div class="text-holder">
                                       <?php echo get_sub_field('_content'); ?>
                                </div>
                        </div>
					
					<?php
				}
				elseif (get_row_layout() == '_my_connections') 
				{
					?>
					<div class="comments-holder mobile-hidden">
						<h2>My HR Connections</h2>
						<ul class="comments-list">
							<li>
								<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/img6.jpg" alt="image description"></div>
								<div class="text-holder">
									<h3><a href="#">Kimberly Surname - 6 minutes ago</a></h3>
									<p> ut mattis vel, elementum iaculis. Brna vel, elementum iaculis. Brna <a href="#">View Post</a></p>
								</div>
							</li>
							<li>
								<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/img7.jpg" alt="image description"></div>
								<div class="text-holder">
									<h3><a href="#">Thomas Pryce - 2 hours ago</a></h3>
									<p>ut mattis vel, elementum iaculis. Brna el, elementum iaculis. Brnanibh,    <a href="#">View Post</a></p>
								</div>
							</li>
							<li>
								<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/img8.jpg" alt="image description"></div>
								<div class="text-holder">
									<h3><a href="#">Dr. Keith - 2 days ago</a></h3>
									<p>Added 2 Hours ago <br />Brna nibh, blandit ut mattis vel, elementum iaculis. Brnanibh, blandit ut mattis vel, eleme <a href="#">View Post</a></p>
								</div>
							</li>
						</ul>
						<a href="#" class="more">Customize My Conections</a>
					</div>
					<article class="block style01 active mobile-visible">
						<div class="heading-holder">
							<a href="#" class="opener">opener</a>
							<h3>My HR Connections</h3>
						</div>
						<div class="slide">
							<ul class="comments-list">
								<li>
									<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/img6.jpg" alt="image description"></div>
									<div class="text-holder">
										<h3><a href="#">Kimberly Surname - 6 minutes ago</a></h3>
										<p> ut mattis vel, elementum iaculis. Brna vel, elementum iaculis. Brna <a href="#">View Post</a></p>
									</div>
								</li>
								<li>
									<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/img7.jpg" alt="image description"></div>
									<div class="text-holder">
										<h3><a href="#">Thomas Pryce - 2 hours ago</a></h3>
										<p>ut mattis vel, elementum iaculis. Brna el, elementum iaculis. Brnanibh,    <a href="#">View Post</a></p>
									</div>
								</li>
								<li>
									<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/img8.jpg" alt="image description"></div>
									<div class="text-holder">
										<h3><a href="#">Dr. Keith - 2 days ago</a></h3>
										<p>Added 2 Hours ago <br />Brna nibh, blandit ut mattis vel, elementum iaculis. Brnanibh, blandit ut mattis vel, eleme <a href="#">View Post</a></p>
									</div>
								</li>
							</ul>
							<div class="more-holder">
								<a href="#" class="more alignright">Add Conection</a>
								<a href="#" class="more alignleft">View All My Connections</a>
							</div>
						</div>
					</article>
					<?php
				}
				elseif (get_row_layout() == '_faq') 
				{
					?>
					<article class="block active style02">
						<div class="heading-holder">
							<h3>How Do I?/ FAQs</h3>
						</div>
						<div>
							<?php 
							if ( ( ! empty( $department) ) && ( $department != 'home' ) ) {
								$faq_query = new WP_Query('post_type=faq&meta_key=_department&meta_value='.$department);
								while ($faq_query->have_posts() ) {
									$faq_query->next_post();
									printf('<p><a href="%s">%s</a></p>', get_permalink( $faq_query->post->ID ), get_the_title( $faq_query->post->ID ) );
								}
							}
							?>
						</div>
						<!-- <div class="slide">
							<div class="more-holder"><a href="#" class="more">Show FAQs</a></div>
						</div> -->
					</article>					
					<?php
				}
				elseif (get_row_layout() == '_calendar') 
				{
					?>
					<article class="block style03">
						<div class="heading-holder">
							<h3>HR Calendar of Events</h3>
						</div>
						<div class="calendar-placeholder">
							<?php if ( function_exists( 'tribe_calendar_mini_grid' ) ) tribe_calendar_mini_grid(); ?>
                            <?php //echo do_shortcode('[my_calendar format="mini" category="human-resources" showkey="no" shownav="yes" toggle="no" time="month"]'); ?> 
						</div>
                        <a href="http://metronetnew.metrogr.org/departments/human-resources/calendar" class="more">View Large Calendar</a>
					</article>	
					<?php
				}
			}
						
			
			if ($column_id == 1)
			{
				?>
				</div>
				<?php
			}
			elseif ($column_id == 2)
			{
				?>
				</div>
				<?php
			}
			elseif ($column_id == 3)
			{
				?>
						</div>
					</div>
				</div>	
				<?php
			}
		}
	}
?>


<?php get_footer(); ?>