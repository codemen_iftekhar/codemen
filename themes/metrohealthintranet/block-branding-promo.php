<?php
$component_name = 'branding-promo';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);
?>
<?php if ($component_properties->movable) { ?><article class="block <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h3>Satisified Patients</h3>
		</div>
		
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
			<div class="portlet-content">
				<figure class="img-holder">
					<img src="<?php echo get_template_directory_uri(); ?>/images/img9.jpg" alt="image description">
					<figcaption class="text-holder"><span>Mia Fallon</span>Well Baby Visit</figcaption>
				</figure>
				<p>And to Becky Fallon, that's a very good thing. Becky had certainly looked forward to having a baby. She just wasn't expecting two until her doctor gave her the doubly good news.... <a href="#">READ MORE</a></p>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>

<!--
<article class="block active mobile-hidden">
	<div class="heading-holder">
		<a href="#" class="opener">opener</a>
		<h3>Satisified Patients</h3>
	</div>
	<div class="slide">
		<div class="block-holder">
			<figure class="img-holder">
				<img src="<?php echo get_template_directory_uri(); ?>/images/img9.jpg" alt="image description">
				<figcaption class="text-holder"><span>Mia Fallon</span>Well Baby Visit</figcaption>
			</figure>
			<p>And to Becky Fallon, that's a very good thing. Becky had certainly looked forward to having a baby. She just wasn't expecting two until her doctor gave her the doubly good news.... <a href="#">READ MORE</a></p>
		</div>
	</div>
</article>
-->