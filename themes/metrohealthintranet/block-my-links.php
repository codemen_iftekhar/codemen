<?php
$component_name = 'my-links';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);
?>
<?php if ($component_properties->movable) { ?><article class="block <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h3>My Links</h3>
		</div>
		
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
			<div class="portlet-content">
				<ul class="block-list">
				<?php
				if (!empty($component_properties->filter_val)) foreach ($component_properties->filter_val as $link_post_id)
				{
					$linked_post = get_post($link_post_id);
					if ($linked_post) printf('<li><a href="%s">%s</a></li>', get_permalink($link_post_id), get_the_title($link_post_id));
				}
				?>
				</ul>
				<p class="align-right"><a href="/profile-settings">Customize My Links</a></p>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>

<!--
<article class="block active">
	<div class="heading-holder">
		<a href="#" class="opener">opener</a>
		<h3>My Links</h3>
	</div>
	<div class="slide">
		<ul class="block-list">
			<li><a href="#">Emergency Dept Home</a></li>
			<li><a href="#">Emergency Dept Schedule</a></li>
			<li><a href="#">Emergency Dept Policies</a></li>
			<li><a href="#">EPIC Training</a></li>
		</ul>
	</div>
</article>
-->