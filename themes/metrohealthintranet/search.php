<?php get_header();

dynamic_sidebar('breadcrumbs') ?>
<div class="column-main-sidenav">
	<?php get_sidebar(); ?>
</div>
<div id="twocolumns">
	<div class="twocolumns-holder">
		<div class="column-inner">
			<div class="main-heading">
				 <h2><?php _e('Search Results', 'base'); ?></h2>
			</div>
		    <?php if(have_posts()) : ?>
				  <?php while (have_posts()) : the_post(); ?>
				
				<div id="post-<?php the_ID(); ?>">
					<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'base'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<?php the_excerpt(); ?>
				</div>
		
			<?php endwhile; ?>
			
			<div class="navigation">
				<div class="next"><?php next_posts_link(__('Older Entries &raquo;', 'base')) ?></div>
				<div class="prev"><?php previous_posts_link(__('&laquo; Newer Entries', 'base')) ?></div>
			</div>
			<?php else : ?>
	
		<div class="block-contact">
			<h2><?php _e('No posts found.', 'base'); ?></h2>
			<p><?php _e('Try a different search?', 'base'); ?></p>
		</div>
		
	<?php endif ?>
			
			
		</div>
		<div class="column-inner">
			<?php get_sidebar('right'); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>