<?php
$component_name = 'my-connections';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);
?>
<?php if ($component_properties->movable) { ?><article class="box style01 <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h2>My Connections</h2>
		</div>
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
			<div id="ffshome-my-connections" class="portlet-content">
				<?php 
                                function word_snippet($text,$length=250,$tail="...") {
                                    $text = trim($text);
                                    $txtl = strlen($text);
                                    if($txtl > $length) {
                                        for($i=1;$text[$length-$i]!=" ";$i++) {
                                            if($i == $length) {
                                                return substr($text,0,$length) . $tail;
                                            }
                                        }
                                        $text = substr($text,0,$length-$i+1) . $tail;
                                    }
                                    return $text;
                                }
                            $args=array(                                  
                                    'scope'=>'friends',
                                    'action'=>'activity_update',
                                    'per_page'=>3
                               );
                            //if ( bp_has_activities( bp_ajax_querystring( 'activity' ) ) ) :   
                             if ( bp_has_activities( $args ) ) : ?>
                            
                                <ul id="activity-stream" class="activity-list item-list">
                                    <?php while ( bp_activities() ) : bp_the_activity(); ?>
                                               <li class="<?php bp_activity_css_class(); ?>" id="activity-<?php bp_activity_id(); ?>">
                                                   <?php 
                                                   global $activities_template;
                                                   $profilel=$activities_template->activity->user_login;
                                                   ?>
                                                    <div class="activity-avatar">
                                                            <a href="<?php echo get_home_url().'/people/'.$profilel.'/profile/'; ?>">

                                                                    <?php 
                                                                    $avatar=array(                                  
                                                                        'width' =>108,
                                                                        'height' =>108
                                                                   );
                                                                    bp_activity_avatar($avatar);
                                                                    
                                                                    ?>

                                                            </a>
                                                    </div>

                                                    <div class="activity-content">

                                                            <div class="activity-header">

                                                                        <?php                                                                           
                                                                               $action = $activities_template->activity->action;
                                                                               preg_match('/(<a.*?<\/a>)/i', $action, $matches);
                                                                               echo $matches[0];
                                                                               $actionmeta = ' - '.bp_insert_activity_meta('');
                                                                               echo preg_replace('/<a[^>]*>(.*?)<\/a>/', '$1', $actionmeta, 1);
                                       
                                                                             ?>

                                                            </div>

                                                            <?php if ( 'activity_comment' == bp_get_activity_type() ) : ?>

                                                                    <div class="activity-inreplyto">
                                                                            <strong><?php _e( 'In reply to: ', 'buddypress' ); ?></strong><?php bp_activity_parent_content(); ?> <a href="<?php bp_activity_thread_permalink(); ?>" class="view" title="<?php _e( 'View Thread / Permalink', 'buddypress' ); ?>"><?php _e( 'View', 'buddypress' ); ?></a>
                                                                    </div>

                                                            <?php endif; ?>

                                                            <?php if ( bp_activity_has_content() ) : ?>

                                                                    <div class="activity-inner">

                                                                            <?php
                                                                                $textcount=bp_get_activity_content_body();
                                                                                                                                                            
                                                                                echo word_snippet($textcount,150);
                                                                            ?>
                                                                           <a class="ffs-readmore" href="<?php echo bp_get_activity_thread_permalink(); ?>">VIEW POST</a>

                                                                    </div>

                                                            <?php endif; ?>

                                                            <?php do_action( 'bp_activity_entry_content' ); ?>
                                                    </div>

                                                    
                                            </li>
                                       
                                            <?php //locate_template( array( 'activity/entry.php' ), true, false ); ?>
                                                       
                                    <?php endwhile; ?>                       
                                            
                                </ul>


                            <?php else : ?>

                                    <div id="message" class="info">
                                            <p><?php _e( 'Sorry, there was no activity found. Please try a different filter.', 'buddypress' ); ?></p>
                                    </div>

                            <?php endif; ?>
                                <?php 
                              global $current_user;
                              $uid=$current_user->ID;
                              $username = $current_user->first_name;
                             
                                ?>
				<!-- <p class="align-right"><a href="/profile-settings">Customize My Connections</a></p> -->
				<p class="align-right"><a href="<?php echo get_home_url().'/people/'.$username.'/friends/' ?>">See All Activity</a></p>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>

<!--

<article class="box active style01">
				<div class="heading-holder">
					<a href="#" class="opener">opener</a>
					<h2>My Connections</h2>
				</div>
				<div class="slide">
					<ul class="comments-list">
						<li>
							<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/img6.jpg" alt="image description"></div>
							<div class="text-holder">
								<h3><a href="#">Kimberly Surname - 6 minutes ago</a></h3>
								<p> ut mattis vel, elementum iaculis. Brna vel, elementum iaculis. Brna</p>
							</div>
						</li>
						<li>
							<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/img7.jpg" alt="image description"></div>
							<div class="text-holder">
								<h3><a href="#">Thomas Pryce - 2 hours ago</a></h3>
								<p>ut mattis vel, elementum iaculis. Brna el, elementum iaculis. Brnanibh, blandit ut mattis vel, </p>
							</div>
						</li>
						<li>
							<div class="img-holder"><img src="<?php echo get_template_directory_uri(); ?>/images/img8.jpg" alt="image description"></div>
							<div class="text-holder">
								<h3><a href="#">Dr. Keith - 2 days ago</a></h3>
								<p>Added 2 Hours ago <br />Brna nibh, blandit ut mattis vel, elementum iaculis. Brnanibh, blandit ut mattis vel, elementum iacu</p>
							</div>
						</li>
					</ul>
					<div class="more-holder">
						<a href="#" class="more alignright">Add Conection</a>
						<a href="#" class="more alignleft">View All My Connections</a>
					</div>
				</div>
			</article>
-->
