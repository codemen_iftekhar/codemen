<?php
get_header();

dynamic_sidebar('breadcrumbs') ?>

<?php /* <div class="column-main-sidenav">
	<?php get_sidebar(); ?>
</div> */ ?>
<div id="twocolumns">
	<div class="twocolumns-holder">
		<div class="column-inner">
			<div class="main-heading">
	   		<h2><?php echo get_the_title(get_the_ID()) ?></h2>
  		</div>
		<form method="post">

			<div class="grid_4">
				<?php 
				#region My Metro Updates // <editor-fold defaultstate="collapse" desc="My Metro Updates">
				$component_name = 'my-metro-updates'; 
				$component_properties = get_component_settings($component_name);
				?>
				<p><strong>My Metro Updates Widget</strong></p>
				<!-- <p><input type="checkbox" name="<?php echo $component_name; ?>_visibility" />Show this widget</p> -->
				<p>Select category of updates you would like to see included on your home page.<br />
				<?php

				$categories = get_categories(array('include' =>  $component_properties->options, 'hide_empty' => 0));
				array_unshift($categories, (object) array('term_id' =>  $component_properties->options, 'name' => 'All'));
				foreach ($categories as $category)
				{
					printf('<input type="radio" name="%1$s[filter_val]" %3$s value="%2$s">%4$s<br />', 
							$component_name, 
							$category->term_id, 
							((string) $component_properties->filter_val === (string) $category->term_id ? 'checked="checked"' : ''), 
							$category->name
					);
				}
				?>
				</p>
				<?php
				#endregion // </editor-fold>
				?>


			</div>

			<div class="grid_4">
				<?php 
				/*
				$component_name = 'my-projects';
				$component_properties = get_component_settings($component_name);
				?>
				<p><strong>My Projects Widget</strong></p>
				<?php
				if ( bp_has_groups( bp_ajax_querystring( 'groups' ) ) )
				{
					if ( $component_properties->filter_val == 0) 
						printf('<input type="radio" name="my_projects[filter_val]" value="%d" checked="checked" />%s<br />', 0, 'All');
					else
						printf('<input type="radio" name="my_projects[filter_val]" value="%d" />%s<br />', 0, 'All');

					while ( bp_groups() ) 
					{
						bp_the_group();

						if ( $component_properties->filter_val == bp_get_group_id())
							printf('<input type="radio" name="my_projects[filter_val]" value="%d" checked="checked" />%s<br />', bp_get_group_id(), bp_get_group_name());
						else
							printf('<input type="radio" name="my_projects[filter_val]" value="%d" />%s<br />', bp_get_group_id(), bp_get_group_name());
					}
				}
				?>

				<br /><br />
				<?php 
				$component_name = 'my-connections'; 
				$component_properties = get_component_settings($component_name);
				?>
				<p><strong>My Connections Widget</strong></p>
				<p>
				<?php
				$friend_list = new Friend_List();
				$lists = $friend_list->get_labels();

				if ( $component_properties->filter_val == 0) 
					printf('<input type="radio" name="my_connections[filter_val]" value="%1$d" checked="checked">%2$s 
						<br />', 
						0, 
						'All');
				else
					printf('<input type="radio" name="my_connections[filter_val]" value="%1$d">%2$s 
						<br />', 
						0, 
						'All');

				if (!empty($lists))
					foreach ($lists as $list_id => $list_name)
					{
						if ( $component_properties->filter_val == $list_id)
							printf('<input type="radio" name="my_connections[filter_val]" value="%1$d" checked="checked">%2$s 
								<a href="%3$s?list_id=%1$d">Edit</a>
								<a href="%1$d">Delete</a>
								<br />', 
								$list_id, 
								$list_name,
								get_permalink(77)
							);
						else
							printf('<input type="radio" name="my_connections[filter_val]" value="%1$d">%2$s 
								<a href="%3$s?list_id=%1$d">Edit</a>
								<a href="%1$d">Delete</a>
								<br />', 
								$list_id, 
								$list_name,
								get_permalink(77)
							);
					}
				?>
				</p>

					<p><strong>Create a new list</strong></p>
					List Name: <input name="list_name" type="text" value="this doesnt work yet" />
					<input type="submit" value="create" />


			</div>

			<div class="grid_4">
				<?php
				*/
				
				#region Applications Widget // <editor-fold defaultstate="collapse" desc="Applications Widget">
				$component_name = 'my-applications';
				$component_properties = get_component_settings($component_name);
				$application_images = MH_Application::get_images();
				?>
				<p><strong>My Applications Widget</strong></p>
				<?php
				foreach ( $component_properties->options as $name => $url)
				{
					if (isset($application_images[$name]))
						printf('<img src="%s" width="40" height="40" />', $application_images[$name]);


					if (in_array($url, $component_properties->filter_val))
						printf('<input type="checkbox" name="%1$s[filter_val][%3$s]" value="%2$s" checked="checked" />%3$s<br />', $component_name, $url, $name);
					else
						printf('<input type="checkbox" name="%1$s[filter_val][%3$s]" value="%2$s" />%3$s<br />', $component_name, $url, $name);
				}

				#endregion // </editor-fold>

				?>
				<br /><br />

				<p><strong>My Links Widget</strong></p>
				<?php
				$component_name = 'my-links';
				$component_properties = get_component_settings($component_name);
				?>
				<ul>
				<?php
				if (!empty($component_properties->filter_val)) foreach ($component_properties->filter_val as $link_post_id)
				{
					$linked_post = get_post($link_post_id);
					if ($linked_post) 
						printf('<li><a href="%s">%s</a> <small><em><a href="#" class="delete_bookmark" rel="%d">[remove]</a></em></small></li>', 
								get_permalink($link_post_id), 
								get_the_title($link_post_id), 
								$link_post_id);
				}
				?>
				</ul>
				<!-- <p class="align-right"><a href="/profile-settings">Customize My Links</a></p> -->
			</div>

			<div class="clear"></div>

			<div class="grid_12">
				<input type="submit" value="Save Preferences" />
				<input type="hidden" name="action" value="save_account_preferences" />
			</div>
		</form>
		</div>
		<?php /* <div class="column-inner">
			<?php get_sidebar('right'); ?>
		</div> */ ?>
	</div>
</div>

<?php get_footer(); ?>