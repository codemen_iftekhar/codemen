<?php
$component_name = 'important-links';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);
?>
<?php if ($component_properties->movable) { ?><article class="block <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h3>Important Links</h3>
		</div>
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
			<div class="portlet-content">
				<ul class="block-list">
				<?php
				foreach ($component_properties->filter_val as $link_name => $link_url)
				{
					printf('<li><a href="%s">%s</a></li>', $link_url, $link_name);
				}
				?>
				</ul>
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>

<!--
<article class="block active">
				<div class="heading-holder">
					<a href="#" class="opener">opener</a>
					<h3>Important Links</h3>
				</div>
				<div class="slide">
					<ul class="block-list">
						<li><a href="#">Emergency Dept Home</a></li>
						<li><a href="#">Emergency Dept Schedule</a></li>
						<li><a href="#">Emergency Dept Policies</a></li>
						<li><a href="#">EPIC Training</a></li>
					</ul>
				</div>
			</article>
-->