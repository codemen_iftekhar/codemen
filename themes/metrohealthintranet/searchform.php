<?php $sq = get_search_query() ? get_search_query() : __('', 'base'); ?>
<div class="search">
	<form method="get" id="searchform" action="<?php echo home_url(); ?>" >
		<fieldset>
			<legend>Search form</legend>
			<label for="item1">Search</label>
			<div class="search-holder">
				
				<?php if(has_nav_menu('search-menu'))
						wp_nav_menu( array('container' => false,
							 'theme_location' => 'search-menu',
							 'menu_class' => 'search-nav',
							 'depth' => 1,
							 'items_wrap' => '<ul class="%2$s">%3$s</ul>'."\n",
							 'walker' => new Custom_Walker_Nav_Menu) ); ?>

				<div class="search-area">
					<input type="submit" value="search">
					<div class="search-frame">
						<input accesskey="4" id="item1" type="text" name="s" value="<?php echo $sq; ?>" placeholder="What Can We Help You Find?">
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>