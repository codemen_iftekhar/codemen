			<article class="block active style02">
				<div class="heading-holder">
					<h3>How Do I?/ FAQs</h3>
				</div>
				<div class="slide">
					<div class="more-holder"><a href="#" class="more">Show FAQs</a></div>
				</div>
			</article>
			<article class="block active">
				<div class="heading-holder">
					<h3>Metro Moments</h3>
				</div>
				<div class="slide">
					<div class="slide-container">
						<?php
						$metro_moment = new WP_Query('cat=59&posts_per_page=1');
						while( $metro_moment->have_posts() ) {
							$metro_moment->next_post();
							?>
						
							<h4><?php echo get_the_title( $metro_moment->post->ID ); ?></h4>
							<?php echo ciho_get_excerpt( $metro_moment->post->post_excerpt, $metro_moment->post->post_content ); ?>
							
								<a href="<?php echo get_permalink( $metro_moment->post->ID ); ?>">READ MORE</a></p>
							<?php
						}
						?>
						<div class="col-holder">
							<div class="col">
								<p><a href="#">Submit</a></p>
								<p><a href="#">Metro Moment</a></p>
							</div>
							<div class="col">
								<p><a href="<?php echo get_category_link( 59 ); ?>">View All </a></p>
								<p><a href="<?php echo get_category_link( 59 ); ?>">Metro Moments</a></p>
							</div>
						</div>
					</div>
				</div>
			</article>
			<article class="block style03">
				<div class="heading-holder">
					<h3>HR Calendar of Events</h3>
				</div>
				<div class="calendar-placeholder">
					<img src="<?php echo get_template_directory_uri(); ?>/images/img-calendar-placeholder.gif" alt="image description">
				</div>
				<a href="#" class="more">View More HR Events</a>
			</article>