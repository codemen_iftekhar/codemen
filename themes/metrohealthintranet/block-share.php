<?php
$component_name = 'share';
$component_properties = get_component_settings($component_name);
$addl_classes = component_properties_to_css_classes($component_properties);
?>
<?php if ($component_properties->movable) { ?><article class="style01 box <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>"><?php } ?>
	<div class="portlet <?php echo $addl_classes; ?>" id="<?php echo $component_name; ?>">
		<div class="<?php echo ($component_properties->movable) ? 'heading-holder' : 'main-heading'; ?>">
			<?php if ($component_properties->collapsible) { ?><a href="#" class="opener">opener</a><?php } ?>
			<h2>Share a Post</h2>
		</div>
		<?php if ($component_properties->movable) { ?><div class="slide"><?php } ?>
			<div id="ffshome-share" class="portlet-content">
				<form action="<?php bp_activity_post_form_action(); ?>" method="post" id="whats-new-form" name="whats-new-form" role="complementary">

					<?php do_action( 'bp_before_activity_post_form' ); ?>

					<!--<h5><?php if ( bp_is_group() )
							printf( __( "What's new in %s, %s?", 'buddypress' ), bp_get_group_name(), bp_get_user_firstname() );
						else
							printf( __( "What's new, %s?", 'buddypress' ), bp_get_user_firstname() );
					?></h5>-->

					<div id="whats-new-content">
						<div id="whats-new-textarea">
							<textarea name="whats-new" id="whats-new" cols="50" rows="10"><?php if ( isset( $_GET['r'] ) ) : ?>@<?php echo esc_attr( $_GET['r'] ); ?> <?php endif; ?></textarea>
						</div>
                                           

						<div id="whats-new-options">
							<div id="whats-new-submit">
								<input type="submit" name="aw-whats-new-submit" id="aw-whats-new-submit" value="<?php _e( 'Share Your Post', 'buddypress' ); ?>" />
                                                               
							</div>
                                                         <div class="show-all-your-post"><?php 
                                                                global $current_user;
                                                                $username = $current_user->user_login;                                                               
                                                                echo'<a href="'. get_home_url().'/people/'.$username.'">Show All Your Post</a>';
                                                                
                                                                ?></div>

							<?php if ( bp_is_active( 'groups' ) && !bp_is_my_profile() && !bp_is_group() ) : ?>

								<!--<div id="whats-new-post-in-box">

									<?php _e( 'Post in', 'buddypress' ); ?>:

									<select id="whats-new-post-in" name="whats-new-post-in">
										<option selected="selected" value="0"><?php _e( 'My Profile', 'buddypress' ); ?></option>

										<?php if ( bp_has_groups( 'user_id=' . bp_loggedin_user_id() . '&type=alphabetical&max=100&per_page=100&populate_extras=0' ) ) :
											while ( bp_groups() ) : bp_the_group(); ?>

												<option value="<?php bp_group_id(); ?>"><?php bp_group_name(); ?></option>

											<?php endwhile;
										endif; ?>

									</select>
								</div>-->
								<input type="hidden" id="whats-new-post-object" name="whats-new-post-object" value="groups" />

							<?php elseif ( bp_is_group_home() ) : ?>

								<input type="hidden" id="whats-new-post-object" name="whats-new-post-object" value="groups" />
								<input type="hidden" id="whats-new-post-in" name="whats-new-post-in" value="<?php bp_group_id(); ?>" />

							<?php endif; ?>

							<?php do_action( 'bp_activity_post_form_options' ); ?>

						</div><!-- #whats-new-options -->
					</div><!-- #whats-new-content -->

					<?php wp_nonce_field( 'post_update', '_wpnonce_post_update' ); ?>
					<?php do_action( 'bp_after_activity_post_form' ); ?>

				</form><!-- #whats-new-form -->
			</div>
		<?php if ($component_properties->movable) { ?></div><?php } ?>
	</div>
<?php if ($component_properties->movable) { ?></article><?php } ?>