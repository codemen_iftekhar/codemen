<?php
class Ciho_Metrohealth_IDM_Department
{
	static public function get_all() {
		global $wpdb;
		$departments = $wpdb->get_results("SELECT id, name FROM idm_department WHERE is_active = 'Y' ORDER BY name ASC");
		$departments_a = array();
		foreach ($departments as $department)
		{
			$departments_a[$department->id] = $department->name;
		}
		return $departments_a;		
	}
	
	/**
	 * Get all IDM records of people under a specific department
	 * 
	 * @global class $wpdb
	 * @param string $department_name
	 * @return array of db row objects 
	 */
	static public function get_deparment_users( $id ) {
		global $wpdb;
		$query = $wpdb->prepare( "SELECT * FROM idm_data WHERE custom_department_id = %d", $id );
		do_action( 'wp_app_log', 'Ciho_Metrohealth_IDM_Department::get_deparment_users() $query', $query );
		$user_departments = $wpdb->get_results( $query );
		do_action('wp_app_log', 'Ciho_Metrohealth_IDM_Department::get_deparment_users() $user_departments', $user_departments);
		return $user_departments;
	}
	
	/**
	 * Get name of a department
	 * 
	 * @global class $wpdb
	 * @param integer $id
	 * @return string 
	 */
	static public function get_one( $id ) {
		global $wpdb;
		$query = $wpdb->prepare("SELECT name FROM idm_department WHERE id = %d AND is_active = 'Y' LIMIT 1", $id );
		do_action( 'wp_app_log', 'Ciho_Metrohealth_IDM_Department::get_one $id', $id);
		do_action( 'wp_app_log', 'Ciho_Metrohealth_IDM_Department::get_one $query', $query);
		return $wpdb->get_var( $wpdb->prepare("SELECT name FROM idm_department WHERE id = %d AND is_active = 'Y' LIMIT 1", $id ) );
	}
	
	/**
	 * Get departments by array of department ids
	 * 
	 * @global class $wpdb
	 * @param array $array Array of integers of department ids
	 * @return array
	 */
	static public function get_array( $array ) {
		global $wpdb;
		$clean_array = array();
		foreach ( $array as $value ) {
			$clean_array []= intval( $value );
		}
		$ids = implode( ',', $clean_array );
		$results = $wpdb->get_results( sprintf("SELECT * FROM idm_department WHERE id IN (%s)", $ids) );
		if ( empty( $results ) ) return null;
		
		$departments = array();
		foreach ( $results as $result ) {
			$departments[ $result->id ] = $result->name;
		}
		return $departments;
	}
	
	static public function get( $id ) {
		global $wpdb;
		return $wpdb->get_row( $wpdb->prepare( "SELECT * FROM idm_department WHERE id = %d AND is_active = 'Y' LIMIT 1", $id) );
	}
}