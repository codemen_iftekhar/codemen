<?php
class AD_Data
{
	public $user_id;
	public $AD_Title;
	
	/**
	 * Create Object
	 * 
	 * @param integer $wp_user_id Wordpress User ID
	 */
	function __construct( $wp_user_id ) {
		
	}
}

class AD_Data_Mapper
{
	static function get_one() {
		
	}
	
	/**
	 * Return array of AD objects
	 * 
	 * @global class $wpdb
	 * @param array $array_of_logins
	 * @return array Array of objects with properties
	 */
	static function get_array_by_user_logins( $array_of_logins ) {
		global $wpdb;
		
		foreach ( $array_of_logins as $key => $AD_ID ) {
			$array_of_logins[ $key ] = "'" . $array_of_logins[ $key ] . "'";
		}
		
		$logins_csv = implode( ',', $array_of_logins );
		
		$query = sprintf( '
			SELECT 
				um.user_id, 
				um.meta_key, 
				um.meta_value AS adi_title
			FROM %s um
			INNER JOIN %s u
			ON u.ID = um.user_id
			WHERE um.meta_key IN ("adi_title")
			AND u.user_login IN (%s)
		', $wpdb->usermeta, $wpdb->users, $logins_csv );
		
		do_action( 'wp_app_log', 'AD_Data_Mapper::get_array_by_user_logins $array_of_logins', $array_of_logins );
		do_action( 'wp_app_log', 'AD_Data_Mapper::get_array_by_user_logins $query', $query );
		
		$results = $wpdb->get_results( $query );
		
		if ( empty( $results ) ) return array();
		
		$new_results = array();
		foreach ( $results as $result ) {
			$new_results[ $result->user_id ] = (object) array( 'title' => $result->adi_title );
		}
		return $new_results;
	}
}
