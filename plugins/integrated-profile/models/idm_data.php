<?php
class Ciho_Metrohealth_IDM_Data
{
	public $first_name;
	public $last_name;
	public $AD_ID;
	public $employee_id;
	public $department_id;
	public $department_name;
	public $custom_department_id;
	public $phone;
	public $email;
	public $address1;
	public $address2;
	public $city;
	public $state;
	public $zip;
	
	/**
	 *
	 * @param string 'AD_ID'
	 * @return type 
	 */
	function __construct( $AD_ID ) {
		$db_row = Ciho_Metrohealth_IDM_Data_Mapper::get( $AD_ID );
		$this->_create_from_received_object( $db_row );
	}
	
	private function _create_from_received_object($obj)
	{
		if ( ! is_object( $obj ) ) return;
		foreach (get_object_vars($this) as $param => $value)
		{
			if ( isset ( $obj->$param ) ) $this->$param = $obj->$param;
		}
	}
}

class Ciho_Metrohealth_IDM_Data_Mapper
{
	static public function get( $AD_ID = '' )
	{
		
		if (empty($AD_ID)) return null;
		
		global $wpdb;
		$query = $wpdb->prepare('SELECT * FROM idm_data WHERE AD_ID = %s', $AD_ID);
		return $wpdb->get_row( $query );
	}
	
	/**
	 * Return rows from idm_data based on array of AD_IDs
	 * 
	 * @param array $array_of_ad_ids
	 * @return array Array of objects (rows from idm_data) 
	 */
	static public function get_users_by_ad_id( $array_of_ad_ids ) {
		global $wpdb;
		
		foreach ( $array_of_ad_ids as $key => $AD_ID ) {
			$array_of_ad_ids[ $key ] = "'" . $array_of_ad_ids[ $key ] . "'";
		}
		
		$ad_ids_csv = implode( ',', $array_of_ad_ids );
		
		$query = sprintf('SELECT * FROM idm_data WHERE AD_ID IN (%s) ORDER BY last_name ASC', $ad_ids_csv );
		do_action( 'wp_app_log', 'Ciho_Metrohealth_IDM_Data_Mapper::get_users_by_ad_id $array_of_ad_ids', $array_of_ad_ids );
		do_action( 'wp_app_log', 'Ciho_Metrohealth_IDM_Data_Mapper::get_users_by_ad_id $query', $query );
		
		$results = $wpdb->get_results( $query );
		
		if ( empty( $results ) ) return array();
		
		$new_results = array();
		
		foreach ( $results as $result ) {
			$new_results[ $result->AD_ID ] = $result;
		}
		
		return $new_results;
	}
}