<?php
/*
Plugin Name: Integrated Profile
*/

require 'models/idm_data.php';
require 'models/idm_department.php';
require 'models/ad_data.php';

class Ciho_Metrohealth_Integrated_Profile
{
	function __construct() {
		// add_action( 'ciho_metrohealth_ad_authenticated', array( $this, 'on_authenticate' ), 10, 2 );
		add_action( 'set_current_user', array( $this, 'stuff_current_user' ), 10 );
		add_filter( 'ciho_hydrate_3rd_party_data', array( $this, 'add_data_to_user_obj' ), 10 );
		
		// Runs when viewing (for edit) another users account
		add_action( 'edit_user_profile', array( $this, 'show_fields_on_user_edit' ) );

		// Runs after a profile is saved (mine or someone elses)
		add_action( 'profile_update', array( $this, 'save_fields_from_user_edit' ) );
		
		
	}
	
	function show_fields_on_user_edit( $user ) {
		global $wpdb;
		$idm_user = Ciho_Metrohealth_IDM_Data_Mapper::get( $user->user_login );
		$idm_departments = Ciho_Metrohealth_IDM_Department::get_all();
		
		$current_custom_dept_id = get_user_meta( $user->ID, 'custom_idm_department_id', true );
		
		?>
		<h3>IDM Department</h3>
		<?php
		
		if ( empty( $idm_user ) ) {
			?>
			<p>This user does not have an IDM account.</p>
			<?php
		} else {
			?>
			<p>According to IDM this user's department is <strong><?php echo $idm_departments[ $idm_user->department_id ]; ?><strong>.</p>
			<?php 
		}
		?>
		<p>Set/override department to: 
			<select name="custom_department_id">
				<option value="0">--</option>
				<?php
				foreach ( $idm_departments as $dept_id => $dept_name) {
					$selected = '';
					if ( $current_custom_dept_id == $dept_id ) $selected = 'selected="selected"';
					?>
					<option value="<?php echo $dept_id; ?>" <?php echo $selected; ?>><?php echo $dept_name; ?></option>
					<?php
				}
				?>
			</select>
		<?php
	}
	
	function save_fields_from_user_edit($user_id) {
		if ( ! empty( $_POST['custom_department_id'] ) ) {
			$custom_department_id = intval( $_POST['custom_department_id'] );
			update_user_meta( $user_id, 'custom_idm_department_id', $custom_department_id ); 
			
			$user = get_user_by( 'id', $user_id );
			
			global $wpdb;
			$wpdb->query( $wpdb->prepare( 'UPDATE idm_data SET custom_department_id = %d WHERE AD_ID = %s', $custom_department_id, $user->user_login ) );
		}
	}
	
	function on_authenticate($user_id) {
		
	}
	
	function stuff_current_user() {
		global $current_user;
		
		$idm_a = self::get_idm_data( $current_user->ID );
		
		$current_user->idm_data = $idm_a;
	}
	
	static function get_idm_data( $user_id ) {
		$AD_ID = get_user_meta( $user_id, 'adi_samaccountname', true );
		
		$idm = new Ciho_Metrohealth_IDM_Data( $AD_ID );
				
		$idm_a = new stdClass();
		foreach( get_object_vars( $idm ) as $param => $value ) {
			$idm_a->$param = $value;
		}
		
		$idm_a->department_name = Ciho_Metrohealth_IDM_Department::get_one( $idm_a->custom_department_id );
		
		return $idm_a;
	}
	
	static function add_data_to_user_obj( $usr_obj ) {
		$idm_a = self::get_idm_data( $usr_obj->ID );
		do_action('wp_app_log', 'got here $usr_obj->ID', $usr_obj->ID);
		do_action('wp_app_log', 'got here $idm_a', $idm_a);
		$usr_obj->idm_data = $idm_a;
		do_action('wp_app_log', 'got here $usr_obj', $usr_obj);
		return $usr_obj;
	}
}
new Ciho_Metrohealth_Integrated_Profile();
