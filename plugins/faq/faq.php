<?php

/*
  Plugin Name: FAQ
 */

function create_fq_post_type() 
{
	$labels = array(
		'name' => 'FAQ',
		'singular_name' => 'FAQ',
		'add_new' => 'Add New',
		'add_new_item' => 'Add New FAQ',
		'edit_item' => 'Edit FAQ',
		'new_item' => 'New FAQ',
		'all_items' => 'All FAQs',
		'view_item' => 'View FAQ',
		'search_items' => 'Search FAQs',
		'not_found' => 'No FAQs found',
		'not_found_in_trash' => 'No FAQs found in Trash',
		'parent_item_colon' => '',
		'menu_name' => 'FAQ'
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => array('slug' => 'faq'),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title', 'editor')
	);

	register_post_type('faq', $args);
}

add_action('init', 'create_fq_post_type');