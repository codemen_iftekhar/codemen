=== TC Disable Browser Upgrade Warning ===
Contributors: taupecat
Requires at least: 3.2
Tested up to: 3.4.1
Stable tag: 1.0.1
Tags: dashboard, browser, browser-nag

Remove the "Your browser is out of date!" warning dashboard widget.

== Description ==

WordPress 3.2 introduced a new dashboard widget that warns you if you're not using the latest version of your chosen browser. While some may find this useful, if your clients/contributors/other interested parties cannot upgrade their browser for whatever reason (or simply choose not to), you can disable this warning across the board rather than scare them with warning messages they have no power to control.

Of course, by disabling this warning message you run the risk that your WordPress site may not function properly for older browser users. However, global browser usage statistics indicate that WordPress' stated support (namely, Internet Explorer >= 7), will be perfectly fine for the vast majority of WordPress users out there.

== Installation ==

1. Place the tc-disable-browser-upgrade-warning directory inside your plugins directory.
2. Navigate to the Plugins section of the Dashboard and click "Activate".

== Changelog ==

= Version 1.0.1 =
* Normalized inconsistent usage of "Update" vs. "Upgrade" in plugin name.

= Version 1.0 =
* Initial release.
