<?php 
	include("IXR_Library.php");
	
	//Set it to false in Production Environment
	$client->debug = true; 
	// $title variable will insert your blog title
	$title = get_the_title();

	//this function will return the file type 
	function get_mime_type($file){

        // our list of mime types
        $mime_types = array(
                "pdf"=>"application/pdf"
                ,"exe"=>"application/octet-stream"
                ,"zip"=>"application/zip"
                ,"docx"=>"application/msword"
                ,"doc"=>"application/msword"
                ,"xls"=>"application/vnd.ms-excel"
                ,"ppt"=>"application/vnd.ms-powerpoint"
                ,"gif"=>"image/gif"
                ,"png"=>"image/png"
                ,"jpeg"=>"image/jpg"
                ,"jpg"=>"image/jpg"
                ,"mp3"=>"audio/mpeg"
                ,"wav"=>"audio/x-wav"
				,"mp4"=>"video/mp4"
				,"flv"=>"video/flv"
				,"avi"=>"video/avi"
                ,"mpeg"=>"video/mpeg"
                ,"mpg"=>"video/mpeg"
                ,"mpe"=>"video/mpeg"
                ,"mov"=>"video/quicktime"
                ,"avi"=>"video/x-msvideo"
                ,"3gp"=>"video/3gpp"
                ,"css"=>"text/css"
                ,"jsc"=>"application/javascript"
                ,"js"=>"application/javascript"
                ,"php"=>"text/html"
                ,"htm"=>"text/html"
                ,"html"=>"text/html"
        );

        $extension = strtolower(end(explode('.',$file)));

        return $mime_types[$extension];
	}
	
	if((!empty($title)) && ($_REQUEST['fields']['field_13']=='yes')){	
	$uploadpath = wp_upload_dir();
	$content = stripslashes($_POST['content']);
	//update the links
	
	$sourceurl = site_url();
	$liveurl = get_option('rfronturl');
	
	$newcontent = str_replace($sourceurl, $liveurl, $content);
	
	
	//get all the img tags from the post content
	$picture = array();
	
	$matched = preg_match_all( '/src=["\']([^"]*)["\']/i', $newcontent, $picture ) ;
    $images = sizeof($picture[1]);
	
	if($images=='0'){
	$matched = preg_match_all( '/href=["\']([^"]*)["\']/i', $newcontent, $picture ) ;
	$images = sizeof($picture[1]);
	}
	
	
	//extract the file name from src
	for($i=0; $i<$images; $i++){
	
		$imgfile = $picture[1][$i];
		//name the src file
		$basename = basename($imgfile);
		//upload the post images
		$tempImagesfolder = $uploadpath['url'];
				
		$filebasename = str_replace("%","", $basename);

		$newcontent = str_replace($basename, $filebasename, $newcontent);
		
		//get the file tyep
		$filetype = get_mime_type($filebasename);
		
		$attachImage = uploadImage($tempImagesfolder,$filebasename, $filetype);

		$newcontent = str_replace($filebasename, basename($attachImage['url']), $newcontent);
		
		//echo  basename($attachImage['url']);
		
	}
	
	//exit;
	
    $posttitle = $title;
  
  
	$ptype = $_POST['post_type'];  

  	$customfields =  array( 'key' => 'sendmail', 'value' => 'yes' ) ;

    $content = array(
        'title'=>$posttitle,
        'description'=>   $newcontent ,
        'mt_allow_comments'=>0,  // 1 to allow comments
        'mt_allow_pings'=>0,  // 1 to allow trackbacks
        'post_type'=>$ptype,
        'mt_keywords'=>$keywords,
        'categories'=>array($category),
		'custom_fields' =>  array($customfields)
		);

	$client = new IXR_Client(get_option('xmlurl'));

	$meta_values = get_post_meta($_POST['post_ID'], 'rpid', true);
	
	$getpost_params = array($meta_values, get_option('rusername'), get_option('rpassword')); 

	//$post_status = $client->query('metaWeblog.getPost', $getpost_params);
	//$rp_resp = $client->getResponse();
	
	//print_r($rp_resp);
	//exit;
	
	if(empty($meta_values)){

		$params = array(0,get_option('rusername'), get_option('rpassword'), $content ,false); 
	
		if (!$client->query('metaWeblog.newPost', $params)) {
			die('Something went wrong - '.$client->getErrorCode().' : '.$client->getErrorMessage());
		}else{	
			echo "Article Posted Successfully";
			$ret = $client->getResponse();
			add_post_meta($_POST['post_ID'], 'rpid', $ret, true);
		}
		
	}else{

		$params = array( $meta_values ,get_option('rusername'), get_option('rpassword'), $content ,false); 
	
		if (!$client->query('metaWeblog.editPost', $params)) {
			die('Something went wrong - '.$client->getErrorCode().' : '.$client->getErrorMessage());
		}else{

			$ret = $client->getResponse();
			add_post_meta($_POST['post_ID'], 'rpid', $ret, true);
	
			//echo "Article Posted Successfully";
			
		}
	}
	
	
	}
	
	function uploadImage($tempImagesfolder,$filebasename,$filetype){
      	
		$filename = $tempImagesfolder ."/" . $filebasename;
		
		$filedata = file_get_contents($filename);

		$rpid = get_post_meta($_POST['post_ID'], 'rpid', true);
		
		//echo $rpid;
		//exit;
        $data = array(
            'name'  => $filebasename, 
            'type'  => $filetype,
            'bits'  => new IXR_Base64($filedata),
		);
		
		//Create the client object
		$client = new IXR_Client(get_option('xmlurl'));
		
		$res = $client->query('metaWeblog.newMediaObject',1,get_option('rusername'), get_option('rpassword'),$data);
     	$returnInfo = $client->getResponse();
		
		//print_r($returnInfo);
		//exit;
		return $returnInfo;     //return the url of the posted Image
	 }

	
?>