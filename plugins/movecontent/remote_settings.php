<?php 
	include("IXR_Library.php");
		
	if(isset($_REQUEST['submit'])){
			
	if((get_option('rusername') != $_REQUEST['rusername']) || (get_option('rpassword') != $_REQUEST['rpassword']) || (get_option('xmlurl') != $_REQUEST['xmlurl']) || (get_option('rfronturl') != $_REQUEST['rfronturl'])) {
		update_option('rusername', $_REQUEST['rusername']);
		update_option('rpassword', $_REQUEST['rpassword']);
		update_option('xmlurl', $_REQUEST['xmlurl']);
		update_option('rfronturl', $_REQUEST['rfronturl']);
			
		}else{
			
		add_option('rusername', $_REQUEST['rusername'], '', 'yes');
		add_option('rpassword', $_REQUEST['rpassword'], '', 'yes');
		add_option('xmlurl', $_REQUEST['xmlurl'], '', 'yes'); 
		add_option('rfronturl', $_REQUEST['rfronturl'], '', 'yes');
			
		}
						
	}
	
?>
<form method="post" action=""> 
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr>
    <td width="15%">&nbsp;</td>
    <td width="85%">&nbsp;</td>
  </tr>
  <tr>
  	<td colspan="2"><h1>Set Options</h1></td>
  </tr>
  <tr>
    <td><strong>Remote Web Url:</strong></td>
    <td><input type="text" name="rfronturl" value="<?php echo get_option('rfronturl');?>"   style="width:300px;"/></td>
  </tr>
  <tr>
    <td><strong>Remote UserName:</strong></td>
    <td><input type="text" name="rusername" value="<?php echo get_option('rusername');?>"  /></td>
  </tr>
  <tr>
    <td><strong>Remote Password:</strong></td>
    <td><input type="text" name="rpassword" value="<?php echo get_option('rpassword');?>" /></td>
  </tr>
  <tr>
    <td><strong>Remote XML-RPC Url:</strong></td>
    <td><input type="text" name="xmlurl"  value="<?php echo get_option('xmlurl');?>" style="width:300px;"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><?php submit_button(); ?></td>
  </tr>
</table>
</form>