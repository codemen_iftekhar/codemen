<?php
class Ciho_Reports {
	function wp_users_ad_and_idm_groups() {
		global $wpdb;
		
		$query = "
SELECT 
    u.ID AS `WP User ID`, 
    u.user_login AS `Active Directory ID`, 
    um.meta_value AS `AD Dept`, 
    i.department_id AS `IDM Dept ID`, 
    i.custom_department_id AS `Custom IDM Dept ID`, 
    i.department_name AS `IDM Dept Name Handwritten`,
    id.name AS `IDM Dept Name Reference`
FROM wp_usermeta um
INNER JOIN wp_users u ON u.ID = um.user_id
INNER JOIN idm_data i ON i.AD_ID = u.user_login
INNER JOIN idm_department id ON id.id = i.custom_department_id
WHERE um.meta_key = 'adi_department'
ORDER BY i.department_name ASC";
		
		return $wpdb->get_results($query);
	}
}