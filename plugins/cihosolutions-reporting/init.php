<?php
/*
Plugin Name: Reporting
Plugin URI: http://cihosolutions.com
Description: Reporting
Author: Andrej Ciho
Version: 0.1
Author URI: http://cihosolutions.com/
*/

require_once(WP_PLUGIN_DIR . '/' . plugin_basename(dirname(__FILE__)).'/reports.php');

class Ciho_Reporting_Settings {
	function admin_screens() {
		add_management_page( 'Reporting', 'Reports', 'manage_options', 'ciho-reporting', array('Ciho_Reporting_Settings', 'main_screen' ));
	}
	
	function main_screen() {
		?>
<h2>Reports</h2>
<p><a target="_blank" href="<?php echo plugin_dir_url(__FILE__); ?>export.php?report=wp_users_ad_and_idm_groups">WP Users and their AD and IDM Groups</a></p>
		<?php
	}
}

add_action('admin_menu', array('Ciho_Reporting_Settings', 'admin_screens'));
