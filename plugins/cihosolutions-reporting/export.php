<?php
/*
 * This script is to be ONLY called directly
 */

define('WP_USE_THEMES', false);
require_once('../../../wp-config.php');

#region Make sure user's eyes were meant to see this report
global $current_user;
get_currentuserinfo();

if (empty($current_user)) die('I\'m sorry but your user account does not have sufficient privileges to generate this report');
if (!user_can($current_user, 'publish_posts')) die('I\'m sorry but your user account does not have sufficient privileges to generate this report');
#endregion

#region Grab Report
require_once('reports.php');
$report_lib = new Ciho_Reports();
$report = sprintf('%s', $_GET['report']);
if (!method_exists($report_lib, $report)) die('Invalid report requested');
$records = $report_lib->$report();
#endregion

$lines = array();

#region Prepare First Row
foreach (array_keys((array) $records[0]) as $field_name) $line[] = '"'.$field_name.'"';
$lines[] = implode(',', $line);
#endregion

#region Populate Each Line of CSV
foreach ($records as $record) {
	foreach ($record as &$field) $field = '"'.$field.'"';
	$lines[] = implode(',', (array) $record);
};
#endregion

#region Spit out CSV
header("Content-type: application/csv");
header('Content-Disposition: attachment; filename='.$report.'.csv');
header("Pragma: no-cache");
header("Expires: 0");

foreach ($lines as $line) {
	echo $line."\r\n";
}
#endregion
?>