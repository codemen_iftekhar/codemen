<?php
if ( class_exists( 'acf_Field' ) && ! class_exists( 'Ciho_MetroHealth_My_Department_Field' ) ) 
{
	class Ciho_MetroHealth_My_Department_Field extends acf_Field 
	{
		/*--------------------------------------------------------------------------------------
		*
		*	Constructor
		*	- This function is called when the field class is initalized on each page.
		*	- Here you can add filters / actions and setup any other functionality for your field
		*
		*	@author Elliot Condon
		*	@since 2.2.0
		* 
		*-------------------------------------------------------------------------------------*/

		function __construct($parent)
		{	
			// do not delete!
			parent::__construct($parent);

			// set name / title
			$this->name = 'metrohealth_my_department'; // variable name (no spaces / special characters / etc)
			$this->title = __("My Department",'acf'); // field label (Displayed in edit screens)

		}
		
		/*--------------------------------------------------------------------------------------
		*
		*	create_field
		*	- this function is called on edit screens to produce the html for this field
		*
		*	@author Elliot Condon
		*	@since 2.2.0
		* 
		*-------------------------------------------------------------------------------------*/
		
		function create_field( $field ) {
			$departments = array();
			
			global $current_user;
			get_currentuserinfo();
			
			if ( class_exists( 'Ciho_Metrohealth_IDM_Department' ) ) {
				$department = Ciho_Metrohealth_IDM_Department::get( $current_user->idm_data->custom_department_id );
			}
			
			do_action( 'wp_app_log', 'Ciho_MetroHealth_Department_Field::create_field $field', $field );
			do_action( 'wp_app_log', 'Ciho_MetroHealth_Department_Field::create_field $department', $department );
			do_action( 'wp_app_log', 'Ciho_MetroHealth_Department_Field::create_field $department', $current_user );
			
			$departments = array( $department->id => $department->name );
			
			echo '<select id="' . $field['name'] . '" class="' . $field['class'] . '" name="' . $field['name'] . '" >';
			foreach ($departments as $department_id => $department_name)
			{
				$selected = ( $department_id == $field['value'] ) ? $selected = 'selected="selected"' : '';
				printf('<option value="%d" %s>%s</option>', $department_id, $selected, $department_name);
			}
			echo '</select>';
		}
	}
}
