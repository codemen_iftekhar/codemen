<?php
if ( class_exists( 'acf_Field' ) && ! class_exists( 'Ciho_MetroHealth_Department_Field' ) ) 
{
	class Ciho_MetroHealth_Department_Field extends acf_Field 
	{
		/*--------------------------------------------------------------------------------------
		*
		*	Constructor
		*	- This function is called when the field class is initalized on each page.
		*	- Here you can add filters / actions and setup any other functionality for your field
		*
		*	@author Elliot Condon
		*	@since 2.2.0
		* 
		*-------------------------------------------------------------------------------------*/

		function __construct($parent)
		{	
			// do not delete!
			parent::__construct($parent);

			// set name / title
			$this->name = 'metrohealth_department'; // variable name (no spaces / special characters / etc)
			$this->title = __("Department",'acf'); // field label (Displayed in edit screens)

		}
		
		/*--------------------------------------------------------------------------------------
		*
		*	create_field
		*	- this function is called on edit screens to produce the html for this field
		*
		*	@author Elliot Condon
		*	@since 2.2.0
		* 
		*-------------------------------------------------------------------------------------*/
		
		function create_field( $field ) {
			$departments = array();
			
			global $current_user;
			get_currentuserinfo();
			
			if ( class_exists( 'Ciho_Metrohealth_IDM_Department' ) ) {
				$departments = Ciho_Metrohealth_IDM_Department::get_all();
				$department = Ciho_Metrohealth_IDM_Department::get( $current_user->idm_data->custom_department_id );
			}
			
			// User is not member of any URLs
			if ( ! current_user_can( 'manage_options' ) ) if ( empty ( $department ) ) return;
			
			if ( ( empty( $field['value'] ) ) && ( ! empty( $department ) ) ) $field['value'] = $department->id;
			
			echo '<select id="' . $field['name'] . '" class="' . $field['class'] . '" name="' . $field['name'] . '" >';
			$i = 0;
			foreach ($departments as $department_id => $department_name)
			{
				if ( ++$i==1 ) if ( current_user_can( 'manage_options' ) ) printf('<option value="%s" %s>%s</option>', 'home', '', 'Home Page');
				$selected = ( $department_id == $field['value'] ) ? $selected = 'selected="selected"' : '';
				if ( current_user_can( 'manage_options' ) )
				{
					printf('<option value="%d" %s>%s</option>', $department_id, $selected, $department_name);
				}
				elseif ( $department_id == $department->id )
				{
					printf('<option value="%d" %s>%s</option>', $department_id, $selected, $department_name);
				}
			}
			echo '</select>';
		}
	}
}
