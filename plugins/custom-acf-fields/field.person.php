<?php
if ( class_exists( 'acf_Field' ) && ! class_exists( 'Ciho_MetroHealth_Person_Field' ) ) 
{
	class Ciho_MetroHealth_Person_Field extends acf_Field 
	{
		/*--------------------------------------------------------------------------------------
		*
		*	Constructor
		*	- This function is called when the field class is initalized on each page.
		*	- Here you can add filters / actions and setup any other functionality for your field
		*
		*	@author Elliot Condon
		*	@since 2.2.0
		* 
		*-------------------------------------------------------------------------------------*/

		function __construct($parent)
		{	
			// do not delete!
			parent::__construct($parent);

			// set name / title
			$this->name = 'metrohealth_person'; // variable name (no spaces / special characters / etc)
			$this->title = __("Person",'acf'); // field label (Displayed in edit screens)

		}
		
		/*--------------------------------------------------------------------------------------
		*
		*	create_field
		*	- this function is called on edit screens to produce the html for this field
		*
		*	@author Elliot Condon
		*	@since 2.2.0
		* 
		*-------------------------------------------------------------------------------------*/
		
		function create_field( $field ) {
			global $current_user, $post;
			
			$department_id = get_field( '_department', $post->ID );
			
			$users = Ciho_Metrohealth_IDM_Department::get_deparment_users( $department_id );
					
			echo '<select id="' . $field['name'] . '" class="' . $field['class'] . '" name="' . $field['name'] . '" >';
			foreach ($users as $user)
			{
				$selected = ( $user->AD_ID == $field['value'] ) ? $selected = 'selected="selected"' : '';
				printf('<option value="%s" %s>%s</option>', $user->AD_ID, $selected, $user->first_name . ' ' . $user->last_name );
			}
			echo '</select>';
		}
	}
}
