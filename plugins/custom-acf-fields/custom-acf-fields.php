<?php
/*
Plugin name: Custom ACF Field
*/

class Custom_ACF_Field
{
	function register_my_acf_field() {
		if ( function_exists( 'register_field' ) ) {
			register_field( 'Ciho_MetroHealth_FAQ_Field',           dirname( __file__ ) . '/field.faq.php' );
			register_field( 'Ciho_MetroHealth_Category_Field',      dirname( __file__ ) . '/field.category.php' );
			register_field( 'Ciho_MetroHealth_Department_Field',    dirname( __file__ ) . '/field.department.php' );
			register_field( 'Ciho_MetroHealth_My_Department_Field', dirname( __file__ ) . '/field.my_department.php' );
			register_field( 'Ciho_MetroHealth_Group_Field',         dirname( __file__ ) . '/field.group.php' );
			register_field( 'Ciho_MetroHealth_Person_Field',        dirname( __file__ ) . '/field.person.php' );
		}
	}
}

add_action( 'functions-php', array( 'Custom_ACF_Field', 'register_my_acf_field' ) );