<?php
if ( class_exists( 'acf_Field' ) && ! class_exists( 'Ciho_MetroHealth_FAQ_Field' ) ) 
{
	class Ciho_MetroHealth_FAQ_Field extends acf_Field 
	{
		/*--------------------------------------------------------------------------------------
		*
		*	Constructor
		*	- This function is called when the field class is initalized on each page.
		*	- Here you can add filters / actions and setup any other functionality for your field
		*
		*	@author Elliot Condon
		*	@since 2.2.0
		* 
		*-------------------------------------------------------------------------------------*/

		function __construct($parent)
		{	
			// do not delete!
			parent::__construct($parent);

			// set name / title
			$this->name = 'ciho_metrohealth_faq_foe;d'; // variable name (no spaces / special characters / etc)
			$this->title = __("FAQ",'acf'); // field label (Displayed in edit screens)

		}
		
		/*--------------------------------------------------------------------------------------
		*
		*	create_field
		*	- this function is called on edit screens to produce the html for this field
		*
		*	@author Elliot Condon
		*	@since 2.2.0
		* 
		*-------------------------------------------------------------------------------------*/
		
		function create_field( $field ) 
		{
			$fad_location = new FAD_Location();
			$locations = $fad_location->get_all();
			
			echo '<select id="' . $field['name'] . '" class="' . $field['class'] . '" name="' . $field['name'] . '" >';
			foreach ($locations as $loc)
			{
				$selected = ( $loc->pkid == $field['value'] ) ? $selected = 'selected="selected"' : '';
				printf('<option value="%d" %s>%s</option>', $loc->pkid, $selected, $loc->label);
			}
			echo '</select>';
		}
	}
}
