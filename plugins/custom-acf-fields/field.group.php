<?php
if ( class_exists( 'acf_Field' ) && ! class_exists( 'Ciho_MetroHealth_Group_Field' ) ) 
{
	class Ciho_MetroHealth_Group_Field extends acf_Field 
	{
		/*--------------------------------------------------------------------------------------
		*
		*	Constructor
		*	- This function is called when the field class is initalized on each page.
		*	- Here you can add filters / actions and setup any other functionality for your field
		*
		*	@author Elliot Condon
		*	@since 2.2.0
		* 
		*-------------------------------------------------------------------------------------*/

		function __construct($parent)
		{	
			// do not delete!
			parent::__construct($parent);

			// set name / title
			$this->name = 'metrohealth_group'; // variable name (no spaces / special characters / etc)
			$this->title = __("Group",'acf'); // field label (Displayed in edit screens)

		}
		
		/*--------------------------------------------------------------------------------------
		*
		*	create_field
		*	- this function is called on edit screens to produce the html for this field
		*
		*	@author Elliot Condon
		*	@since 2.2.0
		* 
		*-------------------------------------------------------------------------------------*/
		
		function create_field( $field ) {
			global $current_user;
			
			$groups = array();
			
			
			if ( current_user_can('manage_options') ) {
				$group_terms = ciho_metrohealth_get_all_group_terms();
			} elseif ( class_exists( 'Ciho_User_Group_Admin_Setting' ) ) {
				$groups = Ciho_User_Group_Admin_Setting::get_user_admin_groups($current_user->ID);
				$group_terms = ciho_metrohealth_get_all_group_terms_by_ids($groups);
			}
			
			echo '<select id="' . $field['name'] . '" class="' . $field['class'] . '" name="' . $field['name'] . '" >';
			foreach ($group_terms as $group_id => $group_name)
			{
				$selected = ( $group_id == $field['value'] ) ? $selected = 'selected="selected"' : '';
				printf('<option value="%d" %s>%s</option>', $group_id, $selected, $group_name);
			}
			echo '</select>';
		}
	}
}
