<?php
class Metro_Widget
{
	public $component_id;
	public $movable;
	public $collapsible;
	public $filter_val;
	public $options;
	public $column;
	public $default_order;
	public $state;
	
	public function __construct($array = null)
	{
		if (empty($array)) return;
		
		foreach ($array as $property => $value)
		{
			if (property_exists('Metro_Widget', $property))
			{
				$this->$property = $value;
			}
		}
		return $this;
	}
	
	static public function get_default_property_names()
	{
		return array('movable', 'collapsible', 'options', 'column', 'default_order', 'filter_var', 'state');
	}
	
	static public function get_saveable_property_names()
	{
		return array('filter_val', 'state');
	}
}