<?php
class MH_Application
{
	static function get_all()
	{
		global $GLOBALS;

		$GLOBALS['acf_field'] = array();

		if (!function_exists('get_field')) return;

		$array = array();
		if (get_field('applications', 'option')) 
		{
			while( has_sub_field('applications', 'option') )
			{
				$array[get_sub_field('name')] = get_sub_field('url');
			}
		}
		return $array;
	}
	
	static function get_images()
	{
		global $GLOBALS;

		$GLOBALS['acf_field'] = array();

		if (!function_exists('get_field')) return;

		$array = array();
		if (get_field('applications', 'option')) 
		{
			while( has_sub_field('applications', 'option') )
			{
				$array[get_sub_field('name')] = get_sub_field('icon');
			}
		}
		return $array;
	}
}