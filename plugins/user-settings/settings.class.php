<?php
class User_Settings
{
	static $wp_user_id;
	
	/*
	 * meta key for wp_usermeta for order of columns
	 */
	const column_order_meta_key = 'home_column_%d_order';
	
	/*
	 * meta key for wp_usermeta for component settings
	 */
	const component_properties_meta_key = 'home_component_%s';
		
	
	/** 
	 * Return User_Settings object with loaded wp_user_id static var
	 * 
	 * @global object $current_user
	 * @staticvar resource $user_settings_o
	 * @return \User_Settings 
	 */
	public function get()
	{
		static $user_settings_o = null;
		
		if ($user_settings_o == null)
		{
			global $current_user;
			get_currentuserinfo();
			
			$user_settings_o = new User_Settings($current_user->ID);
		}

		return $user_settings_o;
	}
	
	/**
	 * Create object with WP_User ID
	 * 
	 * @param int $wp_user_id 
	 */
	private function __construct($wp_user_id)
	{
		self::$wp_user_id = (int) $wp_user_id;
	}
	
	/**
	 * Save order in the DB 
	 * 
	 * @param integer $column_id Column ID - 1, 2 or 3
	 * @param array $values IDs of components
	 */
	public function set_column_order($column_id, $values)
	{
		return update_user_meta(self::$wp_user_id, sprintf(self::column_order_meta_key, $column_id), $values);
	}
	
	/**
	 * Retrieves user settings from DB
	 * 
	 * @param integer $column_id Column ID - 1, 2 or 3
	 * @return array 
	 */
	public function get_column_order($column_id)
	{
		$order = get_user_meta(self::$wp_user_id, sprintf(self::column_order_meta_key, $column_id), true);
		if (empty($order)) return array();
		
		return $order;
	}
	
	/**
	 * Save properties of a single component into DB
	 * 
	 * @param string $component_id
	 * @param array $values 
	 */
	public function set_component_properties($component_id, $values)
	{
		do_action('wp_app_log', 'User_Settings::set_component_properties saving component '.$component_id, $values);
		update_user_meta(self::$wp_user_id, sprintf(self::component_properties_meta_key, $component_id), $values);
	}
	
	/**
	 * Retrieve properties of a single component from DB
	 * 
	 * @param string $component_id
	 * @return array 
	 */
	public function get_component_properties($component_id)
	{
		$properties = get_user_meta(self::$wp_user_id, sprintf(self::component_properties_meta_key, $component_id), true);
		if (empty($properties)) return array();
		
		return $properties;
	}
	
	/**
	 * Add a single value to filter_val
	 * 
	 * @param string $component_id
	 * @param array $values Array of key=>value. E.g. array('google' => 'http://google.com', 'sme' => 'http://sme.sk');
	 */
	public function add_to_component_properties($component_id, $values)
	{
		$properties_s = $this->get_component_properties($component_id);
		
		if (is_serialized($properties_s))
			$properties_a = unserialize($properties_s);
		else 
			$properties_a = $properties_s;
		
		foreach ($values as $key => $value)
		{
			$properties_a['filter_val'][$key] = $value;
		}
		
		$this->set_component_properties($component_id, $properties_a);
	}
	
	/**
	 * Update single property of a component
	 * 
	 * @param string $component_id
	 * @param string $property_name
	 * @param mixed $value 
	 */
	public function update_component_property($component_id, $property_name, $value)
	{
		$properties_a = $this->get_component_properties($component_id);
	
		do_action('wp_app_log', 'update_component_property() Component ID', $component_id);
		
		
		if (empty($properties_a)) 
		{
			do_action('wp_app_log', 'update_component_property() A', 'User settings for this component are blank');
			$properties_a = (array) Metro_Widget_Defaults::get($component_id);
			$properties_a['filter_val'] = $properties_a['options'];
			do_action('wp_app_log', 'update_component_property() B - retrieved defaults', $properties_a);
		}
		
		$properties_a[$property_name]= $value;
		
		do_action('wp_app_log', 'update_component_property() C', $value);
		
		$this->set_component_properties($component_id, $properties_a);
	}
	
	function remove_from_component_properties($component_id, $property_value, $go_by = 'key')
	{
		$properties_s = $this->get_component_properties($component_id);
		
		if (is_serialized($properties_s))
			$properties_a = unserialize($properties_s);
		else 
			$properties_a = $properties_s;
		
		if (empty($properties_a['filter_val'])) return;
		
		$new_filter_val = array();
		
		do_action('wp_app_log', 'remove_from_component_properties A', $properties_a['filter_val']);
		
		foreach ($properties_a['filter_val'] as $key => $value)
		{
			if ($go_by == 'value')
			{
				if ((string) $value != (string) $property_value) 
				{
					$new_filter_val[$key] = $value;
					do_action('wp_app_log', 'remove_from_component_properties B1', array($key, $value, $property_value));
				}
			}
			elseif ($go_by == 'key')
			{
				if ((string) $key != (string)$property_value) 
				{
					$new_filter_val[$key] = $value;
					do_action('wp_app_log', 'remove_from_component_properties B2', array($key, $value, $property_value));
				}
			}
			$properties_a['filter_val'] = $new_filter_val;
		}
		$this->set_component_properties($component_id, $properties_a);
	}
}
