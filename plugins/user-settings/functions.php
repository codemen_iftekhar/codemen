<?php
/**
 * Retrieve component settings. If no user specific found grab default.
 * 
 * @param string $component_name
 * @return object 
 */
function get_component_settings($component_name)
{
	// Load default settings for the component
	$default_component_settings = Metro_Widget_Defaults::get($component_name);
	
	do_action('wp_app_log', sprintf('get_component_settings(%s) A ', $component_name), $default_component_settings);
	
	if (empty($default_component_settings->filter_val) && !empty($default_component_settings->options)) $default_component_settings->filter_val = $default_component_settings->options;
	
	do_action('wp_app_log', sprintf('get_component_settings(%s) B ', $component_name), $default_component_settings);
	
	// Instantiate $user_settings class
	$user_settings = User_Settings::get();

	// Retrieve user settings from DB
	$component_properties_s = $user_settings->get_component_properties($component_name);
	
	// If no user settings available just return default
	if (empty($component_properties_s)) return $default_component_settings;
	
	do_action('wp_app_log', sprintf('get_component_settings(%s) C ', $component_name), $component_properties_s);
	
	
	// If data is serialized, please unserialize it
	if (is_serialized($component_properties_s))
		$component_properties_a = unserialize($component_properties_s);
	else
		$component_properties_a = $component_properties_s;

	do_action('wp_app_log', sprintf('get_component_settings(%s) D ', $component_name), $component_properties_a);
	
	// cycle
	foreach (Metro_Widget::get_default_property_names() as $property_name)
	{
		if (empty($component_properties_a[$property_name])) 
		{
			if (!empty($default_component_settings->$property_name))
				$component_properties_a[$property_name] = $default_component_settings->$property_name;
			else 
				$component_properties_a[$property_name] = '';
		}
		
		$component_properties_a['options'] = $default_component_settings->options;
	}
	do_action('wp_app_log', sprintf('get_component_settings(%s) E ', $component_name), $component_properties_a);
	return new Metro_Widget($component_properties_a);
}
