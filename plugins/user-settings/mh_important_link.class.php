<?php
class MH_Important_Link
{
	function get_all()
	{
		global $GLOBALS;

		$GLOBALS['acf_field'] = array();

		if (!function_exists('get_field')) return;

		$array = array();
		if (get_field('links', 'option')) 
		{
			while( has_sub_field('links', 'option') )
			{
				$array[get_sub_field('name')] = get_sub_field('url');
			}
		}
		return $array;
	}
	
	function add_link($pair)
	{
		
	}
}