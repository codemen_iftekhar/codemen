<?php
class Metro_Widget_Defaults
{
	static $defaults     = '';
	static $column_order = '';
	
	/**
	 * Get settings of a single component
	 * 
	 * @param string $widget_name
	 * @return object 
	 */
	static public function get($widget_name)
	{
		$defaults = self::load_defaults();
		if (!in_array($widget_name, array_keys($defaults))) return null;
		
		$defaults[$widget_name]->component_id = $widget_name;
		
		return $defaults[$widget_name];
	}
	
	static public function get_default_column_order($column_id)
	{
 		$column_order = self::get_default_columns_order();
		return $column_order[$column_id];
	}
	
	static public function get_default_columns_order()
	{
		if (!empty(self::$column_order)) return self::$column_order;
		
		$columns = array();
		foreach (self::load_defaults() as $widget_name => $widget)
		{
			$columns[$widget->column][$widget->default_order] = $widget_name;
		}
		
		$column_ordered = array();
		foreach ($columns as $column_id => $column_data)
		{
			$column_ordered[$column_id] = implode(',', $column_data);
		}
		self::$column_order = $column_ordered;
		return self::$column_order;
	}
	
	static public function load_defaults()
	{
		if (!empty(self::$defaults)) return self::$defaults;
		self::$defaults = self::populate_defaults();
		return self::$defaults;
	}
	
	static public function populate_defaults()
	{
		return array
		(
			/* Column 1 */
			'important-news' => new Metro_Widget(array
			(
				'movable'       => false,
				'collapsible'   => false,
				'filter_val'    => 8,
				'options'       => 8,
				'column'        => 1,
				'default_order' => 1,
			)),
			'my-metro-updates' => new Metro_Widget(array
			(
				'movable'       => true,
				'collapsible'   => true,
				'state'         => true,
				'options'       => '52,54,55',
				'column'        => 1,
				'default_order' => 2
			)),
			'best-of-metro' => new Metro_Widget(array
			(
				'movable'       => true,
				'collapsible'   => true,
				'options'       => '57,58',
				'filter_val'    => '57,58',
				'column'        => 1,
				'default_order' => 3
			)),

			/* Column 2 */
			'my-profile' => new Metro_Widget(array
			(
				'movable'       => true,
				'collapsible'   => true,
				'column'        => 2,
				'default_order' => 1
			)),
			'my-projects' => new Metro_Widget(array
			(
				'movable'       => true,
				'collapsible'   => true,
				'options'       => null,
				'column'        => 2,
				'default_order' => 2
			)),
			'share' => new Metro_Widget(array
			(
				'movable'       => true,
				'collapsible'   => true,
				'column'        => 2,
				'default_order' => 3
			)),
			'my-connections' => new Metro_Widget(array
			(
				'movable'       => true,
				'collapsible'   => true,
				'options'       => null,
				'column'        => 2,
				'default_order' => 4
			)),

			/* Column 3 */		
			'my-applications' => new Metro_Widget(array
			(
				'movable'       => true,
				'collapsible'   => true,
				'column'        => 3,
				'options'       => MH_Application::get_all(),
				'default_order' => 1
			)),
			'important-links' => new Metro_Widget(array
			(
				'movable'       => true,
				'collapsible'   => false,
				'column'        => 3,
				'options'       => MH_Important_Link::get_all(),
				'default_order' => 2
			)),
			'my-links' => new Metro_Widget(array
			(
				'movable'       => true,
				'collapsible'   => true,
				'column'        => 3,
				'options'       => array(),
				'default_order' => 3
			)),
			'how-do-i' => new Metro_Widget(array
			(
				'movable'       => true,
				'collapsible'   => true,
				'column'        => 3,
				'default_order' => 4
			)),/*
			'cafe-menu' => new Metro_Widget(array
			(
				'movable'       => true,
				'collapsible'   => true,
				'column'        => 3,
				'default_order' => 5
			)), */
			'newshound' => new Metro_Widget(array
			(
				'movable'       => true,
				'collapsible'   => true,
				'column'        => 3,
				'default_order' => 6
			)),
			'branding-promo' => new Metro_Widget(array
			(
				'movable'       => true,
				'collapsible'   => false,
				'column'        => 3,
				'default_order' => 7
			)),
			'metro-moments' => new Metro_Widget(array
			(
				'movable'       => true,
				'collapsible'   => true,
				'filter_val'    => '59',
				'options'        => '59',
				'column'        => 3,
				'default_order' => 8,
				'visibility'    => true
			))
		);
	}
}

