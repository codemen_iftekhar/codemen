<?php
/*
Plugin Name: User Settings
 */

require_once 'settings.class.php'; 
require_once 'metro-widget.class.php'; 
require_once 'defaults.class.php'; 

require_once 'mh_application.class.php'; 
require_once 'mh_important_link.class.php'; 
require_once 'mh_my_link.class.php'; 

require_once 'ajax.class.php'; 
require_once 'functions.php'; 

require_once 'process-post.php'; 

// require_once 'TEST_user-settings.php'; // This is a test

