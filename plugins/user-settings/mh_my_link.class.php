<?php
class MH_My_Link
{
	function get_all()
	{
		global $GLOBALS;

		$GLOBALS['acf_field'] = array();

		if (!function_exists('get_field')) return;

		$array = array();
		if (get_field('links', 'option')) 
		{
			while( has_sub_field('links', 'option') )
			{
				$array[get_sub_field('name')] = get_sub_field('url');
			}
		}
		return $array;
	}
	
	function add_link($post_id)
	{
		$user_settings = User_Settings::get();	
		$user_settings->add_to_component_properties('my-links', array($post_id => $post_id));
	}
	
	function remove_link($post_id)
	{
		$user_settings = User_Settings::get();	
		$user_settings->remove_from_component_properties('my-links', $post_id, 'key');
	}
}