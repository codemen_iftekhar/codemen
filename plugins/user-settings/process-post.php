<?php
function intercept_post_and_save()
{
	if (!isset($_POST)) return;
	if (!isset($_POST['action'])) return;
	

	
	global $mh_notification;
	
	/*
	 * This gets executed from profile page when changing filter on a single widget thet posts back
	 */
	if ($_POST['action'] == 'single_widget_action')
	{
		// Instantiate User_Settings with loaded wp_user_id
		$user_settings = User_Settings::get();
		
		// Get all serialized component settings
		$component_properties = get_component_settings($_POST['component_id']);
		foreach(Metro_Widget::get_saveable_property_names() as $propert_name)
		{
			if (isset($_POST[$propert_name]))
				$new_settings[$propert_name] = $_POST[$propert_name];
			else
				$new_settings[$propert_name] = $component_properties->$propert_name;
		}
	
		// Save new serialized data
		$user_settings->set_component_properties($_POST['component_id'], $new_settings);
		
		// Set Success Message
		$mh_notification = array(
			'text' => 'Changes saved successfully', // Saved changes to '.$_POST['component_id'] . ' so that it filters to '.$_POST['filter_val'], 
			'type' => 'success');
	}
	/*
	 * This gets executed on profile edit page
	 */
	elseif($_POST['action'] == 'save_account_preferences')
	{
		$user_settings = User_Settings::get();
		
		$user_settings->set_component_properties('my-projects', $_POST['my_projects']);
		$user_settings->set_component_properties('my-metro-updates', $_POST['my-metro-updates']);
		$user_settings->set_component_properties('my-applications', $_POST['my-applications']);
		
		$mh_notification = array('text' => 'Account Preferences Saved.', 'type' => 'success');
	}
}

add_action('init', 'intercept_post_and_save');

function intercept_admin_init()
{
	
	// do_action('wp_app_log', 'intercept all posts ', $GLOBALS);	
	// var_dump($pagenow); die(); post.php
	
	// post-new.php?post_type=acf
	
	// post=49&action=edit
	
}

add_action('admin_init', 'intercept_admin_init');
