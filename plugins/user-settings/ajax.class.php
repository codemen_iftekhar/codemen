<?php
/**
 * Ajax Methods 
 */
class Settings_Interface
{
	function set_order_in_column()
	{
		$new_order = $_POST['newOrder'];
		$column_id = $_POST['columnID'];
		
		$nonce = $_POST['postCommentNonce'];
		
		if ( ! wp_verify_nonce( $nonce, 'myajax-post-comment-nonce' ) ) die ( 'Busted!');
		
		$user_settings = User_Settings::get();
		$outcome = $user_settings->set_column_order($column_id, $new_order);
		
		do_action('wp_app_log', 'set_order_in_column() new order', $new_order);
		do_action('wp_app_log', 'set_order_in_column() column id', $column_id);
		do_action('wp_app_log', 'set_order_in_column() outcome', $outcome);
		
		// generate the response
		$response = json_encode( array( 'the_new_order' => $new_order, 'column_id' => $column_id ) );

		// response output
		header( "Content-Type: application/json" );
		echo $response;

		// IMPORTANT: don't forget to "exit"
		exit;
	}
	
	function set_component_properties()
	{
		$new_properties = $_POST['newProperties'];
		$component_id = $_POST['componentID'];
		
		$nonce = $_POST['postCommentNonce'];
		
		do_action('wp_app_log', 'set_component_properties() component_id', $component_id);
		do_action('wp_app_log', 'set_component_properties() new properties', $new_properties);
		
		if ( ! wp_verify_nonce( $nonce, 'myajax-post-comment-nonce' ) ) die ( 'Busted!');
		
		$user_settings = User_Settings::get();
		$user_settings->set_component_properties($component_id, $new_properties);
		
		// generate the response
		$response = json_encode( array( 'the_new_properties' => $new_properties, 'component_id' => $component_id ) );

		// response output
		header( "Content-Type: application/json" );
		echo $response;

		// IMPORTANT: don't forget to "exit"
		exit;
	}
	
	function save_link()
	{
		$post_id = $_POST['postID'];
		$nonce = $_POST['postCommentNonce'];
		
		if ( ! wp_verify_nonce( $nonce, 'myajax-post-comment-nonce' ) ) die ( 'Busted!');
	
		do_action('wp_app_log', 'save_link_ajax', $post_id);
		
		MH_My_Link::add_link($post_id);
		
		// generate the response
		$response = json_encode( array( 'post_id' => $post_id ) );

		// response output
		header( "Content-Type: application/json" );
		echo $response;

		// IMPORTANT: don't forget to "exit"
		exit;
	}
	
	function delete_link()
	{
		$post_id = $_POST['postID'];
		$nonce = $_POST['postCommentNonce'];
		
		if ( ! wp_verify_nonce( $nonce, 'myajax-post-comment-nonce' ) ) die ( 'Busted!');
	
		do_action('wp_app_log', 'delete_link_ajax', $post_id);
	
		MH_My_Link::remove_link($post_id);
		
		// generate the response
		$response = json_encode( array( 'post_id' => $post_id ) );

		// response output
		header( "Content-Type: application/json" );
		echo $response;

		// IMPORTANT: don't forget to "exit"
		exit;
	}
	
	function collapse_component()
	{
		$component_id = $_POST['componentID'];
		$nonce = $_POST['postCommentNonce'];
		
		if ( ! wp_verify_nonce( $nonce, 'myajax-post-comment-nonce' ) ) die ( 'Busted!');
	
		do_action('wp_app_log', 'collapse_component_ajax', $component_id);
		
		$user_settings = User_Settings::get();
		$user_settings->update_component_property($component_id, 'state', 'collapsed');
		
		// generate the response
		$response = json_encode( array( 'collapsed_component' => $component_id ) );

		// response output
		header( "Content-Type: application/json" );
		echo $response;

		// IMPORTANT: don't forget to "exit"
		exit;
	}
	
	function expand_component()
	{
		$component_id = $_POST['componentID'];
		$nonce = $_POST['postCommentNonce'];
		
		if ( ! wp_verify_nonce( $nonce, 'myajax-post-comment-nonce' ) ) die ( 'Busted!');
	
		do_action('wp_app_log', 'expand_component_ajax', $component_id);
	
		$user_settings = User_Settings::get();
		$user_settings->update_component_property($component_id, 'state', 'expanded');
		
		// generate the response
		$response = json_encode( array( 'expanded_component' => $component_id ) );

		// response output
		header( "Content-Type: application/json" );
		echo $response;

		// IMPORTANT: don't forget to "exit"
		exit;
	}
}

// Connect AJAX actions
add_action( 'wp_ajax_myajax-save-order',         array('Settings_Interface', 'set_order_in_column') );
add_action( 'wp_ajax_myajax-save-properties',    array('Settings_Interface', 'set_component_properties') );
add_action( 'wp_ajax_myajax-save-link',          array('Settings_Interface', 'save_link') );
add_action( 'wp_ajax_myajax-delete-link',        array('Settings_Interface', 'delete_link') );
add_action( 'wp_ajax_myajax-expand-component',   array('Settings_Interface', 'expand_component') );
add_action( 'wp_ajax_myajax-collapse-component', array('Settings_Interface', 'collapse_component') );

/*
 * Usage:
 * 
 * jQuery.post(
			MyAjax.ajaxurl,
			{
				action : 'myajax-save-order',
				newOrder : new_order,
				columnID : jQuery(ui.item).parent().attr('column_id'),
				postCommentNonce : MyAjax.postCommentNonce
			},
			function( response ) {
				// alert( response.the_new_order );
				// alert( response.column_id );
			}
		);
 * 
 */