<?php

/**
 * Plugin Name: Only Logged In Users
 */

function force_to_log_in() {
	if (!is_user_logged_in()) {
		wp_redirect(home_url('/wp-login.php'));
		exit;
	}
}

add_action('get_header', 'force_to_log_in');